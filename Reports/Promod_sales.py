
# coding: utf-8

# In[6]:

import pandas as pd
import numpy as np
import datetime
from datetime import date, timedelta
from pandas import DataFrame
import sqlalchemy as sq
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
engine = sq.create_engine("postgresql+psycopg2://analysis_user:20180703@asdTgd@dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")
sql_str2="""
SELECT 'PROMOD INDIA PVT LTD'  as vendor_name,
       TO_CHAR(getdate(),'YYYYMMDD')::BIGINT AS DATE,
       F_OI.order_id,
       F_OI.brand,
       F_OI.style_id,
       F_OI.order_created_date,
       F_OI.order_created_time,
       F_OI.po_type,       
       F_OI.warehouse_id,
       F_OI.article_type,
       F_OI.sku_code,
       dp.vendor_article_number,
       F_OI.gender,
       F_OI.master_category,
       dp.gtin AS gtin,
       SUM(F_OI.quantity) AS qty,
       AVG(F_OI.item_mrp_value) AS mrp,
       (sum(NULLIF(F_OI.product_discount,0) ::float)/sum(NULLIF(F_OI.item_mrp_value,0)))*100 AS product_discount_perc,
       sum(F_OI.product_discount ::float) as product_discount,
        avg(nvl(F_OI.cogs,
        0)+nvl(royalty_commission,
        0)+nvl(stn_input_vat_reversal,
        0)+ nvl(entry_tax,
        0))  AS cost_price,              
       sum(F_OI.item_mrp_value-F_OI.product_discount) AS Net_Sales,
       sum(0.23*(F_OI.item_mrp_value-F_OI.product_discount)) AS margin,
       SUM(case when (F_OI.igst_tax_amount is null or F_OI.igst_tax_amount=0) then (sgst_tax_amount+cgst_tax_amount) else F_OI.igst_tax_amount end ) AS tax,
       avg(case when (F_OI.igst_tax_rate is null or F_OI.igst_tax_rate=0) then (sgst_tax_rate+cgst_tax_rate) else F_OI.igst_tax_rate end ) AS tax_rate,
       SUM(F_OI.item_mrp_value-F_OI.product_discount-(0.23*(F_OI.item_mrp_value-F_OI.product_discount))
       
        - (case when (F_OI.igst_tax_amount is null or F_OI.igst_tax_amount=0) then (sgst_tax_amount+cgst_tax_amount) else F_OI.igst_tax_amount end) 
       )AS base_value 
from fact_core_item f_oi
JOIN dim_product dp ON F_OI.idproduct = dp.id
inner join bidb.reverse_tracking_table ret
on f_oi.order_id=ret.order_release_id 
and f_oi.core_item_id = ret.core_item_id
WHERE  F_OI.order_created_date >= TO_CHAR(getdate()-90,'YYYYMMDD')::BIGINT 
and ret.platform_id=1
and nvl(to_char(received_on_wms,'YYYYMMDD'):: bigint,received_date_rms:: bigint)=to_char(getdate()-1,'YYYYMMDD'):: bigint
and F_OI.vendor_id=3941
GROUP BY 
              TO_CHAR(getdate(),'YYYYMMDD')::BIGINT,
       F_OI.brand,
       F_OI.style_id,
       F_OI.order_created_date,
       F_OI.po_type,
       F_OI.order_id,
       F_OI.order_created_time,
       F_OI.warehouse_id,
       F_OI.article_type,
       F_OI.sku_code,
       dp.vendor_article_number,
       F_OI.gender,
       F_OI.master_category,
       dp.gtin"""



sql_str1="""
SELECT 'PROMOD INDIA PVT LTD'  as vendor_name,
       TO_CHAR(getdate(),'YYYYMMDD')::BIGINT AS DATE,
       F_OI.order_id,
       F_OI.brand,
       F_OI.style_id,
       F_OI.order_created_date,
       F_OI.order_created_time,
       F_OI.po_type,       
       F_OI.warehouse_id,
       F_OI.article_type,
       F_OI.sku_code,
       dp.vendor_article_number,
       F_OI.gender,
       F_OI.master_category,
       dp.gtin AS gtin,
       SUM(F_OI.quantity) AS qty,
       AVG(F_OI.item_mrp_value) AS mrp,
       (sum(NULLIF(F_OI.product_discount,0) ::float)/sum(NULLIF(F_OI.item_mrp_value,0)))*100 AS product_discount_perc,
       sum(F_OI.product_discount ::float) as product_discount,
        avg(nvl(cogs,
        0)+nvl(royalty_commission,
        0)+nvl(stn_input_vat_reversal,
        0)+ nvl(entry_tax,
        0))  AS cost_price,              
       sum(F_OI.item_mrp_value-F_OI.product_discount) AS Net_Sales,
       sum(0.23*(F_OI.item_mrp_value-F_OI.product_discount)) AS margin,
       SUM(case when (F_OI.igst_tax_amount is null or F_OI.igst_tax_amount=0) then (sgst_tax_amount+cgst_tax_amount) else F_OI.igst_tax_amount end ) AS tax,
       avg(case when (F_OI.igst_tax_rate is null or F_OI.igst_tax_rate=0) then (sgst_tax_rate+cgst_tax_rate) else F_OI.igst_tax_rate end ) AS tax_rate,
       SUM(F_OI.item_mrp_value-F_OI.product_discount-(0.23*(F_OI.item_mrp_value-F_OI.product_discount))
       
        - (case when (F_OI.igst_tax_amount is null or F_OI.igst_tax_amount=0) then (sgst_tax_amount+cgst_tax_amount) else F_OI.igst_tax_amount end) 
       )AS base_value 
from fact_core_item f_oi
JOIN dim_product dp ON F_OI.idproduct = dp.id
WHERE (is_realised = 1 OR is_shipped = 1) 
  AND F_OI.order_created_date = to_char(getdate()-1,'YYYYMMDD')::bigint
  and F_OI.vendor_id=3941
GROUP BY 
       TO_CHAR(getdate(),'YYYYMMDD')::BIGINT ,
       F_OI.order_id,
       F_OI.brand,
       F_OI.style_id,
       F_OI.order_created_date,
       F_OI.order_created_time,
       F_OI.po_type,       
       F_OI.warehouse_id,
       F_OI.article_type,
       F_OI.sku_code,
       dp.vendor_article_number,
       F_OI.gender,
       F_OI.master_category,
       dp.gtin
"""
                    


report1=pd.read_sql_query(sql_str1,engine)
report2=pd.read_sql_query(sql_str2,engine)

report1.to_csv('/home/rishi/sales.csv',index=False)
report2.to_csv('/home/rishi/returns.csv',index=False)



report1.round(decimals=0)
report2.round(decimals=0)

a1=report1.to_html(index=False)
a2=report2.to_html(index=False)

t1 ="Hi,\n\nPlease find below the sales performance of 'PROMOD INDIA PVT LTD' for yesterday.\n\nSales data:\n"
t2 ="Returns data:\n"

sender = 'rishi.sharma1@myntra.com'
receivers = ['aarthiy.devendran@myntra.com','tulika.khandelwal@myntra.com','manish.verma@myntra.com']

msg = MIMEMultipart()
msg['Subject'] = 'PROMOD INDIA PVT LTD sales performance report'
msg['From'] =sender
msg['to'] =", ".join(receivers)
 
part1 = MIMEText(t1,'plain')
part2 = MIMEText(a1,'html')
part3 = MIMEText(t2,'plain')
part4 = MIMEText(a2,'html')

msg.attach(part1)
msg.attach(part2)
msg.attach(part3)
msg.attach(part4)

e = file(('/home/rishi/sales.csv'))
attachment = MIMEText(e.read())
attachment.add_header('Content-Disposition', 'attachment', filename=('sales.csv'))
msg.attach(attachment)

e1 = file(('/home/rishi/returns.csv'))
attachment = MIMEText(e1.read())
attachment.add_header('Content-Disposition', 'attachment', filename=('returns.csv'))
msg.attach(attachment)


try:
   smtpObj = smtplib.SMTP('smtp.gmail.com:587')
   smtpObj.ehlo()
   smtpObj.starttls()
   smtpObj.login("rishi.sharma1@myntra.com", "fohmrtjewafukkfx")
   smtpObj.sendmail(sender, receivers, msg.as_string())
   smtpObj.close()
   print "Successfully sent email"
except :
   print "Error: unable to send email"

