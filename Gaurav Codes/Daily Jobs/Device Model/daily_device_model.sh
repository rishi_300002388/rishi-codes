DAY="$(date "+%d")"
YEAR="$(date "+%Y" -d "1 days ago")"
MONTH="$(date "+%m" -d "1 days ago")"
DATE="$(date "+%Y_%m_%d" -d "1 days ago")"
DATE1="$(date "+%Y%m%d" -d "1 days ago")"

source /home/apollo/.config
export PGPASSWORD=$REDSHIFT_PASSWORD

/usr/bin/psql -h $CLICKSTREAM_CLUSTER -U $USER -d $DATABASE -p 5439 -t -c  "drop table if exists gaurav_device_name_mapping_med;
create table customer_insights.gaurav_device_name_mapping_med as

select * 

from

(select device_id, device_name, row_number() over(partition by device_id order by device_name) rank

from

(select device_id, device_name
from gaurav_device_name_mapping
group by 1, 2

union 

select device_id, device_model_number device_name
from events_view
where device_model_number is not null
and load_date = to_char(sysdate - interval '1 day','YYYYMMDD')::bigint
group by 1, 2) as a) as a

where rank = 1;

----
drop table if exists gaurav_device_name_mapping;
create table customer_insights.gaurav_device_name_mapping as

select * from gaurav_device_name_mapping_med;"

if [ $? -gt 0 ]
then
        echo "Either there is a Redshift DB connectivity issue or there might be an error in the script "
        exit 1;
else
        echo "Ran Successfully"
fi