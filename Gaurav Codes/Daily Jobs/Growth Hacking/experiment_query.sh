source /home/apollo/.config
export PGPASSWORD=$REDSHIFT_PASSWORD

/usr/bin/psql -h $CLICKSTREAM_CLUSTER -U $USER -d $DATABASE -p 5439 -t -c  "

drop table if exists customer_insights.experiment_performance_base_query;
create table customer_insights.experiment_performance_base_query as 
SELECT uid, TO_CHAR(sysdate,'YYYYMMDD')::BIGINT AS DATE,
                         a.experiment_id,
                         c.experiment_name,
                         a.bucket_name,
                         SUM(all_pdp_views) AS total_pdp_views,
                         SUM(lp_views) AS total_list_views,
                         SUM(revenue) AS rev,
                         SUM(orders) AS orders,
                         SUM(payment_pageview) AS total_payment_views,
                         SUM(address_pageview) AS total_address_pageview,
                         SUM(homepage_visits_final) AS total_homepage_views,
                         SUM(cart_view) AS total_cart_views,
                         SUM(my_orders_final) AS total_orders_view
                  FROM phantasos_user_agg_metrics a
                    JOIN experiment_metadata_morpheus b ON a.experiment_id = b.experiment_id
                    join (select experiment_id, experiment_name from bucket_assignment group by 1,2) as c
                    on a.experiment_id=c.experiment_id
                  WHERE TO_CHAR(b.stop_time,'YYYYMMDD')::BIGINT>= TO_CHAR(sysdate,'YYYYMMDD')::BIGINT
                  -- and a.experiment_id = 'belkraqgnbom0dm080hg'
                  GROUP BY 1,
                           2,
                           3,
                           4,
                           5;
"

if [ $? -gt 0 ]
then
        echo "Either there is a Redshift DB connectivity issue or there might be an error in the script "
        exit 1;
else
        echo "Ran Successfully"
fi