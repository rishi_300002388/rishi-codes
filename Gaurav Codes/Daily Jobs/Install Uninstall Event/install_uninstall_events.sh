DAY="$(date "+%d")"
YEAR="$(date "+%Y" -d "1 days ago")"
MONTH="$(date "+%m" -d "1 days ago")"
DATE="$(date "+%Y_%m_%d" -d "1 days ago")"
DATE1="$(date "+%Y%m%d" -d "1 days ago")"

source /home/apollo/.config
export PGPASSWORD=$REDSHIFT_PASSWORD

/usr/bin/psql -h $CLICKSTREAM_CLUSTER -U $USER -d $DATABASE -p 5439 -t -c  "drop table if exists customer_insights.pushkar_agg_user_install_uninstall_events;
create table customer_insights.pushkar_agg_user_install_uninstall_events as

select device_id, os, load_date, event, ts as event_ts

from

(select *, lag(event) over(partition by device_id order by load_date) prev_event

from

(select *, row_number() over(partition by device_id, load_date order by flag, ts) rank

from

(select device_id, os, load_date, timestamp as ts, 
	(case when device_class = '' then 'b' else 'a' end) flag, 
	(case when device_class = '' then 'Installed' else 'Uninstalled' end) event
from pushkar_notifications_$DATE
where os in ('Android', 'iOS') 
and tenandid = 'myntra_retail'
and device_class in ('', 'NotRegistered', 'Unregistered')) as a) as a

where rank = 1) as a

where ((prev_event is null) or (prev_event != event));

-----
insert into user_install_uninstall_events

select device_id, os, load_date, event, event_ts

from

(select *, lag(event) over(partition by device_id order by load_date) prev_event 

from

(select *
from user_install_uninstall_events
union all 
select * 
from pushkar_agg_user_install_uninstall_events) as a) as a

where ((prev_event is null) or (prev_event != event))
and load_date = $DATE1;"

if [ $? -gt 0 ]
then
        echo "Either there is a Redshift DB connectivity issue or there might be an error in the script "
        exit 1;
else
        echo "Ran Successfully"
fi