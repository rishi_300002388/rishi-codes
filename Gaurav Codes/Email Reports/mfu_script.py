import pandas as pd
import numpy as np
import datetime
from datetime import date, timedelta
from pandas import DataFrame
import sqlalchemy as sq
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import re
engine = sq.create_engine("postgresql+psycopg2://customer_insights_ddl:20180703@ajqoyP@dw-clickstream.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")

sql_str="""
select a.date, a.hr, revenue revenue_actual, gm gm_actual, asp asp_actual, aisp aisp_actual, b.app_sessions app_traffic_actual, 
100.0*c.app_orders/b.app_sessions app_conversion_actual, total_discount total_discount_actual, units_sold units_sold_actual, mp_share mp_share_actual

from

(SELECT order_created_date date, substring(lpad(order_created_time,4,0),1,2)::bigint hr,
SUM(nvl (item_revenue,0) + nvl (cashback_redeemed,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) AS revenue,
100.0*(SUM(item_revenue + cashback_redeemed - nvl (tax_amount,0) - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) - SUM(CASE WHEN brand = 'Puma' AND seller_id = 28 THEN item_revenue + cashback_redeemed - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0) - nvl (sales_commission,0) - nvl (logistics_charges,0) - nvl (tax_amount,0) ELSE item_purchase_price_inc_tax - vendor_funding + royalty_commission + nvl (entry_tax,0) END))/SUM(item_revenue + cashback_redeemed - nvl (tax_amount,0) - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) AS gm,
1.0*SUM(nvl (item_revenue,0) + nvl (cashback_redeemed,0) + nvl (shipping_charge,0) + nvl (gift_charge,0))/COUNT(DISTINCT order_group_id) asp,
1.0*SUM(nvl (item_revenue,0) + nvl (cashback_redeemed,0) + nvl (shipping_charge,0) + nvl (gift_charge,0))/SUM(item_quantity) aisp,
100.0*(sum(trade_discount) + sum(coupon_discount))/sum(item_mrp) total_discount,
SUM(item_quantity) AS units_sold,
100.0*sum(case when supply_type IN ('JUST_IN_TIME') then (nvl (item_revenue,0) + nvl (cashback_redeemed,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) end)/SUM(nvl (item_revenue,0) + nvl (cashback_redeemed,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) mp_share

FROM fact_order_live
WHERE (is_realised = 1 OR is_shipped = 1)
AND (customer_login not ilike '%%myntra360%%' and customer_login not like '%%benchmarking%%' and customer_login not ilike '%%test%%')
and (order_created_date = to_char(date(convert_timezone('UTC-5:30', getdate())),'YYYYMMDD')::bigint and order_created_date <=20181125)
GROUP BY 1, 2) as a

left join 

(select hr::bigint, count(distinct session_id) as app_sessions

from 

(select session_id, app_name, device_id, min(hour) AS hr
from clickstream.sessionhour_view
where load_Date = to_char(date(convert_timezone('UTC-5:30', getdate())),'YYYYMMDD')::bigint
and app_name in ('MyntraRetailAndroid', 'Myntra')
group by 1, 2, 3)

group by 1
order by 1) as b

on a.hr = b.hr

left join

(SELECT substring(lpad(order_created_time,4,0),1,2)::bigint hr,
count(distinct order_group_id) app_orders
FROM fact_order_live
WHERE device_channel = 'mobile-app'
AND (customer_login not ilike '%%myntra360%%' and customer_login not like '%%benchmarking%%' and customer_login not ilike '%%test%%')
and (order_created_date = to_char(date(convert_timezone('UTC-5:30', getdate())),'YYYYMMDD')::bigint and order_created_date <=20181125)
GROUP BY 1
order by 1) as c

on a.hr = c.hr

order by 1, 2

"""
abc = datetime.datetime.now().strftime("%Y-%m-%d")
today_actual=pd.read_sql_query(sql_str,engine)
today_planned = pd.read_csv("hourly_planned_numbers.csv")
today = pd.merge(today_actual, today_planned, how='inner', on = ['date', 'hr'])
today['revenue_delta'] = 100*(today['revenue_actual']-today['revenue_planned'])/today['revenue_actual']
today['gm_delta'] = 100*(today['gm_actual']-today['gm_planned'])/today['gm_actual']
today['asp_delta'] = 100*(today['asp_actual']-today['asp_planned'])/today['asp_actual']
#today['traffic_delta'] = 100*(today['traffic_actual']-today['Traffic_planned'])/today['traffic_actual']

today['gm_actual'] = today['gm_actual'].round(1).astype(str) + '%'
today['gm_planned'] = today['gm_planned'].round(1).astype(str) + '%'
today['revenue_delta'] = today['revenue_delta'].round(1).astype(str) + '%'
today['gm_delta'] = today['gm_delta'].round(1).astype(str) + '%'
today['asp_delta'] = today['asp_delta'].round(1).astype(str) + '%'
#today['traffic_delta'] = today['traffic_delta'].round(1).astype(str) + '%'
today['app_conversion_actual'] = today['app_conversion_actual'].round(1).astype(str) + '%'
today['total_discount_actual'] = today['total_discount_actual'].round(1).astype(str) + '%'
today['mp_share_actual'] = today['mp_share_actual'].round(1).astype(str) + '%'

today = today[['hr','revenue_actual','revenue_planned','revenue_delta','gm_actual','gm_planned','gm_delta','asp_actual','asp_planned','asp_delta','app_traffic_actual','Traffic_planned','app_conversion_actual','aisp_actual','total_discount_actual','units_sold_actual','mp_share_actual']]
today.rename(columns={'hr':'Hour',
					'revenue_actual':'Revenue_Actual',
					'revenue_planned':'Revenue_Planned',
					'revenue_delta':'Revenue_Delta',
					'gm_actual':'GM_Actual',
					'gm_planned':'GM_Planned',
					'gm_delta':'GM_Delta',
					'asp_actual':'ASP_Actual',
					'asp_planned':'ASP_Planned',
					'asp_delta':'ASP_Delta',
					'app_traffic_actual':'App_Traffic_Actual',
					'Traffic_planned':'Traffic_Planned',
					#'traffic_delta':'Traffic_Delta',
					'app_conversion_actual':'App_Converion_Actual',
					'aisp_actual':'AISP_Actual',
					'total_discount_actual':'Total_Discount_Actual',
					'units_sold_actual':'Units_Sold_Actual',
					'mp_share_actual':'MP_Share_Actual'}, 
                 inplace=True)
today = today.round(1)
today = today.replace(np.nan, '', regex=True)
today['App_Converion_Actual'] = today['App_Converion_Actual'].str.replace('None%', '')
today['App_Converion_Actual'] = today['App_Converion_Actual'].replace('nan%', '')
#today['Traffic_Delta'] = today['Traffic_Delta'].replace('nan%', '')


sql_str="""
select a.date, a.revenue revenue_actual, a.gm gm_actual, a.asp asp_actual, a.aisp aisp_actual, b.app_sessions app_traffic_actual, 
100.0*c.app_orders/nullif(b.app_sessions,0) app_conversion_actual, a.total_discount total_discount_actual, a.units_sold units_sold_actual, a.mp_share mp_share_actual

from

(select date, sum(revenue) revenue, 100.0*sum(rgm)/sum(gm_den) gm, 1.0*sum(revenue)/sum(orders) asp, 1.0*sum(revenue)/sum(units_sold) aisp,
100.0*sum(total_discount)/sum(mrp) total_discount, sum(units_sold) units_sold, 100.0*sum(mp_share)/sum(revenue) mp_share

from

(SELECT order_created_date date, 
SUM(nvl (item_revenue,0) + nvl (cashback_redeemed,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) AS revenue,
(SUM(item_revenue + cashback_redeemed - nvl (tax_amount,0) - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) - SUM(CASE WHEN brand = 'Puma' AND seller_id = 28 THEN item_revenue + cashback_redeemed - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0) - nvl (sales_commission,0) - nvl (logistics_charges,0) - nvl (tax_amount,0) ELSE item_purchase_price_inc_tax - vendor_funding + royalty_commission + nvl (entry_tax,0) END)) AS rgm,
SUM(item_revenue + cashback_redeemed - nvl (tax_amount,0) - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) gm_den,
COUNT(DISTINCT order_group_id) orders,
(sum(trade_discount) + sum(coupon_discount)) total_discount,
sum(item_mrp) mrp,
SUM(item_quantity) AS units_sold,
sum(case when supply_type IN ('JUST_IN_TIME') then (nvl (item_revenue,0) + nvl (cashback_redeemed,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) end) mp_share

FROM fact_order_live
WHERE (is_realised = 1 OR is_shipped = 1)
AND (customer_login not ilike '%%myntra360%%' and customer_login not like '%%benchmarking%%' and customer_login not ilike '%%test%%')
and (order_created_date >= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint and order_created_date >= 20181119 and order_created_date <=20181125)
GROUP BY 1

UNION

SELECT order_created_date date,
SUM((NVL (item_revenue_inc_cashback,0) + NVL (shipping_charges,0) + NVL (gift_charges,0))) AS revenue,
SUM(((nvl (item_revenue_inc_cashback,0) - nvl (tax,0) + nvl (shipping_charges,0) + nvl (gift_charges,0)) -(nvl (cogs,0) + nvl (royalty_commission,0) + nvl (stn_input_vat_reversal,0) + nvl (entry_tax,0)))) AS rgm,
SUM((nvl (item_revenue_inc_cashback,0) - nvl (tax,0) + NVL (shipping_charges,0) + NVL (gift_charges,0))) gm_den,
COUNT(DISTINCT order_group_id) AS orders,
(sum(product_discount) + sum(coupon_discount)) total_discount,
sum(item_mrp_value) mrp,
SUM(quantity) units_sold,
sum(case when po_type IN ('JIT_MARKETPLACE','SMART_JIT') then (NVL (item_revenue_inc_cashback,0) + NVL (shipping_charges,0) + NVL (gift_charges,0)) end) mp_share

FROM fact_core_item 
WHERE (is_realised = 1 OR is_shipped = 1)
AND (order_created_date >= 20181119 and order_created_date <= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '2 day')),'YYYYMMDD')::bigint and order_created_date <=20181125)
AND   store_id = 1
GROUP BY 1) as a

group by 1) as a

left join 

(select load_Date, count(distinct session_id) as app_sessions

from 

(select session_id, app_name, device_id, load_Date, min(hour) AS hr
from clickstream.sessionhour_view
where load_Date >= 20181119
and load_Date <= to_char(date(convert_timezone('UTC-5:30', getdate())),'YYYYMMDD')::bigint
and app_name in ('MyntraRetailAndroid', 'Myntra')
group by 1, 2, 3, 4)

group by 1
order by 1) as b

on a.date = b.load_Date

left join 

(select date, sum(orders) app_orders

from

(SELECT order_created_date date, 
count(distinct order_group_id) orders

FROM fact_order_live
WHERE device_channel = 'mobile-app'
AND (customer_login not ilike '%%myntra360%%' and customer_login not like '%%benchmarking%%' and customer_login not ilike '%%test%%')
and (order_created_date >= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint and order_created_date >= 20181119 and order_created_date <=20181125)
GROUP BY 1

UNION

SELECT order_created_date date,
count(distinct order_group_id) orders

FROM fact_core_item 
WHERE is_booked = 1
and device_channel = 'mobile-app'
AND (order_created_date >= 20181119 and order_created_date <= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '2 day')),'YYYYMMDD')::bigint and order_created_date <=20181125)
AND   store_id = 1
GROUP BY 1) as a

group by 1) as c

on a.date = c.date

order by 1

"""

day_actual=pd.read_sql_query(sql_str,engine)
day_planned = pd.read_csv("daily_planned_numbers.csv")
day = pd.merge(day_actual, day_planned, how='inner', on = ['date'])
day['revenue_delta'] = 100*(day['revenue_actual']-day['revenue_planned'])/day['revenue_actual']
day['gm_delta'] = 100*(day['gm_actual']-day['gm_planned'])/day['gm_actual']
day['asp_delta'] = 100*(day['asp_actual']-day['asp_planned'])/day['asp_actual']
#day['traffic_delta'] = 100*(day['traffic_actual']-day['Traffic_planned'])/day['traffic_actual']

day['gm_actual'] = day['gm_actual'].round(1).astype(str) + '%'
day['gm_planned'] = day['gm_planned'].round(1).astype(str) + '%'
day['revenue_delta'] = day['revenue_delta'].round(1).astype(str) + '%'
day['gm_delta'] = day['gm_delta'].round(1).astype(str) + '%'
day['asp_delta'] = day['asp_delta'].round(1).astype(str) + '%'
#day['traffic_delta'] = day['traffic_delta'].round(1).astype(str) + '%'
day['app_conversion_actual'] = day['app_conversion_actual'].round(1).astype(str) + '%'
day['total_discount_actual'] = day['total_discount_actual'].round(1).astype(str) + '%'
day['mp_share_actual'] = day['mp_share_actual'].round(1).astype(str) + '%'

day = day[['date','revenue_actual','revenue_planned','revenue_delta','gm_actual','gm_planned','gm_delta','asp_actual','asp_planned','asp_delta','app_traffic_actual','Traffic_planned','app_conversion_actual','aisp_actual','total_discount_actual','units_sold_actual','mp_share_actual']]
day.rename(columns={'date':'Date',
                    'revenue_actual':'Revenue_Actual',
					'revenue_planned':'Revenue_Planned',
					'revenue_delta':'Revenue_Delta',
					'gm_actual':'GM_Actual',
					'gm_planned':'GM_Planned',
					'gm_delta':'GM_Delta',
					'asp_actual':'ASP_Actual',
					'asp_planned':'ASP_Planned',
					'asp_delta':'ASP_Delta',
					'app_traffic_actual':'App_Traffic_Actual',
					'Traffic_planned':'Traffic_Planned',
					#'traffic_delta':'Traffic_Delta',
					'app_conversion_actual':'App_Converion_Actual',
					'aisp_actual':'AISP_Actual',
					'total_discount_actual':'Total_Discount_Actual',
					'units_sold_actual':'Units_Sold_Actual',
					'mp_share_actual':'MP_Share_Actual'}, 
                 inplace=True)
day = day.round(1)
day = day.replace(np.nan, '', regex=True)
day['App_Converion_Actual'] = day['App_Converion_Actual'].replace('None%', '')
day['App_Converion_Actual'] = day['App_Converion_Actual'].replace('nan%', '')
#day['Traffic_Delta'] = day['Traffic_Delta'].replace('nan%', '')

sender = 'gaurav.choudhary@myntra.com'
receivers = ['vinayak.naik@myntra.com', 'srinivasan.satyamurthy@myntra.com', 'rishi.sharma1@myntra.com','shrinivas.ron@myntra.com','arvind.nair@myntra.com','arunava.chatterjee@myntra.com', 'gaurav.choudhary@myntra.com', 'bhavesh.singhal@myntra.com','growth_core@myntra.com','revenue_analytics@myntra.com']
#receivers = ['gaurav.choudhary@myntra.com']
msg = MIMEMultipart()
msg['Subject'] = 'MFU-2018: Hourly Performance Metrics'
msg['From'] = sender
msg['to'] =", ".join(receivers)

b="Hi All,\n \nHourly Event Summary for Date: "+str(abc)+"\n \n"
t=today.to_html(index=False)
d="\nDaywise Event Summary:\n \n"
c=day.to_html(index=False)
e="\n \n**App conversion definition in above report is same as BM42 definition"

part1 = MIMEText(b,'plain')
part2 = MIMEText(t,'html')
part3 = MIMEText(d,'plain')
part4 = MIMEText(c,'html')
part5 = MIMEText(e,'plain')

msg.attach(part1)
msg.attach(part2)
msg.attach(part3)
msg.attach(part4)
msg.attach(part5)

try:
   smtpObj = smtplib.SMTP('smtp.gmail.com:587')
   smtpObj.ehlo()
   smtpObj.starttls()
   smtpObj.login("gaurav.choudhary@myntra.com", "evxhmfgyslwcmsfi")
   smtpObj.sendmail(sender, receivers, msg.as_string())
   smtpObj.close()
   print "Successfully sent email"
except :
   print "Error: unable to send email"
