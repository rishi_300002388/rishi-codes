import pandas as pd
import numpy as np
import datetime
from datetime import date, timedelta
from pandas import DataFrame
import sqlalchemy as sq
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import re
import gspread_dataframe as gd
import json
import gspread
from oauth2client.client import SignedJwtAssertionCredentials
import pandas as pd
import numpy as np
import datetime
from datetime import date, timedelta
from pandas import DataFrame
import sqlalchemy as sq
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
engine = sq.create_engine("postgresql+psycopg2://customer_insights_ddl:20180703@ajqoyP@dw-clickstream.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")
pd.options.display.float_format = '{:.1f}'.format

##hour level summary
#Myntra Hourly
sql_str="""

select (case when platform_id = 1 then 'Myntra' else 'Jabong' end) platform, order_created_date date, substring(lpad(order_created_time,4,0),1,2)::bigint hr,
SUM(nvl (item_revenue,0) + nvl (cashback_redeemed,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) AS revenue_actual,
100.0*(SUM(item_revenue + cashback_redeemed - nvl (tax_amount,0) - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) - SUM(CASE WHEN brand = 'Puma' AND seller_id = 28 THEN item_revenue + cashback_redeemed - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0) - nvl (sales_commission,0) - nvl (logistics_charges,0) - nvl (tax_amount,0) ELSE item_purchase_price_inc_tax - vendor_funding + royalty_commission + nvl (entry_tax,0) END))/SUM(item_revenue + cashback_redeemed - nvl (tax_amount,0) - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) AS gm_actual,
COUNT(DISTINCT order_group_id) orders_actual,
1.0*SUM(nvl (item_revenue,0) + nvl (cashback_redeemed,0) + nvl (shipping_charge,0) + nvl (gift_charge,0))/COUNT(DISTINCT order_group_id) asp_actual,
count(distinct case when device_channel = 'mobile-app' then order_group_id end) app_orders

FROM bidb.fact_order_live
WHERE (is_realised = 1 OR is_shipped = 1)
and platform_id in (1, 3, 9)
AND (customer_login not ilike '%%myntra360%%' and customer_login not like '%%benchmarking%%' and customer_login not ilike '%%test%%')
and (order_created_date = to_char(date(convert_timezone('UTC-5:30', getdate())),'YYYYMMDD')::bigint)
and ((order_created_date <= 20181225 and order_created_date >= 20181220) or (order_created_date = 20181219 and order_created_time >= 2000))

GROUP BY 1, 2, 3

order by 1, 2, 3

"""

abc = datetime.datetime.now().strftime("%Y-%m-%d")
today_actual_sep=pd.read_sql_query(sql_str,engine)
today_planned_sep = pd.read_csv("hourly_planned_numbers_sep.csv")
today_sep = pd.merge(today_actual_sep, today_planned_sep, how='inner', on = ['date', 'hr', 'platform'])

today_m = today_sep[(today_sep.platform == 'Myntra')]

sql_str="""

select load_date date, hour::bigint, 'Myntra' platform, count(distinct session_id) as app_sessions

from 

(select session_id , app_name, device_id, min(load_date) load_date, min(hour) AS HOUR
from clickstream.sessionhour_view
where load_date = to_char(date(convert_timezone('UTC-5:30', getdate())),'YYYYMMDD')::bigint
and ((load_date <= 20181225 and load_date >= 20181220) or (load_date = 20181219 and hour::bigint >= 20))
and app_name in ('MyntraRetailAndroid', 'Myntra')
group by 1, 2, 3)

group by 1, 2, 3
order by 1, 2

"""

traffic_m=pd.read_sql_query(sql_str,engine)

json_key = json.load(open('My_Project_43976-95951b4cfbd9.json')) # json credentials you downloaded earlier
scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']

credentials = SignedJwtAssertionCredentials(json_key['client_email'], json_key['private_key'].encode(), scope) # get email and key from creds

file = gspread.authorize(credentials) # authenticate with Google
sheet = file.open("BI automation sheet").worksheet("Sheet1")
existing = gd.get_as_dataframe(sheet)
existing = existing.rename(columns = {'=IMPORTRANGE("14PXwpeAWvqT-oA3hgVmWql4Br9J6aqKHO-RUGwW5Jb8","Consolidated!A1:B25")':'hour'})
#existing['hour'] = existing['hour'].apply(lambda x: '{0:0>2}'.format(x))

traffic_m = pd.merge(traffic_m, existing, how='left', on=['hour'])
traffic_m = traffic_m.replace(np.nan, 0, regex=True)
#traffic_m.ix[data.shape[0]-1, 'Mweb+Desktop'] = traffic_m['Mweb+Desktop'].sum()
traffic_m['traffic_actual'] = traffic_m['app_sessions'] + traffic_m['Mweb+Desktop']
traffic_planned_sep = pd.read_csv("traffic_hourly_planned_numbers_sep.csv")
#traffic_m['date'] = traffic_m['date'].astype(int)
#traffic_m = pd.merge(traffic_m, traffic_planned_sep, how='inner', on=['date', 'hour', 'platform'])
traffic_m = traffic_m[['hour','traffic_actual']]
traffic_m.rename(columns={'hour':'hr'}, 
                 inplace=True)

today_m = pd.merge(today_m, traffic_m, how='left', on=['hr'])
traffic_planned_sep.rename(columns={'hour':'hr'}, 
                 inplace=True)
today_m = pd.merge(today_m, traffic_planned_sep, how='left', on=['date', 'hr', 'platform'])
today_m['conversion_actual'] = 100*today_m['orders_actual']/today_m['traffic_actual']
#today_m = today_m.replace(np.nan, '-', regex=True)

conversion = today_m
conversion = conversion[conversion.conversion_actual.notnull()]

try:
	conversion_actual = sum(conversion['conversion_actual']*conversion['revenue_actual'])/sum(conversion['revenue_actual'])
except ZeroDivisionError as err:
	conversion_actual = 0

try:
	conversion_planned = sum(conversion['conversion_planned']*conversion['revenue_actual'])/sum(conversion['revenue_actual'])
except ZeroDivisionError as err:
	conversion_planned = 0

try:
	traffic_actual = sum(conversion['traffic_actual'])
except ZeroDivisionError as err:
	traffic_actual = 0

traffic_planned = sum(conversion['traffic_planned'])

gm_actual = sum(today_m['gm_actual']*today_m['revenue_actual'])/sum(today_m['revenue_actual'])
asp_actual = sum(today_m['asp_actual']*today_m['revenue_actual'])/sum(today_m['revenue_actual'])
gm_planned = sum(today_m['gm_planned']*today_m['revenue_planned'])/sum(today_m['revenue_planned'])
asp_planned = sum(today_m['asp_planned']*today_m['revenue_planned'])/sum(today_m['revenue_planned'])
#conversion_planned = sum(today_m['conversion_planned']*today_m['revenue_planned'])/sum(today_m['revenue_planned'])
today_m.loc[today_m.shape[0]] = today_m.sum()

today_m.iloc[today_m.shape[0]-1, today_m.columns.get_loc('platform')] = 'Myntra'
today_m.iloc[today_m.shape[0]-1, today_m.columns.get_loc('date')] = today_m.iloc[today_m.shape[0]-2, today_m.columns.get_loc('date')]
today_m.iloc[today_m.shape[0]-1, today_m.columns.get_loc('hr')] = 'Till now'
today_m.iloc[today_m.shape[0]-1, today_m.columns.get_loc('gm_actual')] = gm_actual
today_m.iloc[today_m.shape[0]-1, today_m.columns.get_loc('asp_actual')] = asp_actual
today_m.iloc[today_m.shape[0]-1, today_m.columns.get_loc('conversion_actual')] = conversion_actual
today_m.iloc[today_m.shape[0]-1, today_m.columns.get_loc('traffic_actual')] = traffic_actual
today_m.iloc[today_m.shape[0]-1, today_m.columns.get_loc('gm_planned')] = gm_planned
today_m.iloc[today_m.shape[0]-1, today_m.columns.get_loc('asp_planned')] = asp_planned
today_m.iloc[today_m.shape[0]-1, today_m.columns.get_loc('conversion_planned')] = conversion_planned
today_m.iloc[today_m.shape[0]-1, today_m.columns.get_loc('traffic_planned')] = traffic_planned

#today_m = today_m.replace(0, np.nan)

try:
	today_m['conversion_delta'] = 100*(today_m['conversion_actual']-today_m['conversion_planned'])/today_m['conversion_planned']
except ZeroDivisionError as err:
	today_m['conversion_delta'] = 0

#today_m['conversion_delta'] = 100*(today_m['conversion_actual']-today_m['conversion_planned'])/today_m['conversion_actual']
try:
	today_m['traffic_delta'] = 100*(today_m['traffic_actual']-today_m['traffic_planned'])/today_m['traffic_planned']
except ZeroDivisionError as err:
	today_m['traffic_delta'] = 0

#today_m['conversion_delta'] = 100*(today_m['conversion_actual']-today_m['conversion_planned'])/today_m['conversion_actual']
#today_m['traffic_delta'] = 100*(today_m['traffic_actual']-today_m['traffic_planned'])/today_m['traffic_actual']
today_m['revenue_delta'] = 100*(today_m['revenue_actual']-today_m['revenue_planned'])/today_m['revenue_planned']
today_m['gm_delta'] = 100*(today_m['gm_actual']-today_m['gm_planned'])/today_m['gm_planned']
today_m['asp_delta'] = 100*(today_m['asp_actual']-today_m['asp_planned'])/today_m['asp_planned']
today_m['orders_delta'] = 100*(today_m['orders_actual']-today_m['orders_planned'])/today_m['orders_planned']

today_m_1 = today_m[today_m.hr == 'Till now'][['platform', 'revenue_actual', 'revenue_planned', 'gm_actual', 'gm_planned', 'orders_actual', 'orders_planned', 'asp_actual', 'asp_planned']]

today_m['traffic_delta'] = today_m['traffic_delta'].round(1).astype(str) + '%'
today_m['conversion_delta'] = today_m['conversion_delta'].round(1).astype(str) + '%'
today_m['gm_actual'] = today_m['gm_actual'].round(1).astype(str) + '%'
today_m['gm_planned'] = today_m['gm_planned'].round(1).astype(str) + '%'
today_m['conversion_actual'] = today_m['conversion_actual'].astype(float).round(2).astype(str) + '%'
today_m['conversion_planned'] = today_m['conversion_planned'].round(2).astype(str) + '%'
today_m['revenue_delta'] = today_m['revenue_delta'].round(1).astype(str) + '%'
today_m['gm_delta'] = today_m['gm_delta'].round(1).astype(str) + '%'
today_m['asp_delta'] = today_m['asp_delta'].round(1).astype(str) + '%'
today_m['orders_delta'] = today_m['orders_delta'].round(1).astype(str) + '%'

today_m = today_m[['platform', 'hr','revenue_actual','revenue_planned','revenue_delta','gm_actual','gm_planned','gm_delta','orders_actual','orders_planned','orders_delta','asp_actual','asp_planned','asp_delta', 'traffic_actual', 'traffic_planned', 'traffic_delta', 'conversion_actual', 'conversion_planned', 'conversion_delta']]

today_m.rename(columns={'platform':'Platform',
					'hr':'Hour',
					'revenue_actual':'Revenue_Actual',
					'revenue_planned':'Revenue_Planned',
					'revenue_delta':'Revenue_Delta',
					'gm_actual':'GM_Actual',
					'gm_planned':'GM_Planned',
					'gm_delta':'GM_Delta',
					'orders_actual':'Orders_Actual',
					'orders_planned':'Orders_Planned',
					'orders_delta':'Orders_Delta',
					'asp_actual':'ASP_Actual',
					'asp_planned':'ASP_Planned',
					'asp_delta':'ASP_Delta',
					'traffic_actual':'Traffic_Actual', 
					'traffic_planned':'Traffic_Planned', 
					'traffic_delta':'Traffic_Delta', 
					'conversion_actual':'Conversion_Actual', 
					'conversion_planned':'Conversion_Planned', 
					'conversion_delta':'Conversion_Delta'
					}, 
                 inplace=True)

today_m = today_m.round(1)
today_m = today_m.sort_values(['Hour'], ascending=[True])
today_m = today_m.replace(np.nan, '-', regex=True)
today_m = today_m.replace({'nan%':'-'}, regex=True)

today_m['Revenue_Actual'] = today_m['Revenue_Actual']/100000
today_m['Revenue_Planned'] = today_m['Revenue_Planned']/100000

today_m.rename(columns={'Revenue_Actual':'Revenue_Actual (in lacs)',
					'Revenue_Planned':'Revenue_Planned (in lacs)'
					}, 
                 inplace=True)

#Jabong Hourly
today_j = today_sep[(today_sep.platform == 'Jabong')]

sql_str="""

select a.date, hour, platform, 1.0*(android)/(factor)  as traffic_actual

from

(select load_date::bigint date, hour::bigint, 'Jabong' platform, 
count(distinct case when app_name = 'JabongRetailAndroid' then session_id end) android

from 

(select session_id , app_name, device_id, min(load_date) load_date, min(hour) AS HOUR
from clickstream.session_hourly_jabong
where load_date = to_char(date(convert_timezone('UTC-5:30', getdate())),'YYYYMMDD')::bigint
and ((load_date <= 20181225 and load_date >= 20181220) or (load_date = 20181219 and hour::bigint >= 20))
and app_name in ('JabongRetailAndroid', 'JabongRetailiOS')
and screenloads >= 1 
group by 1, 2, 3) as a

group by 1, 2, 3) as a

inner join jabong_traffic_factor as b

on a.date = b.date

order by 1, 2

"""

traffic_j=pd.read_sql_query(sql_str,engine)
traffic_j = traffic_j[['hour','traffic_actual']]
traffic_j.rename(columns={'hour':'hr'}, 
                 inplace=True)

today_j = pd.merge(today_j, traffic_j, how='left', on=['hr'])
traffic_planned_sep.rename(columns={'hour':'hr'}, 
                 inplace=True)
today_j = pd.merge(today_j, traffic_planned_sep, how='inner', on=['date', 'hr', 'platform'])
today_j['conversion_actual'] = 100*today_j['app_orders']/today_j['traffic_actual']

conversion = today_j
conversion = conversion[conversion.conversion_actual.notnull()]
try:
	conversion_actual = sum(conversion['conversion_actual']*conversion['revenue_actual'])/sum(conversion['revenue_actual'])
except:
	conversion_actual = 0

try:
	conversion_planned = sum(conversion['conversion_planned']*conversion['revenue_actual'])/sum(conversion['revenue_actual'])
except ZeroDivisionError as err:
	conversion_planned = 0

try:
	traffic_actual = sum(conversion['traffic_actual'])
except ZeroDivisionError as err:
	traffic_actual = 0

traffic_planned = sum(conversion['traffic_planned'])

gm_actual = sum(today_j['gm_actual']*today_j['revenue_actual'])/sum(today_j['revenue_actual'])
asp_actual = sum(today_j['asp_actual']*today_j['revenue_actual'])/sum(today_j['revenue_actual'])
gm_planned = sum(today_j['gm_planned']*today_j['revenue_planned'])/sum(today_j['revenue_planned'])
asp_planned = sum(today_j['asp_planned']*today_j['revenue_planned'])/sum(today_j['revenue_planned'])
#conversion_planned = sum(today_j['conversion_planned']*today_j['revenue_actual'])/sum(today_j['revenue_actual'])
today_j.loc[today_j.shape[0]] = today_j.sum()

today_j.iloc[today_j.shape[0]-1, today_j.columns.get_loc('platform')] = 'Jabong'
today_j.iloc[today_j.shape[0]-1, today_j.columns.get_loc('date')] = today_j.iloc[today_j.shape[0]-2, today_j.columns.get_loc('date')]
today_j.iloc[today_j.shape[0]-1, today_j.columns.get_loc('hr')] = 'Till now'
today_j.iloc[today_j.shape[0]-1, today_j.columns.get_loc('gm_actual')] = gm_actual
today_j.iloc[today_j.shape[0]-1, today_j.columns.get_loc('asp_actual')] = asp_actual
today_j.iloc[today_j.shape[0]-1, today_j.columns.get_loc('conversion_actual')] = conversion_actual
today_j.iloc[today_m.shape[0]-1, today_j.columns.get_loc('traffic_actual')] = traffic_actual
today_j.iloc[today_j.shape[0]-1, today_j.columns.get_loc('gm_planned')] = gm_planned
today_j.iloc[today_j.shape[0]-1, today_j.columns.get_loc('asp_planned')] = asp_planned
today_j.iloc[today_j.shape[0]-1, today_j.columns.get_loc('conversion_planned')] = conversion_planned
today_j.iloc[today_j.shape[0]-1, today_j.columns.get_loc('traffic_planned')] = traffic_planned

#today_j = today_j.replace(0, np.nan)

try:
	today_j['conversion_delta'] = 100*(today_j['conversion_actual']-today_j['conversion_planned'])/today_j['conversion_planned']
except ZeroDivisionError as err:
	today_j['conversion_delta'] = 0

#today_m['conversion_delta'] = 100*(today_m['conversion_actual']-today_m['conversion_planned'])/today_m['conversion_actual']
try:
	today_j['traffic_delta'] = 100*(today_j['traffic_actual']-today_j['traffic_planned'])/today_j['traffic_planned']
except ZeroDivisionError as err:
	today_j['traffic_delta'] = 0

#today_j['traffic_delta'] = 100*(today_j['traffic_actual']-today_j['traffic_planned'])/today_j['traffic_actual']
#today_j['conversion_delta'] = 100*(today_j['conversion_actual']-today_j['conversion_planned'])/today_j['conversion_actual']
today_j['revenue_delta'] = 100*(today_j['revenue_actual']-today_j['revenue_planned'])/today_j['revenue_planned']
today_j['gm_delta'] = 100*(today_j['gm_actual']-today_j['gm_planned'])/today_j['gm_planned']
today_j['asp_delta'] = 100*(today_j['asp_actual']-today_j['asp_planned'])/today_j['asp_planned']
today_j['orders_delta'] = 100*(today_j['orders_actual']-today_j['orders_planned'])/today_j['orders_planned']

today_j_1 = today_j[today_j.hr == 'Till now'][['platform', 'revenue_actual', 'revenue_planned', 'gm_actual', 'gm_planned', 'orders_actual', 'orders_planned', 'asp_actual', 'asp_planned']]

today_j['traffic_delta'] = today_j['traffic_delta'].round(1).astype(str) + '%'
today_j['conversion_delta'] = today_j['conversion_delta'].round(1).astype(str) + '%'
today_j['gm_actual'] = today_j['gm_actual'].round(1).astype(str) + '%'
today_j['gm_planned'] = today_j['gm_planned'].round(1).astype(str) + '%'
today_j['conversion_actual'] = today_j['conversion_actual'].round(2).astype(str) + '%'
today_j['conversion_planned'] = today_j['conversion_planned'].round(2).astype(str) + '%'
today_j['revenue_delta'] = today_j['revenue_delta'].round(1).astype(str) + '%'
today_j['gm_delta'] = today_j['gm_delta'].round(1).astype(str) + '%'
today_j['asp_delta'] = today_j['asp_delta'].round(1).astype(str) + '%'
today_j['orders_delta'] = today_j['orders_delta'].round(1).astype(str) + '%'

today_j = today_j[['platform', 'hr','revenue_actual','revenue_planned','revenue_delta','gm_actual','gm_planned','gm_delta','orders_actual','orders_planned','orders_delta','asp_actual','asp_planned','asp_delta', 'traffic_actual', 'traffic_planned', 'traffic_delta', 'conversion_actual', 'conversion_planned', 'conversion_delta']]

today_j.rename(columns={'platform':'Platform',
					'hr':'Hour',
					'revenue_actual':'Revenue_Actual',
					'revenue_planned':'Revenue_Planned',
					'revenue_delta':'Revenue_Delta',
					'gm_actual':'GM_Actual',
					'gm_planned':'GM_Planned',
					'gm_delta':'GM_Delta',
					'orders_actual':'Orders_Actual',
					'orders_planned':'Orders_Planned',
					'orders_delta':'Orders_Delta',
					'asp_actual':'ASP_Actual',
					'asp_planned':'ASP_Planned',
					'asp_delta':'ASP_Delta',
					'traffic_actual':'Traffic_Actual', 
					'traffic_planned':'Traffic_Planned', 
					'traffic_delta':'Traffic_Delta', 
					'conversion_actual':'Conversion_Actual', 
					'conversion_planned':'Conversion_Planned', 
					'conversion_delta':'Conversion_Delta'
					}, 
                 inplace=True)

today_j = today_j.round(1)
today_j = today_j.sort_values(['Hour'], ascending=[True])
today_j = today_j.replace(np.nan, '-', regex=True)
today_j = today_j.replace({'nan%':'-'}, regex=True)

today_j['Revenue_Actual'] = today_j['Revenue_Actual']/100000
today_j['Revenue_Planned'] = today_j['Revenue_Planned']/100000

today_j.rename(columns={'Revenue_Actual':'Revenue_Actual (in lacs)',
					'Revenue_Planned':'Revenue_Planned (in lacs)'
					}, 
                 inplace=True)

#Myntra+Jabong Hourly
sql_str="""

select 'Myntra+Jabong' platform, order_created_date date, substring(lpad(order_created_time,4,0),1,2)::bigint hr,
SUM(nvl (item_revenue,0) + nvl (cashback_redeemed,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) AS revenue_actual,
100.0*(SUM(item_revenue + cashback_redeemed - nvl (tax_amount,0) - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) - SUM(CASE WHEN brand = 'Puma' AND seller_id = 28 THEN item_revenue + cashback_redeemed - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0) - nvl (sales_commission,0) - nvl (logistics_charges,0) - nvl (tax_amount,0) ELSE item_purchase_price_inc_tax - vendor_funding + royalty_commission + nvl (entry_tax,0) END))/SUM(item_revenue + cashback_redeemed - nvl (tax_amount,0) - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) AS gm_actual,
COUNT(DISTINCT order_group_id) orders_actual,
1.0*SUM(nvl (item_revenue,0) + nvl (cashback_redeemed,0) + nvl (shipping_charge,0) + nvl (gift_charge,0))/COUNT(DISTINCT order_group_id) asp_actual

FROM bidb.fact_order_live
WHERE (is_realised = 1 OR is_shipped = 1)
and platform_id in (1, 3, 9)
AND (customer_login not ilike '%%myntra360%%' and customer_login not like '%%benchmarking%%' and customer_login not ilike '%%test%%')
and (order_created_date = to_char(date(convert_timezone('UTC-5:30', getdate())),'YYYYMMDD')::bigint)
and ((order_created_date <= 20181225 and order_created_date >= 20181220) or (order_created_date = 20181219 and order_created_time >= 2000))
GROUP BY 1, 2, 3

"""
today_actual_com=pd.read_sql_query(sql_str,engine)
today_planned_com = pd.read_csv("hourly_planned_numbers_com.csv")
today_com = pd.merge(today_actual_com, today_planned_com, how='inner', on = ['date', 'hr'])
gm_actual = sum(today_com['gm_actual']*today_com['revenue_actual'])/sum(today_com['revenue_actual'])
asp_actual = sum(today_com['asp_actual']*today_com['revenue_actual'])/sum(today_com['revenue_actual'])
gm_planned = sum(today_com['gm_planned']*today_com['revenue_planned'])/sum(today_com['revenue_planned'])
asp_planned = sum(today_com['asp_planned']*today_com['revenue_planned'])/sum(today_com['revenue_planned'])
today_com.loc[today_com.shape[0]] = today_com.sum()

today_com.iloc[today_com.shape[0]-1, today_com.columns.get_loc('platform')] = 'Myntra+Jabong'
today_com.iloc[today_com.shape[0]-1, today_com.columns.get_loc('date')] = today_com.iloc[today_com.shape[0]-2, today_com.columns.get_loc('date')]
today_com.iloc[today_com.shape[0]-1, today_com.columns.get_loc('hr')] = 'Till now'
today_com.iloc[today_com.shape[0]-1, today_com.columns.get_loc('gm_actual')] = gm_actual
today_com.iloc[today_com.shape[0]-1, today_com.columns.get_loc('asp_actual')] = asp_actual
today_com.iloc[today_com.shape[0]-1, today_com.columns.get_loc('gm_planned')] = gm_planned
today_com.iloc[today_com.shape[0]-1, today_com.columns.get_loc('asp_planned')] = asp_planned

today_com['revenue_delta'] = 100*(today_com['revenue_actual']-today_com['revenue_planned'])/today_com['revenue_planned']
today_com['gm_delta'] = 100*(today_com['gm_actual']-today_com['gm_planned'])/today_com['gm_planned']
today_com['asp_delta'] = 100*(today_com['asp_actual']-today_com['asp_planned'])/today_com['asp_planned']
today_com['orders_delta'] = 100*(today_com['orders_actual']-today_com['orders_planned'])/today_com['orders_planned']

today_com_1 = today_com[today_com.hr == 'Till now'][['platform', 'revenue_actual', 'revenue_planned', 'gm_actual', 'gm_planned', 'orders_actual', 'orders_planned', 'asp_actual', 'asp_planned']]

today_com['gm_actual'] = today_com['gm_actual'].round(1).astype(str) + '%'
today_com['gm_planned'] = today_com['gm_planned'].round(1).astype(str) + '%'
today_com['revenue_delta'] = today_com['revenue_delta'].round(1).astype(str) + '%'
today_com['gm_delta'] = today_com['gm_delta'].round(1).astype(str) + '%'
today_com['asp_delta'] = today_com['asp_delta'].round(1).astype(str) + '%'
today_com['orders_delta'] = today_com['orders_delta'].round(1).astype(str) + '%'

#today_com['Traffic_Actual'] = today_m['Traffic_Actual'].replace('-', 0) + today_j['Traffic_Actual'].replace('-', 0)
#today_com['Traffic_Planned'] = today_m['Traffic_Planned'].replace('-', 0) + today_j['Traffic_Planned'].replace('-', 0)
#today_com['Conversion_Actual'] = (today_m['Conversion_Actual'].replace({'%':''}, regex=True).replace('-', 0)*today_m['Revenue_Actual'] + today_j['Conversion_Actual'].replace({'%':''}, regex=True).replace('-', 0).astype(float)*today_j['Revenue_Actual'])/today_com['Revenue_Actual']

today_com = today_com[['platform', 'hr','revenue_actual','revenue_planned','revenue_delta','gm_actual','gm_planned','gm_delta','orders_actual','orders_planned','orders_delta','asp_actual','asp_planned','asp_delta']]

today_com.rename(columns={'platform':'Platform',
					'hr':'Hour',
					'revenue_actual':'Revenue_Actual',
					'revenue_planned':'Revenue_Planned',
					'revenue_delta':'Revenue_Delta',
					'gm_actual':'GM_Actual',
					'gm_planned':'GM_Planned',
					'gm_delta':'GM_Delta',
					'orders_actual':'Orders_Actual',
					'orders_planned':'Orders_Planned',
					'orders_delta':'Orders_Delta',
					'asp_actual':'ASP_Actual',
					'asp_planned':'ASP_Planned',
					'asp_delta':'ASP_Delta'}, 
                 inplace=True)

today_com = today_com.round(1)
today_com = today_com.sort_values(['Hour'], ascending=[True])
today_com = today_com.replace(np.nan, '-', regex=True)
today_com = today_com.replace({'nan%':'-'}, regex=True)

today_com['Revenue_Actual'] = today_com['Revenue_Actual']/100000
today_com['Revenue_Planned'] = today_com['Revenue_Planned']/100000

today_com.rename(columns={'Revenue_Actual':'Revenue_Actual (in lacs)',
					'Revenue_Planned':'Revenue_Planned (in lacs)'
					}, 
                 inplace=True)

#Myntra Daily
sql_str="""

select date, platform, sum(revenue) revenue_actual, 100.0*sum(rgm)/sum(gm_den) gm_actual, 1.0*sum(revenue)/sum(orders) asp_actual, 
sum(orders) orders_actual

from

(SELECT order_created_date date, (case when platform_id = 1 then 'Myntra' else 'Jabong' end) platform,
SUM(nvl (item_revenue,0) + nvl (cashback_redeemed,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) AS revenue,
(SUM(item_revenue + cashback_redeemed - nvl (tax_amount,0) - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) - SUM(CASE WHEN brand = 'Puma' AND seller_id = 28 THEN item_revenue + cashback_redeemed - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0) - nvl (sales_commission,0) - nvl (logistics_charges,0) - nvl (tax_amount,0) ELSE item_purchase_price_inc_tax - vendor_funding + royalty_commission + nvl (entry_tax,0) END)) AS rgm,
SUM(item_revenue + cashback_redeemed - nvl (tax_amount,0) - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) gm_den,
COUNT(DISTINCT order_group_id) orders

FROM bidb.fact_order_live
WHERE (is_realised = 1 OR is_shipped = 1)
AND (customer_login not ilike '%%myntra360%%' and customer_login not like '%%benchmarking%%' and customer_login not ilike '%%test%%')
and order_created_date = to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint 
and ((order_created_date <= 20181225 and order_created_date >= 20181220) or (order_created_date = 20181219 and order_created_time >= 2000))
GROUP BY 1, 2

UNION

SELECT order_created_date date, (case when platform_id = 1 then 'Myntra' else 'Jabong' end) platform,
SUM((NVL (item_revenue_inc_cashback,0) + NVL (shipping_charges,0) + NVL (gift_charges,0))) AS revenue,
SUM(((nvl (item_revenue_inc_cashback,0) - nvl (tax,0) + nvl (shipping_charges,0) + nvl (gift_charges,0)) -(nvl (cogs,0) + nvl (royalty_commission,0) + nvl (stn_input_vat_reversal,0) + nvl (entry_tax,0)))) AS rgm,
SUM((nvl (item_revenue_inc_cashback,0) - nvl (tax,0) + NVL (shipping_charges,0) + NVL (gift_charges,0))) gm_den,
COUNT(DISTINCT order_group_id) AS orders

FROM bidb.fact_core_item 
WHERE (is_realised = 1 OR is_shipped = 1)
AND order_created_date <= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '2 day')),'YYYYMMDD')::bigint 
and ((order_created_date <= 20181225 and order_created_date >= 20181220) or (order_created_date = 20181219 and order_created_time >= 200000))
GROUP BY 1, 2) as a

group by 1, 2

order by 2, 1

"""

day_actual_sep=pd.read_sql_query(sql_str,engine)
day_planned_sep = pd.read_csv("daily_planned_numbers_sep.csv")
day_sep = pd.merge(day_actual_sep, day_planned_sep, how='inner', on = ['date', 'platform'])

day_m = day_sep[(day_sep.platform == 'Myntra')]

day_m_1 = day_m[['platform', 'revenue_actual', 'revenue_planned', 'gm_actual', 'gm_planned', 'orders_actual', 'orders_planned', 'asp_actual', 'asp_planned']]

gm_actual = sum(day_m['gm_actual']*day_m['revenue_actual'])/sum(day_m['revenue_actual'])
asp_actual = sum(day_m['asp_actual']*day_m['revenue_actual'])/sum(day_m['revenue_actual'])
gm_planned = sum(day_m['gm_planned']*day_m['revenue_planned'])/sum(day_m['revenue_planned'])
asp_planned = sum(day_m['asp_planned']*day_m['revenue_planned'])/sum(day_m['revenue_planned'])
#day_m.loc[day_m.shape[0]] = day_m.sum()
day_m = day_m.append(day_m.sum(), ignore_index=True)

day_m.iloc[day_m.shape[0]-1, day_m.columns.get_loc('platform')] = 'Myntra'
day_m.iloc[day_m.shape[0]-1, day_m.columns.get_loc('date')] = 'Total'
day_m.iloc[day_m.shape[0]-1, day_m.columns.get_loc('gm_actual')] = gm_actual
day_m.iloc[day_m.shape[0]-1, day_m.columns.get_loc('asp_actual')] = asp_actual
day_m.iloc[day_m.shape[0]-1, day_m.columns.get_loc('gm_planned')] = gm_planned
day_m.iloc[day_m.shape[0]-1, day_m.columns.get_loc('asp_planned')] = asp_planned

day_m['revenue_delta'] = 100*(day_m['revenue_actual']-day_m['revenue_planned'])/day_m['revenue_planned']
day_m['gm_delta'] = 100*(day_m['gm_actual']-day_m['gm_planned'])/day_m['gm_planned']
day_m['asp_delta'] = 100*(day_m['asp_actual']-day_m['asp_planned'])/day_m['asp_planned']
day_m['orders_delta'] = 100*(day_m['orders_actual']-day_m['orders_planned'])/day_m['orders_planned']

day_m['gm_actual'] = day_m['gm_actual'].round(1).astype(str) + '%'
day_m['gm_planned'] = day_m['gm_planned'].round(1).astype(str) + '%'
day_m['revenue_delta'] = day_m['revenue_delta'].round(1).astype(str) + '%'
day_m['gm_delta'] = day_m['gm_delta'].round(1).astype(str) + '%'
day_m['asp_delta'] = day_m['asp_delta'].round(1).astype(str) + '%'
day_m['orders_delta'] = day_m['orders_delta'].round(1).astype(str) + '%'

day_m = day_m[['platform', 'date','revenue_actual','revenue_planned','revenue_delta','gm_actual','gm_planned','gm_delta','orders_actual','orders_planned','orders_delta','asp_actual','asp_planned','asp_delta']]

day_m.rename(columns={'platform':'Platform',
					'date':'Date',
					'revenue_actual':'Revenue_Actual',
					'revenue_planned':'Revenue_Planned',
					'revenue_delta':'Revenue_Delta',
					'gm_actual':'GM_Actual',
					'gm_planned':'GM_Planned',
					'gm_delta':'GM_Delta',
					'orders_actual':'Orders_Actual',
					'orders_planned':'Orders_Planned',
					'orders_delta':'Orders_Delta',
					'asp_actual':'ASP_Actual',
					'asp_planned':'ASP_Planned',
					'asp_delta':'ASP_Delta'
					}, 
                 inplace=True)

day_m = day_m.round(1)
day_m = day_m.sort_values(['Date'], ascending=[True])
day_m['Revenue_Actual'] = day_m['Revenue_Actual']/10000000
day_m['Revenue_Planned'] = day_m['Revenue_Planned']/10000000

day_m.rename(columns={'Revenue_Actual':'Revenue_Actual (in crores)',
					'Revenue_Planned':'Revenue_Planned (in crores)'
					}, 
                 inplace=True)
#day_m = day_m.replace(np.nan, '-', regex=True)
#day_m = day_m.replace({'nan%':'-'}, regex=True)

#Jabong Daily
day_j = day_sep[(day_sep.platform == 'Jabong')]

day_j_1 = day_j[['platform', 'revenue_actual', 'revenue_planned', 'gm_actual', 'gm_planned', 'orders_actual', 'orders_planned', 'asp_actual', 'asp_planned']]

gm_actual = sum(day_j['gm_actual']*day_j['revenue_actual'])/sum(day_j['revenue_actual'])
asp_actual = sum(day_j['asp_actual']*day_j['revenue_actual'])/sum(day_j['revenue_actual'])
gm_planned = sum(day_j['gm_planned']*day_j['revenue_planned'])/sum(day_j['revenue_planned'])
asp_planned = sum(day_j['asp_planned']*day_j['revenue_planned'])/sum(day_j['revenue_planned'])
#day_j.loc[day_j.shape[0]] = day_j.sum()
day_j = day_j.append(day_j.sum(), ignore_index=True)

day_j.iloc[day_j.shape[0]-1, day_j.columns.get_loc('platform')] = 'Jabong'
day_j.iloc[day_j.shape[0]-1, day_j.columns.get_loc('date')] = 'Total'
day_j.iloc[day_j.shape[0]-1, day_j.columns.get_loc('gm_actual')] = gm_actual
day_j.iloc[day_j.shape[0]-1, day_j.columns.get_loc('asp_actual')] = asp_actual
day_j.iloc[day_j.shape[0]-1, day_j.columns.get_loc('gm_planned')] = gm_planned
day_j.iloc[day_j.shape[0]-1, day_j.columns.get_loc('asp_planned')] = asp_planned

day_j['revenue_delta'] = 100*(day_j['revenue_actual']-day_j['revenue_planned'])/day_j['revenue_planned']
day_j['gm_delta'] = 100*(day_j['gm_actual']-day_j['gm_planned'])/day_j['gm_planned']
day_j['asp_delta'] = 100*(day_j['asp_actual']-day_j['asp_planned'])/day_j['asp_planned']
day_j['orders_delta'] = 100*(day_j['orders_actual']-day_j['orders_planned'])/day_j['orders_planned']

day_j['gm_actual'] = day_j['gm_actual'].round(1).astype(str) + '%'
day_j['gm_planned'] = day_j['gm_planned'].round(1).astype(str) + '%'
day_j['revenue_delta'] = day_j['revenue_delta'].round(1).astype(str) + '%'
day_j['gm_delta'] = day_j['gm_delta'].round(1).astype(str) + '%'
day_j['asp_delta'] = day_j['asp_delta'].round(1).astype(str) + '%'
day_j['orders_delta'] = day_j['orders_delta'].round(1).astype(str) + '%'

day_j = day_j[['platform', 'date','revenue_actual','revenue_planned','revenue_delta','gm_actual','gm_planned','gm_delta','orders_actual','orders_planned','orders_delta','asp_actual','asp_planned','asp_delta']]

day_j.rename(columns={'platform':'Platform',
					'date':'Date',
					'revenue_actual':'Revenue_Actual',
					'revenue_planned':'Revenue_Planned',
					'revenue_delta':'Revenue_Delta',
					'gm_actual':'GM_Actual',
					'gm_planned':'GM_Planned',
					'gm_delta':'GM_Delta',
					'orders_actual':'Orders_Actual',
					'orders_planned':'Orders_Planned',
					'orders_delta':'Orders_Delta',
					'asp_actual':'ASP_Actual',
					'asp_planned':'ASP_Planned',
					'asp_delta':'ASP_Delta'
					}, 
                 inplace=True)

day_j = day_j.round(1)
day_j = day_j.sort_values(['Date'], ascending=[True])

day_j['Revenue_Actual'] = day_j['Revenue_Actual']/10000000
day_j['Revenue_Planned'] = day_j['Revenue_Planned']/10000000

day_j.rename(columns={'Revenue_Actual':'Revenue_Actual (in crores)',
					'Revenue_Planned':'Revenue_Planned (in crores)'
					}, 
                 inplace=True)

#Myntra+Jabong Daily
sql_str="""

select date, 'Myntra+Jabong' platform, sum(revenue) revenue_actual, 100.0*sum(rgm)/sum(gm_den) gm_actual, 1.0*sum(revenue)/sum(orders) asp_actual, 
sum(orders) orders_actual

from

(SELECT order_created_date date, 
SUM(nvl (item_revenue,0) + nvl (cashback_redeemed,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) AS revenue,
(SUM(item_revenue + cashback_redeemed - nvl (tax_amount,0) - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) - SUM(CASE WHEN brand = 'Puma' AND seller_id = 28 THEN item_revenue + cashback_redeemed - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0) - nvl (sales_commission,0) - nvl (logistics_charges,0) - nvl (tax_amount,0) ELSE item_purchase_price_inc_tax - vendor_funding + royalty_commission + nvl (entry_tax,0) END)) AS rgm,
SUM(item_revenue + cashback_redeemed - nvl (tax_amount,0) - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) gm_den,
COUNT(DISTINCT order_group_id) orders

FROM bidb.fact_order_live
WHERE (is_realised = 1 OR is_shipped = 1)
AND (customer_login not ilike '%%myntra360%%' and customer_login not like '%%benchmarking%%' and customer_login not ilike '%%test%%')
and order_created_date = to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint 
and ((order_created_date <= 20181225 and order_created_date >= 20181220) or (order_created_date = 20181219 and order_created_time >= 2000))
GROUP BY 1

UNION

SELECT order_created_date date, 
SUM((NVL (item_revenue_inc_cashback,0) + NVL (shipping_charges,0) + NVL (gift_charges,0))) AS revenue,
SUM(((nvl (item_revenue_inc_cashback,0) - nvl (tax,0) + nvl (shipping_charges,0) + nvl (gift_charges,0)) -(nvl (cogs,0) + nvl (royalty_commission,0) + nvl (stn_input_vat_reversal,0) + nvl (entry_tax,0)))) AS rgm,
SUM((nvl (item_revenue_inc_cashback,0) - nvl (tax,0) + NVL (shipping_charges,0) + NVL (gift_charges,0))) gm_den,
COUNT(DISTINCT order_group_id) AS orders

FROM bidb.fact_core_item 
WHERE (is_realised = 1 OR is_shipped = 1)
AND order_created_date <= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '2 day')),'YYYYMMDD')::bigint 
and ((order_created_date <= 20181225 and order_created_date >= 20181220) or (order_created_date = 20181219 and order_created_time >= 2000))
GROUP BY 1) as a

group by 1, 2

order by 1

"""
day_actual_com=pd.read_sql_query(sql_str,engine)
day_planned_com = pd.read_csv("daily_planned_numbers_com.csv")
day_com = pd.merge(day_actual_com, day_planned_com, how='inner', on = ['date'])

day_com_1 = day_com[['platform', 'revenue_actual', 'revenue_planned', 'gm_actual', 'gm_planned', 'orders_actual', 'orders_planned', 'asp_actual', 'asp_planned']]

gm_actual = sum(day_com['gm_actual']*day_com['revenue_actual'])/sum(day_com['revenue_actual'])
asp_actual = sum(day_com['asp_actual']*day_com['revenue_actual'])/sum(day_com['revenue_actual'])
gm_planned = sum(day_com['gm_planned']*day_com['revenue_planned'])/sum(day_com['revenue_planned'])
asp_planned = sum(day_com['asp_planned']*day_com['revenue_planned'])/sum(day_com['revenue_planned'])
#day_com.loc[day_com.shape[0]] = day_com.sum()
day_com = day_com.append(day_com.sum(), ignore_index=True)

day_com.iloc[day_com.shape[0]-1, day_com.columns.get_loc('platform')] = 'Myntra+Jabong'
day_com.iloc[day_com.shape[0]-1, day_com.columns.get_loc('date')] = 'Total'
day_com.iloc[day_com.shape[0]-1, day_com.columns.get_loc('gm_actual')] = gm_actual
day_com.iloc[day_com.shape[0]-1, day_com.columns.get_loc('asp_actual')] = asp_actual
day_com.iloc[day_com.shape[0]-1, day_com.columns.get_loc('gm_planned')] = gm_planned
day_com.iloc[day_com.shape[0]-1, day_com.columns.get_loc('asp_planned')] = asp_planned

day_com['revenue_delta'] = 100*(day_com['revenue_actual']-day_com['revenue_planned'])/day_com['revenue_planned']
day_com['gm_delta'] = 100*(day_com['gm_actual']-day_com['gm_planned'])/day_com['gm_planned']
day_com['asp_delta'] = 100*(day_com['asp_actual']-day_com['asp_planned'])/day_com['asp_planned']
day_com['orders_delta'] = 100*(day_com['orders_actual']-day_com['orders_planned'])/day_com['orders_planned']

day_com['gm_actual'] = day_com['gm_actual'].round(1).astype(str) + '%'
day_com['gm_planned'] = day_com['gm_planned'].round(1).astype(str) + '%'
day_com['revenue_delta'] = day_com['revenue_delta'].round(1).astype(str) + '%'
day_com['gm_delta'] = day_com['gm_delta'].round(1).astype(str) + '%'
day_com['asp_delta'] = day_com['asp_delta'].round(1).astype(str) + '%'
day_com['orders_delta'] = day_com['orders_delta'].round(1).astype(str) + '%'

day_com = day_com[['platform', 'date','revenue_actual','revenue_planned','revenue_delta','gm_actual','gm_planned','gm_delta','orders_actual','orders_planned','orders_delta','asp_actual','asp_planned','asp_delta']]

day_com.rename(columns={'platform':'Platform',
					'date':'Date',
					'revenue_actual':'Revenue_Actual',
					'revenue_planned':'Revenue_Planned',
					'revenue_delta':'Revenue_Delta',
					'gm_actual':'GM_Actual',
					'gm_planned':'GM_Planned',
					'gm_delta':'GM_Delta',
					'orders_actual':'Orders_Actual',
					'orders_planned':'Orders_Planned',
					'orders_delta':'Orders_Delta',
					'asp_actual':'ASP_Actual',
					'asp_planned':'ASP_Planned',
					'asp_delta':'ASP_Delta'
					}, 
                 inplace=True)

day_com = day_com.round(1)
day_com = day_com.sort_values(['Date'], ascending=[True])

day_com['Revenue_Actual'] = day_com['Revenue_Actual']/10000000
day_com['Revenue_Planned'] = day_com['Revenue_Planned']/10000000

day_com.rename(columns={'Revenue_Actual':'Revenue_Actual (in crores)',
					'Revenue_Planned':'Revenue_Planned (in crores)'
					}, 
                 inplace=True)

#Myntra Event Summary
event_m = day_m_1.append(today_m_1, ignore_index=True)
#event_m = pd.concat([today_m_1, day_m_1])
gm_actual = sum(event_m['gm_actual'].astype(float)*event_m['revenue_actual'].astype(float))/sum(event_m['revenue_actual'].astype(float))
gm_planned = sum(event_m['gm_planned'].astype(float)*event_m['revenue_actual'].astype(float))/sum(event_m['revenue_actual'].astype(float))
asp_actual = sum(event_m['asp_actual'].astype(float)*event_m['revenue_actual'].astype(float))/sum(event_m['revenue_actual'].astype(float))
asp_planned = sum(event_m['asp_planned'].astype(float)*event_m['revenue_actual'].astype(float))/sum(event_m['revenue_actual'].astype(float))

event_m.loc[event_m.shape[0]] = event_m.sum()

event_m.iloc[event_m.shape[0]-1, event_m.columns.get_loc('platform')] = 'Myntra'
event_m.iloc[event_m.shape[0]-1, event_m.columns.get_loc('gm_actual')] = gm_actual
event_m.iloc[event_m.shape[0]-1, event_m.columns.get_loc('asp_actual')] = asp_actual
event_m.iloc[event_m.shape[0]-1, event_m.columns.get_loc('gm_planned')] = gm_planned
event_m.iloc[event_m.shape[0]-1, event_m.columns.get_loc('asp_planned')] = asp_planned

event_m = event_m.tail(1)
event_m['revenue_delta'] = 100*(event_m['revenue_actual']-event_m['revenue_planned'])/event_m['revenue_planned']
event_m['gm_delta'] = 100*(event_m['gm_actual']-event_m['gm_planned'])/event_m['gm_planned']
event_m['asp_delta'] = 100*(event_m['asp_actual']-event_m['asp_planned'])/event_m['asp_planned']
event_m['orders_delta'] = 100*(event_m['orders_actual']-event_m['orders_planned'])/event_m['orders_planned']

event_m['gm_actual'] = event_m['gm_actual'].round(1).astype(str) + '%'
event_m['gm_planned'] = event_m['gm_planned'].round(1).astype(str) + '%'
event_m['revenue_delta'] = event_m['revenue_delta'].round(1).astype(str) + '%'
event_m['gm_delta'] = event_m['gm_delta'].round(1).astype(str) + '%'
event_m['asp_delta'] = event_m['asp_delta'].round(1).astype(str) + '%'
event_m['orders_delta'] = event_m['orders_delta'].round(1).astype(str) + '%'

event_m = event_m[['platform', 'revenue_actual','revenue_planned','revenue_delta','gm_actual','gm_planned','gm_delta','orders_actual','orders_planned','orders_delta','asp_actual','asp_planned','asp_delta']]

event_m.rename(columns={'platform':'Platform',
					'revenue_actual':'Revenue_Actual',
					'revenue_planned':'Revenue_Planned',
					'revenue_delta':'Revenue_Delta',
					'gm_actual':'GM_Actual',
					'gm_planned':'GM_Planned',
					'gm_delta':'GM_Delta',
					'orders_actual':'Orders_Actual',
					'orders_planned':'Orders_Planned',
					'orders_delta':'Orders_Delta',
					'asp_actual':'ASP_Actual',
					'asp_planned':'ASP_Planned',
					'asp_delta':'ASP_Delta'
					}, 
                 inplace=True)

event_m['Revenue_Actual'] = event_m['Revenue_Actual']/10000000
event_m['Revenue_Planned'] = event_m['Revenue_Planned']/10000000

event_m.rename(columns={'Revenue_Actual':'Revenue_Actual (in crores)',
					'Revenue_Planned':'Revenue_Planned (in crores)'
					}, 
                 inplace=True)

#Jabong Event Summary
event_j = day_j_1.append(today_j_1, ignore_index=True)
gm_actual = sum(event_j['gm_actual'].astype(float)*event_j['revenue_actual'].astype(float))/sum(event_j['revenue_actual'].astype(float))
gm_planned = sum(event_j['gm_planned'].astype(float)*event_j['revenue_actual'].astype(float))/sum(event_j['revenue_actual'].astype(float))
asp_actual = sum(event_j['asp_actual'].astype(float)*event_j['revenue_actual'].astype(float))/sum(event_j['revenue_actual'].astype(float))
asp_planned = sum(event_j['asp_planned'].astype(float)*event_j['revenue_actual'].astype(float))/sum(event_j['revenue_actual'].astype(float))

event_j.loc[event_j.shape[0]] = event_j.sum()

event_j.iloc[event_j.shape[0]-1, event_j.columns.get_loc('platform')] = 'Jabong'
event_j.iloc[event_j.shape[0]-1, event_j.columns.get_loc('gm_actual')] = gm_actual
event_j.iloc[event_j.shape[0]-1, event_j.columns.get_loc('asp_actual')] = asp_actual
event_j.iloc[event_j.shape[0]-1, event_j.columns.get_loc('gm_planned')] = gm_planned
event_j.iloc[event_j.shape[0]-1, event_j.columns.get_loc('asp_planned')] = asp_planned

event_j = event_j.tail(1)
event_j['revenue_delta'] = 100*(event_j['revenue_actual']-event_j['revenue_planned'])/event_j['revenue_planned']
event_j['gm_delta'] = 100*(event_j['gm_actual']-event_j['gm_planned'])/event_j['gm_planned']
event_j['asp_delta'] = 100*(event_j['asp_actual']-event_j['asp_planned'])/event_j['asp_planned']
event_j['orders_delta'] = 100*(event_j['orders_actual']-event_j['orders_planned'])/event_j['orders_planned']

event_j['gm_actual'] = event_j['gm_actual'].round(1).astype(str) + '%'
event_j['gm_planned'] = event_j['gm_planned'].round(1).astype(str) + '%'
event_j['revenue_delta'] = event_j['revenue_delta'].round(1).astype(str) + '%'
event_j['gm_delta'] = event_j['gm_delta'].round(1).astype(str) + '%'
event_j['asp_delta'] = event_j['asp_delta'].round(1).astype(str) + '%'
event_j['orders_delta'] = event_j['orders_delta'].round(1).astype(str) + '%'

event_j = event_j[['platform', 'revenue_actual','revenue_planned','revenue_delta','gm_actual','gm_planned','gm_delta','orders_actual','orders_planned','orders_delta','asp_actual','asp_planned','asp_delta']]

event_j.rename(columns={'platform':'Platform',
					'revenue_actual':'Revenue_Actual',
					'revenue_planned':'Revenue_Planned',
					'revenue_delta':'Revenue_Delta',
					'gm_actual':'GM_Actual',
					'gm_planned':'GM_Planned',
					'gm_delta':'GM_Delta',
					'orders_actual':'Orders_Actual',
					'orders_planned':'Orders_Planned',
					'orders_delta':'Orders_Delta',
					'asp_actual':'ASP_Actual',
					'asp_planned':'ASP_Planned',
					'asp_delta':'ASP_Delta'
					}, 
                 inplace=True)

event_j['Revenue_Actual'] = event_j['Revenue_Actual']/10000000
event_j['Revenue_Planned'] = event_j['Revenue_Planned']/10000000

event_j.rename(columns={'Revenue_Actual':'Revenue_Actual (in crores)',
					'Revenue_Planned':'Revenue_Planned (in crores)'
					}, 
                 inplace=True)

#Myntra+Jabong Event Summary
event_com = day_com_1.append(today_com_1, ignore_index=True)
gm_actual = sum(event_com['gm_actual'].astype(float)*event_com['revenue_actual'].astype(float))/sum(event_com['revenue_actual'].astype(float))
gm_planned = sum(event_com['gm_planned'].astype(float)*event_com['revenue_actual'].astype(float))/sum(event_com['revenue_actual'].astype(float))
asp_actual = sum(event_com['asp_actual'].astype(float)*event_com['revenue_actual'].astype(float))/sum(event_com['revenue_actual'].astype(float))
asp_planned = sum(event_com['asp_planned'].astype(float)*event_com['revenue_actual'].astype(float))/sum(event_com['revenue_actual'].astype(float))

event_com.loc[event_com.shape[0]] = event_com.sum()

event_com.iloc[event_com.shape[0]-1, event_com.columns.get_loc('platform')] = 'Myntra+Jabong'
event_com.iloc[event_com.shape[0]-1, event_com.columns.get_loc('gm_actual')] = gm_actual
event_com.iloc[event_com.shape[0]-1, event_com.columns.get_loc('asp_actual')] = asp_actual
event_com.iloc[event_com.shape[0]-1, event_com.columns.get_loc('gm_planned')] = gm_planned
event_com.iloc[event_com.shape[0]-1, event_com.columns.get_loc('asp_planned')] = asp_planned

event_com = event_com.tail(1)
event_com['revenue_delta'] = 100*(event_com['revenue_actual']-event_com['revenue_planned'])/event_com['revenue_planned']
event_com['gm_delta'] = 100*(event_com['gm_actual']-event_com['gm_planned'])/event_com['gm_planned']
event_com['asp_delta'] = 100*(event_com['asp_actual']-event_com['asp_planned'])/event_com['asp_planned']
event_com['orders_delta'] = 100*(event_com['orders_actual']-event_com['orders_planned'])/event_com['orders_planned']

event_com['gm_actual'] = event_com['gm_actual'].round(1).astype(str) + '%'
event_com['gm_planned'] = event_com['gm_planned'].round(1).astype(str) + '%'
event_com['revenue_delta'] = event_com['revenue_delta'].round(1).astype(str) + '%'
event_com['gm_delta'] = event_com['gm_delta'].round(1).astype(str) + '%'
event_com['asp_delta'] = event_com['asp_delta'].round(1).astype(str) + '%'
event_com['orders_delta'] = event_com['orders_delta'].round(1).astype(str) + '%'

event_com = event_com[['platform', 'revenue_actual','revenue_planned','revenue_delta','gm_actual','gm_planned','gm_delta','orders_actual','orders_planned','orders_delta','asp_actual','asp_planned','asp_delta']]

event_com.rename(columns={'platform':'Platform',
					'revenue_actual':'Revenue_Actual',
					'revenue_planned':'Revenue_Planned',
					'revenue_delta':'Revenue_Delta',
					'gm_actual':'GM_Actual',
					'gm_planned':'GM_Planned',
					'gm_delta':'GM_Delta',
					'orders_actual':'Orders_Actual',
					'orders_planned':'Orders_Planned',
					'orders_delta':'Orders_Delta',
					'asp_actual':'ASP_Actual',
					'asp_planned':'ASP_Planned',
					'asp_delta':'ASP_Delta'
					}, 
                 inplace=True)

event_com['Revenue_Actual'] = event_com['Revenue_Actual']/10000000
event_com['Revenue_Planned'] = event_com['Revenue_Planned']/10000000

event_com.rename(columns={'Revenue_Actual':'Revenue_Actual (in crores)',
					'Revenue_Planned':'Revenue_Planned (in crores)'
					}, 
                 inplace=True)

#mailer
sender = 'gaurav.choudhary@myntra.com'
receivers = ['shraddha.gupta@myntra.com', 'mteam@myntra.com', 'neha.bhasin@jabong.com', 'soumita.chatterjee@myntra.com', 'harsh.chaudhary@myntra.com', 'ayyappan.rajagopal@myntra.com', 'saurabh.chambhare@jabong.com', 'shreyas.nangia@jabong.com', 'rohit.verma@jabong.com', 'sakshi.bansal@jabong.com', 'vinayak.naik@myntra.com', 'srinivasan.satyamurthy@myntra.com', 'rishi.sharma1@myntra.com','shrinivas.ron@myntra.com','arvind.nair@myntra.com','arunava.chatterjee@myntra.com', 'gaurav.choudhary@myntra.com', 'bhavesh.singhal@myntra.com','growth_core@myntra.com','revenue_analytics@myntra.com']
#receivers = ['gaurav.choudhary@myntra.com']
msg = MIMEMultipart()
msg['Subject'] = 'EORS-9: Hourly Performance Metrics'
msg['From'] = sender
msg['to'] =", ".join(receivers)

m="\nEORS-9: Platform wise Summary (till now):\n \n"
n=event_m.to_html(index=False)
q="\n"
o=event_j.to_html(index=False)
r="\n"
p=event_com.to_html(index=False)
a="\nMyntra Hourly Summary for Date: "+str(abc)+"\n \n"
b=today_m.to_html(index=False)
c="\nJabong Hourly Summary for Date: "+str(abc)+"\n \n"
d=today_j.to_html(index=False)
e="\nMyntra+Jabong Hourly Summary for Date: "+str(abc)+"\n \n"
f=today_com.to_html(index=False)
g="\nMyntra daywise Event summary:\n \n"
h=day_m.to_html(index=False)
i="\nJabong daywise Event summary:\n \n"
j=day_j.to_html(index=False)
k="\nMyntra+Jabong daywise Event summary:\n \n"
l=day_com.to_html(index=False)

part13 = MIMEText(m,'plain')
part14 = MIMEText(n,'html')
part15 = MIMEText(q,'plain')
part16 = MIMEText(o,'html')
part17 = MIMEText(r,'plain')
part18 = MIMEText(p,'html')
part1 = MIMEText(a,'plain')
part2 = MIMEText(b,'html')
part3 = MIMEText(c,'plain')
part4 = MIMEText(d,'html')
part5 = MIMEText(e,'plain')
part6 = MIMEText(f,'html')
part7 = MIMEText(g,'plain')
part8 = MIMEText(h,'html')
part9 = MIMEText(i,'plain')
part10 = MIMEText(j,'html')
part11 = MIMEText(k,'plain')
part12 = MIMEText(l,'html')

msg.attach(part13)
msg.attach(part14)
msg.attach(part15)
msg.attach(part16)
msg.attach(part17)
msg.attach(part18)
msg.attach(part1)
msg.attach(part2)
msg.attach(part3)
msg.attach(part4)
msg.attach(part5)
msg.attach(part6)
msg.attach(part7)
msg.attach(part8)
msg.attach(part9)
msg.attach(part10)
msg.attach(part11)
msg.attach(part12)

try:
   smtpObj = smtplib.SMTP('smtp.gmail.com:587')
   smtpObj.ehlo()
   smtpObj.starttls()
   smtpObj.login("gaurav.choudhary@myntra.com", "evxhmfgyslwcmsfi")
   smtpObj.sendmail(sender, receivers, msg.as_string())
   smtpObj.close()
   print "Successfully sent email"
except :
   print "Error: unable to send email"

