import gspread_dataframe as gd
import json
import gspread
from oauth2client.client import SignedJwtAssertionCredentials
import pandas as pd
import numpy as np
import datetime
from datetime import date, timedelta
from pandas import DataFrame
import sqlalchemy as sq
import smtplib
import email.message
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

engine = sq.create_engine("postgresql+psycopg2://customer_insights_ddl:20180703@ajqoyP@dw-clickstream.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")

sql_str="""
select * 

from 

(select hour, count(distinct session_id) as sessions, 
        count(distinct case when app_name = 'MyntraRetailAndroid' then session_id end) android_sessions,
        count(distinct case when app_name = 'Myntra' then session_id end) ios_sessions,
        round(1.0*count(distinct session_id)/count(distinct device_id),2) as session_user,
        round(1.0*count(distinct case when app_name = 'MyntraRetailAndroid' then session_id end)/count(distinct case when app_name = 'MyntraRetailAndroid' then device_id end), 2) android_sessions_user,
        round(1.0*count(distinct case when app_name = 'Myntra' then session_id end)/count(distinct case when app_name = 'Myntra' then device_id end), 2) ios_sessions_user

from 

(select session_id , app_name, device_id, min(hour) AS HOUR
from clickstream.sessionhour_view
where load_Date = to_char(sysdate , 'YYYYMMDD') ::bigint
and app_name in ('MyntraRetailAndroid', 'Myntra')
group by 1, 2, 3)

group by 1
order by 1)

union 

(select 'Total' as hour, count(distinct session_id ) as sessions,
        count(distinct case when app_name = 'MyntraRetailAndroid' then session_id end) android_sessions,
        count(distinct case when app_name = 'Myntra' then session_id end) ios_sessions,
        round(1.0*count(distinct session_id)/count(distinct device_id),2) as session_user,
        round(1.0*count(distinct case when app_name = 'MyntraRetailAndroid' then session_id end)/count(distinct case when app_name = 'MyntraRetailAndroid' then device_id end), 2) android_sessions_user,
        round(1.0*count(distinct case when app_name = 'Myntra' then session_id end)/count(distinct case when app_name = 'Myntra' then device_id end), 2) ios_sessions_user
from 
clickstream.sessionhour_view
where load_Date = to_char(sysdate , 'YYYYMMDD') ::bigint
and app_name in ('MyntraRetailAndroid', 'Myntra'))

order by 1

"""
abc = (datetime.datetime.now() - datetime.timedelta(minutes=900)).strftime("%Y-%m-%d")

data=pd.read_sql_query(sql_str,engine)

json_key = json.load(open('My_Project_43976-95951b4cfbd9.json')) # json credentials you downloaded earlier
scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']

credentials = SignedJwtAssertionCredentials(json_key['client_email'], json_key['private_key'].encode(), scope) # get email and key from creds

file = gspread.authorize(credentials) # authenticate with Google
sheet = file.open("GA Traffic Automation Sheet").worksheet("Sheet2")
existing = gd.get_as_dataframe(sheet)
existing = existing.rename(columns = {'=IMPORTRANGE("https://docs.google.com/spreadsheets/d/11nI53UDb_bD1t5i1ZIxqYvDXFdZXDpDiYXa5CgOTQtE/edit#gid=29954178","yesterday GA traffic!D1:D25")':'Mweb+Desktop'})
existing['hour'] = existing['hour'].apply(lambda x: '{0:0>2}'.format(x))

data = pd.merge(data, existing, how='left', on=['hour'])
data = data.replace(np.nan, 0, regex=True)
data.ix[data.shape[0]-1, 'Mweb+Desktop'] = data['Mweb+Desktop'].sum()
data = data.rename(columns = {'sessions':'app_sessions'})
data = data.rename(columns = {'session_user':'app_session_user'})
data['total_sessions'] = data['app_sessions'] + data['Mweb+Desktop']

data = data[['hour', 'total_sessions', 'app_sessions', 'android_sessions', 'ios_sessions', 'Mweb+Desktop', 'app_session_user', 'android_sessions_user', 'ios_sessions_user']]

h1 = """

<html>
 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<style type="text/css">
    table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

th{
 background-color: #1e356f;
border: 2px solid BLACK;
    text-align: center;
    padding: 8px;
    color: #D3D3D3;
}

td {
    border: 2px solid BLACK;
    text-align: center;
    padding: 8px;
}

tr {
    background-color: #F5F5F5;
}

</style>
</head>
<body>

Hi All,<br><br>
Please find the hourly summary of sessions data for yesterday:
<br>
<br>
<table>

""" 
h2= MIMEText(data.to_html(index=False), 'html').as_string()[134:]

h3= """ </body> </html> """

h=h1+h2+h3

sender = 'gaurav.choudhary@myntra.com'
receivers = ['neha.malhan@myntra.com', 'rishi.sharma1@myntra.com','shrinivas.ron@myntra.com','aashish.kumar@myntra.com','anuj.sharma@myntra.com','Notificationcore@myntra.com']
#receivers = ['gaurav.choudhary@myntra.com']
msg = email.message.Message()
msg['Subject'] = 'Traffic Summary for yesterday '+str(abc)
msg['From'] =sender
msg['to'] =", ".join(receivers)
msg.add_header('Content-Type', 'text/html')
msg.set_payload(h)

try:
   smtpObj = smtplib.SMTP('smtp.gmail.com:587')
   smtpObj.ehlo()
   smtpObj.starttls()
   smtpObj.login("gaurav.choudhary@myntra.com", "evxhmfgyslwcmsfi")
   smtpObj.sendmail(sender, receivers, msg.as_string())
   smtpObj.close()
   print "Successfully sent email"
except :
   print "Error: unable to send email"
