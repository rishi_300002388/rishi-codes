def color_code(val): 
  global i
  if i%14 == 8:
    if float(val[0:val.find('%')])/100 <= 1:
        color = 'background-color: rgba(204,234,207,0.6); color: green'
    else:
        color = 'background-color: rgba(245,199,205,0.6); color: red'
  elif float(val[0:val.find('%')])/100 < 1:
    color = 'background-color: rgba(245,199,205,0.6); color: red'
  else:
    color = 'background-color: rgba(204,234,207,0.6); color: green'
  i+=1
  return color

def color_code1(val): 
  global j
  if j%14 == 8:
    if val[0] == '-':
        color = 'background-color: rgba(204,234,207,0.6); color: green'
    else:
        color = 'background-color: rgba(245,199,205,0.6); color: red'
  elif val[0] == '-':
    color = 'background-color: rgba(245,199,205,0.6); color: red'
  else:
    color = 'background-color: rgba(204,234,207,0.6); color: green'
  j+=1
  return color

import gspread_dataframe as gd
import json
from premailer import transform
import gspread
from oauth2client.client import SignedJwtAssertionCredentials
import pandas as pd
import numpy as np
import datetime
from datetime import date, timedelta
from datetime import datetime
from pandas import DataFrame
import sqlalchemy as sq
import smtplib
import email.message
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
#engine = sq.create_engine("postgresql+psycopg2://customer_insights_ddl:20180703@ajqoyP@dw-clickstream.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")
engine = sq.create_engine("postgresql+psycopg2://foci_admin:password@dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")

sql_str="""
delete from customer_insights.gaurav_executive_summary_foci 
where date between to_char(date(convert_timezone('UTC-5:30', getdate() - interval '55 day')),'YYYYMMDD')::bigint 
and to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint;

insert into customer_insights.gaurav_executive_summary_foci

select order_created_date date, SUM((NVL (item_revenue_inc_cashback,0) + NVL (shipping_charges,0) + NVL (gift_charges,0))) AS revenue,
SUM(case when brand_type = 'Private' then (NVL (item_revenue_inc_cashback,0) + NVL (shipping_charges,0) + NVL (gift_charges,0)) end) AS mfb_revenue,
SUM(case when brand_type != 'Private' then (NVL (item_revenue_inc_cashback,0) + NVL (shipping_charges,0) + NVL (gift_charges,0)) end) AS mmb_revenue,
1.0*SUM((NVL (item_revenue_inc_cashback,0) + NVL (shipping_charges,0) + NVL (gift_charges,0)))/COUNT(DISTINCT order_group_id) asp,
COUNT(DISTINCT order_group_id) orders,
100.0*SUM(((nvl (item_revenue_inc_cashback,0) - nvl (tax,0) + nvl (shipping_charges,0) + nvl (gift_charges,0)) -(nvl (cogs,0) + nvl (royalty_commission,0) + nvl (stn_input_vat_reversal,0) + nvl (entry_tax,0))))/SUM((nvl (item_revenue_inc_cashback,0) - nvl (tax,0) + NVL (shipping_charges,0) + NVL (gift_charges,0))) gm,
100.0*(sum(product_discount) + sum(coupon_discount) + sum(1.0*loyalty_pts_used/2))/sum(item_mrp_value) overall_discount,
(sum(product_discount) + sum(coupon_discount) + sum(1.0*loyalty_pts_used/2)) overall_discount_num,
sum(product_discount) product_discount,
sum(coupon_discount) coupon_discount,
sum(1.0*loyalty_pts_used/2) loyalty_discount,
sum(item_mrp_value) mrp,
1.0*SUM(((nvl (item_revenue_inc_cashback,0) - nvl (tax,0) + nvl (shipping_charges,0) + nvl (gift_charges,0)) -(nvl (cogs,0) + nvl (royalty_commission,0) + nvl (stn_input_vat_reversal,0) + nvl (entry_tax,0))))/sum(quantity) gm_num,
SUM((nvl (item_revenue_inc_cashback,0) - nvl (tax,0) + NVL (shipping_charges,0) + NVL (gift_charges,0))) den_gm,
sum(rgm_final) rgm,
1.0*SUM((NVL (item_revenue_inc_cashback,0) + NVL (shipping_charges,0) + NVL (gift_charges,0)))/sum(quantity) aisp,
sum(quantity) quantity,
SUM(case when is_first_order = 'Y' then (NVL (item_revenue_inc_cashback,0) + NVL (shipping_charges,0) + NVL (gift_charges,0)) end) AS acq_revenue,
count(distinct case when is_first_order = 'Y' then idcustomer end) AS acq_customers

from bidb.fact_core_item 
WHERE (is_realised = 1 OR is_shipped = 1)
and platform_id = 1
and order_created_date between to_char(date(convert_timezone('UTC-5:30', getdate() - interval '55 day')),'YYYYMMDD')::bigint 
and to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint

group by 1;
"""
connection = engine.connect()
connection.execute(sql_str)
connection.close()

sql_str="""delete from customer_insights.gaurav_daily_installs where date >= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '2 day')),'YYYYMMDD')::bigint;
insert into customer_insights.gaurav_daily_installs

select date, count(distinct device_id) installs

from

(select device_id, TO_CHAR(created_datetime,'YYYYMMDD')::BIGINT date

from app_install_tune

where TO_CHAR(created_datetime,'YYYYMMDD')::BIGINT between to_char(date(convert_timezone('UTC-5:30', getdate() - interval '2 day')),'YYYYMMDD')::bigint
and to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint
AND attribution = 'Install'

group by 1, 2

union 

select device_id, TO_CHAR(created_datetime,'YYYYMMDD')::BIGINT date

from app_install_appsflyer

where TO_CHAR(created_datetime,'YYYYMMDD')::BIGINT between to_char(date(convert_timezone('UTC-5:30', getdate() - interval '2 day')),'YYYYMMDD')::bigint
and to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint
and platform_id = 1

group by 1, 2) as a

group by 1;
"""

connection = engine.connect()
connection.execute(sql_str)
connection.close()

sql_str="""
select date::varchar date, revenue, mmb_revenue, mfb_revenue, traffic, 100.0*orders/traffic as conversion, asp, gm, overall_discount, 
rgm, aisp, acq_revenue, acq_customers, c.installs

from

(select date, revenue, mmb_revenue, mfb_revenue, asp, gm, overall_discount, 1.0*rgm/quantity rgm, aisp, acq_revenue, acq_customers, orders

from customer_insights.gaurav_executive_summary_foci

where date = to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint) as a

cross join 

(select sum(sessions) traffic from customer_insights.daily_traffic where load_date = to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint) as b

cross join

(select installs from customer_insights.gaurav_daily_installs

where date = to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint) as c;

"""

yesterday=pd.read_sql_query(sql_str,engine)

yesterday.rename(columns={'platform':'Platform',
          'revenue':'Revenue',
          'mmb_revenue':'MMB - Revenue',
          'mfb_revenue':'MFB - Revenue',
          'traffic':'Traffic',
          'conversion':'Conversion',
          'asp':'ASP',
          'gm':'GM',
          'overall_discount':'Discounts',
          'rgm':'RGM/unit',
          'aisp':'ISP',
          'acq_revenue':'Acquisitions rev',
          'acq_customers':'Acquisitions cust',
          'installs':'Installs'
          },
                 inplace=True)
                 
sql_str="""
select 'MTD Actuals' date, revenue, mmb_revenue, mfb_revenue, traffic, 100.0*orders/traffic as conversion, asp, gm, overall_discount, rgm, aisp, acq_revenue, acq_customers,
c.installs

from

(select sum(revenue) revenue, sum(mmb_revenue) mmb_revenue, sum(mfb_revenue) mfb_revenue, 1.0*sum(revenue)/sum(orders) asp, 100.0*sum(gm_num*quantity)/sum(den_gm) gm, 
100.0*sum(overall_discount_num)/sum(mrp) overall_discount, 1.0*sum(rgm)/sum(quantity) rgm, 1.0*sum(revenue)/sum(quantity) aisp, sum(acq_revenue) acq_revenue, 
sum(acq_customers) acq_customers, sum(orders) orders

from customer_insights.gaurav_executive_summary_foci

where date <= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint
and substring(date, 5, 2) = substring(to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint, 5, 2)
and substring(date, 1, 4) = substring(to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint, 1, 4)) as a

cross join

(select sum(sessions) traffic from customer_insights.daily_traffic 
where load_date <= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint
and substring(load_date, 5, 2) = substring(to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint, 5, 2)
and substring(load_date, 1, 4) = substring(to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint, 1, 4)) as b

cross join

(select sum(installs) installs from customer_insights.gaurav_daily_installs

where date <= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint
and substring(date, 5, 2) = substring(to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint, 5, 2)
and substring(date, 1, 4) = substring(to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint, 1, 4)) as c;

"""

mtd=pd.read_sql_query(sql_str,engine)

mtd.rename(columns={'platform':'Platform',
          'revenue':'Revenue',
          'mmb_revenue':'MMB - Revenue',
          'mfb_revenue':'MFB - Revenue',
          'traffic':'Traffic',
          'conversion':'Conversion',
          'asp':'ASP',
          'gm':'GM',
          'overall_discount':'Discounts',
          'rgm':'RGM/unit',
          'aisp':'ISP',
          'acq_revenue':'Acquisitions rev',
          'acq_customers':'Acquisitions cust',
          'installs':'Installs'
          },
                 inplace=True)

sql_str="""
select 'MTD LY' date, revenue, mmb_revenue, mfb_revenue, traffic, 100.0*orders/traffic as conversion, asp, gm, overall_discount, rgm, aisp, acq_revenue, acq_customers,
c.installs

from

(select sum(revenue) revenue, sum(mmb_revenue) mmb_revenue, sum(mfb_revenue) mfb_revenue, 1.0*sum(revenue)/sum(orders) asp, 100.0*sum(gm_num*quantity)/sum(den_gm) gm, 
100.0*sum(overall_discount_num)/sum(mrp) overall_discount, 1.0*sum(rgm)/sum(quantity) rgm, 1.0*sum(revenue)/sum(quantity) aisp, sum(acq_revenue) acq_revenue, 
sum(acq_customers) acq_customers, sum(orders) orders

from customer_insights.gaurav_executive_summary_foci

where date <= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '366 day')),'YYYYMMDD')::bigint
and substring(date, 5, 2) = substring(to_char(date(convert_timezone('UTC-5:30', getdate() - interval '366 day')),'YYYYMMDD')::bigint, 5, 2)
and substring(date, 1, 4) = substring(to_char(date(convert_timezone('UTC-5:30', getdate() - interval '366 day')),'YYYYMMDD')::bigint, 1, 4)) as a

cross join

(select sum(sessions) traffic from customer_insights.daily_traffic 
where load_date <= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '366 day')),'YYYYMMDD')::bigint
and substring(load_date, 5, 2) = substring(to_char(date(convert_timezone('UTC-5:30', getdate() - interval '366 day')),'YYYYMMDD')::bigint, 5, 2)
and substring(load_date, 1, 4) = substring(to_char(date(convert_timezone('UTC-5:30', getdate() - interval '366 day')),'YYYYMMDD')::bigint, 1, 4)) as b

cross join

(select sum(installs) installs from customer_insights.gaurav_daily_installs

where date <= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '366 day')),'YYYYMMDD')::bigint
and substring(date, 5, 2) = substring(to_char(date(convert_timezone('UTC-5:30', getdate() - interval '366 day')),'YYYYMMDD')::bigint, 5, 2)
and substring(date, 1, 4) = substring(to_char(date(convert_timezone('UTC-5:30', getdate() - interval '366 day')),'YYYYMMDD')::bigint, 1, 4)) as c;

"""

mtd_ly=pd.read_sql_query(sql_str,engine)

mtd_ly.rename(columns={'platform':'Platform',
          'revenue':'Revenue',
          'mmb_revenue':'MMB - Revenue',
          'mfb_revenue':'MFB - Revenue',
          'traffic':'Traffic',
          'conversion':'Conversion',
          'asp':'ASP',
          'gm':'GM',
          'overall_discount':'Discounts',
          'rgm':'RGM/unit',
          'aisp':'ISP',
          'acq_revenue':'Acquisitions rev',
          'acq_customers':'Acquisitions cust',
          'installs':'Installs'
          },
                 inplace=True)

json_key = json.load(open('/myntra/intelligence/current_repo/MyntraDW/scripts/My_Project_43976-95951b4cfbd9.json')) # json credentials you downloaded earlier
scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']

credentials = SignedJwtAssertionCredentials(json_key['client_email'], json_key['private_key'].encode(), scope) # get email and key from creds

file = gspread.authorize(credentials) # authenticate with Google
sheet = file.open("Plan for Daily report").worksheet("Executive Summary")
existing = gd.get_as_dataframe(sheet)

existing = existing.dropna()

existing = pd.concat([existing.iloc[:,[0,1,2,3,4,6,9,10,11,12,13,14]].astype(int),existing.iloc[:,[5,7,8]]], axis = 1)

yesterday_plan = existing[existing.date.astype(int).astype(str) == yesterday.iloc[yesterday.shape[0]-1, yesterday.columns.get_loc('date')]]
ed = str(yesterday.iloc[yesterday.shape[0]-1, yesterday.columns.get_loc('date')])
sd = datetime.strptime(ed, '%Y%m%d').replace(day=1).strftime('%Y%m%d')

mtd_plan = existing[(existing['date'].astype(int) >= int(sd)) & (existing['date'].astype(int) <= int(ed))]

conversion = sum(mtd_plan['Conversion']*mtd_plan['Traffic'])/sum(mtd_plan['Traffic'])
asp = sum(mtd_plan['Revenue'])/((sum(mtd_plan['Conversion']*mtd_plan['Traffic']))/100)
gm = sum(mtd_plan['GM']*mtd_plan['Revenue'])/sum(mtd_plan['Revenue'])
discount = sum(mtd_plan['Discounts']*mtd_plan['Revenue'])/sum(mtd_plan['Revenue'])
rgm = sum(mtd_plan['RGM/unit']*mtd_plan['Revenue'])/sum(mtd_plan['Revenue'])
isp = sum(mtd_plan['ISP']*mtd_plan['Revenue'])/sum(mtd_plan['Revenue'])

mtd_plan.loc[mtd_plan.shape[0]] = mtd_plan.sum()
mtd_plan.iloc[mtd_plan.shape[0]-1, mtd_plan.columns.get_loc('date')] = 'MTD Plan'
mtd_plan.iloc[mtd_plan.shape[0]-1, mtd_plan.columns.get_loc('Conversion')] = conversion
mtd_plan.iloc[mtd_plan.shape[0]-1, mtd_plan.columns.get_loc('ASP')] = asp
mtd_plan.iloc[mtd_plan.shape[0]-1, mtd_plan.columns.get_loc('GM')] = gm
mtd_plan.iloc[mtd_plan.shape[0]-1, mtd_plan.columns.get_loc('Discounts')] = discount
mtd_plan.iloc[mtd_plan.shape[0]-1, mtd_plan.columns.get_loc('RGM/unit')] = rgm
mtd_plan.iloc[mtd_plan.shape[0]-1, mtd_plan.columns.get_loc('ISP')] = isp

mtd_plan = mtd_plan[mtd_plan.date == 'MTD Plan']

yesterday.date = pd.to_datetime(yesterday.date, format='%Y%m%d')
yesterday.date = yesterday.date.apply(lambda x: x.strftime('%d-%b'))
yesterday['date'] = yesterday['date'].astype(str) + ' Actuals'

yesterday_plan.date = pd.to_datetime(yesterday_plan.date, format='%Y%m%d')
yesterday_plan.date = yesterday_plan.date.apply(lambda x: x.strftime('%d-%b'))
yesterday_plan['date'] = yesterday_plan['date'].astype(str) + ' Plan'
yesterday_plan = yesterday_plan.reset_index(drop=True)

yesterday['Units'] = yesterday['Revenue']/yesterday['ISP']
mtd['Units'] = mtd['Revenue']/mtd['ISP']
mtd_ly['Units'] = mtd_ly['Revenue']/mtd_ly['ISP']

final = pd.concat([yesterday_plan[yesterday.columns.tolist()], yesterday, mtd_plan[yesterday.columns.tolist()], mtd, mtd_ly])
#final[['ASP', 'RGM/unit', 'ISP']] = final[['ASP', 'RGM/unit', 'ISP']].astype(int)

final = final[[u'date', u'Revenue', u'MMB - Revenue', u'MFB - Revenue', u'Traffic',
       u'Conversion', u'Units', u'ASP', u'GM', u'Discounts', u'RGM/unit', u'ISP',
       u'Acquisitions rev', u'Acquisitions cust', u'Installs']]

final_t = final.loc[:,final.columns != 'date'].T
final_t.columns = final.date.tolist()
final_t['Metrics'] = final_t.index.tolist()
final_t = final_t[['Metrics'] + final.date.tolist()]

final_t['Achieved (%)'] = 100*(final_t[yesterday['date'].loc[0]])/final_t[yesterday_plan['date'].loc[0]]
final_t['MTD Achieved (%)'] = 100*(final_t['MTD Actuals'])/final_t['MTD Plan']
final_t['MTD growth YoY'] = 100*(final_t['MTD Actuals']-final_t['MTD LY'])/final_t['MTD LY']

check_list = final_t['Achieved (%)'].loc[['Revenue', 'Traffic', 'Conversion', 'ASP']].tolist()

a = 0

for i in range(0,len(check_list)):
  if check_list[i] < -12:
    a = 1
    break

final_t = final_t[['Metrics', yesterday_plan['date'].loc[0], yesterday['date'].loc[0], 'Achieved (%)', 'MTD Plan', 'MTD Actuals', 'MTD Achieved (%)', 'MTD LY', 'MTD growth YoY']]

final_t['Achieved (%)'] = final_t['Achieved (%)'].round(1).astype(str) + '%'
final_t['MTD Achieved (%)'] = final_t['MTD Achieved (%)'].round(1).astype(str) + '%'
final_t['MTD growth YoY'] = final_t['MTD growth YoY'].round(1).astype(str) + '%'

final_t.iloc[[4,7,8], [1,2,4,5,7]] = final_t[[yesterday['date'].loc[0], yesterday_plan['date'].loc[0], 'MTD Actuals', 'MTD Plan', 'MTD LY']].loc[['Conversion', 'GM', 'Discounts']].round(2).astype(str) + '%'

final_t.iloc[[0,1,2],[1,2,4,5,7]] = final_t.iloc[[0,1,2],[1,2,4,5,7]]/10000000
final_t.iloc[[3],[1,2,4,5,7]] = final_t.iloc[[3],[1,2,4,5,7]]/1000000
final_t.iloc[[5],[1,2,4,5,7]] = final_t.iloc[[5],[1,2,4,5,7]]/100000
final_t.iloc[[11],[1,2,4,5,7]] = final_t.iloc[[11],[1,2,4,5,7]]/10000000
final_t.iloc[[12,13],[1,2,4,5,7]] = final_t.iloc[[12,13],[1,2,4,5,7]]/1000
final_t.iloc[[0,1,2],[0]] = final_t.iloc[[0,1,2],[0]] + ' (in crs)'
final_t.iloc[[3],[0]] = final_t.iloc[[3],[0]] + ' (in millions)'
final_t.iloc[[5],[0]] = final_t.iloc[[5],[0]] + ' (in lacs)'
final_t.iloc[[11],[0]] = final_t.iloc[[11],[0]] + ' (in crs)'
final_t.iloc[[12,13],[0]] = final_t.iloc[[12,13],[0]] + ' (in Ks)'

if final_t.isnull().any().any() == 1:
    a = 1

final_t.iloc[[0,1,2,3,5,6,9,10,11,12],[1,2,4,5,7]] = (final_t.iloc[[0,1,2,3,5,6,9,10,11,12],[1,2,4,5,7]]*10).astype(int)/10.0

pd.options.display.float_format = '{:,.1f}'.format

discount = final_t[['Achieved (%)', 'MTD Achieved (%)', 'MTD growth YoY']].loc['Discounts'].tolist()

final_t.index = final_t['Metrics'].tolist()

final_t = final_t.iloc[:,[1,2,3,4,5,6,7,8]]

final_t.iloc[[6,9,10],[0,1,3,4,6]] = (final_t.iloc[[6,9,10],[0,1,3,4,6]]).astype(int)

final_t.iloc[[6,10],[0,1,3,4,6]] = final_t.iloc[[6,10],[0,1,3,4,6]].applymap('{:,}'.format)

i = 0
j = 0

final_t_html = final_t.style.applymap(lambda x: color_code(x), subset=['Achieved (%)', 'MTD Achieved (%)']).applymap(lambda x: color_code1(x), subset=['MTD growth YoY'])

table_style = ' border="2" cellpadding="5"'
cell_style =' align="center" valign="middle"'

final_t_html = transform(final_t_html.render()).replace('<table','<table'+table_style).replace('<td','<td'+cell_style)

abc = (datetime.now()- timedelta(1)).strftime("%Y-%m-%d")

HEADER = """

<html>
 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<style>
    table {
    font-family: calibiri, sans-serif;
    border-collapse: collapse;
    width: 100%;
    }

    th{
    background-color: #1e356f;
    border: 1px solid BLACK;
    text-align: center;
    padding: 5px;
    color: #D3D3D3;
    }

    td {
    border: 1px solid BLACK;
    text-align: center;
    font-size: small;
    padding: 5px;
    }

    tr {
        background-color: #F5F5F5;
    }    

    tr:nth-child(even) {
        background-color: #dddddd;
    }

    tr:nth-child(odd) {
        background-color: #F5F5F5;
    }

</style>
</head>
<body>

<h4>Executive Summary:</h4>
"""

FOOTER = """
<html>
<body>
<hr></hr>
<h4>Notes:</h4>
<sup id="fn1">1. Revenue reported is net of cancellations.</sup>
<br/>
<sup id="fn1">2. Acquisition revenue/customer is for their first orders only.</sup>
<br/>
<sup id="fn1">2. Forwarding this report will disorder formating.</sup>

</body>
</html>
 """

doubleNewline = '<br><br>'

final_html = HEADER + \
final_t_html + doubleNewline + \
FOOTER

#h2= MIMEText(final_t.to_html(index=False), 'html').as_string()[134:]

#MIMEText(final_t_html.to_html(index=False), 'html').as_string()[134:]

#h3= """ </body> </html> """

#h=h1+h2+h3

sender = 'insights@myntra.com'
msg = MIMEMultipart()
#receivers = ['engg_bi@myntra.com', 'gaurav.choudhary@myntra.com']

if a == 1:
    receivers = ['bhavesh.singhal@myntra.com','anuj.sharma@myntra.com','mohit.panjwani@myntra.com','phaneendra.pamarti@myntra.com','vinayak.naik@myntra.com','srinivasan.satyamurthy@myntra.com','neha.malhan@myntra.com','shrinivas.ron@myntra.com','rishi.sharma1@myntra.com','nirag.gosalia@myntra.com','gaurav.choudhary@myntra.com','engg_bi@myntra.com','dhritiman.mitra@myntra.com']
    #receivers = ['shivam.goyal@myntra.com','gaurav.choudhary@myntra.com', 'nirag.gosalia@myntra.com']
    #bcc = ['bhavesh.singhal@myntra.com','anuj.sharma@myntra.com','mohit.panjwani@myntra.com','phaneendra.pamarti@myntra.com','vinayak.naik@myntra.com','srinivasan.satyamurthy@myntra.com','neha.malhan@myntra.com','shrinivas.ron@myntra.com','rishi.sharma1@myntra.com','nirag.gosalia@myntra.com']
    msg['Subject'] = 'Alert: Myntra Executive Summary: '+str(abc)
else:
    receivers = ['leadership@myntra.com','bhavesh.singhal@myntra.com','anuj.sharma@myntra.com','mohit.panjwani@myntra.com','phaneendra.pamarti@myntra.com','vinayak.naik@myntra.com','srinivasan.satyamurthy@myntra.com','neha.malhan@myntra.com','shrinivas.ron@myntra.com','rishi.sharma1@myntra.com','nirag.gosalia@myntra.com','gaurav.choudhary@myntra.com','engg_bi@myntra.com','dhritiman.mitra@myntra.com']
    #receivers = ['shivam.goyal@myntra.com','gaurav.choudhary@myntra.com', 'nirag.gosalia@myntra.com']
    #bcc = ['executive_report@myntra.com']
    msg['Subject'] = 'Myntra Executive Summary: '+str(abc)


msg['From'] = sender
msg['to'] =", ".join(receivers)
#msg['bcc'] =", ".join(bcc)

part2 = MIMEText(final_html, 'html')

msg.attach(part2)

#msg.add_header('Content-Type', 'text/html')
#msg.set_payload(h)

try:
   smtpObj = smtplib.SMTP('localhost')
   smtpObj.sendmail(sender, receivers, msg.as_string())
   smtpObj.close()
   print "Successfully sent email"
except :
   print "Error: unable to send email"
