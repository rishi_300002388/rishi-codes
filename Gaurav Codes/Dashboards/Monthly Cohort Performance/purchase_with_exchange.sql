select b.order_created_date date, (case when a.month_bucket is null then 'M-0' else a.month_bucket end) month_bucket, 
(case when a.purchase_sequence is null then '0' else a.purchase_sequence end) purchase_sequence, c.city_tier, 
(case when f.gender is null then 'unk' else f.gender end) gender,
(case when b.device_channel = 'mobile-app' and b.os_info = 'Android' then 'Android'
    when b.device_channel = 'mobile-app' and b.os_info = 'iOS' then 'iOS' else 'Website' end) as os_info,
count(distinct b.idcustomer) purchase_repeat_users,
sum(nvl(b.shipping_charges, 0) + nvl(b.gift_charges, 0) + nvl(b.item_revenue_inc_cashback,0)) revenue,
count(distinct b.order_group_id) orders, sum(b.quantity) quantity,
sum(nvl(b.item_revenue_inc_cashback,0)+nvl(b.shipping_charges,0)+nvl(b.gift_charges,0) - nvl(b.tax,0) - nvl(b.cogs,0)-nvl(b.royalty_commission,0)-nvl(b.stn_input_vat_reversal,0)- nvl(b.entry_tax,0)) as num_gm,
sum(nvl(b.item_revenue_inc_cashback,0)+nvl(b.shipping_charges,0)+nvl(b.gift_charges,0) - nvl(b.tax,0)) as den_gm, 
sum(b.product_discount) product_discount, sum(b.coupon_discount) coupon_discount, sum(b.item_mrp_value) mrp

from

(select b.order_created_date, b.device_channel, b.os_info, b.idcustomer, b.shipping_charges, b.gift_charges, b.item_revenue_inc_cashback, b.order_group_id, b.quantity,
b.tax, b.cogs, b.royalty_commission, b.stn_input_vat_reversal, b.entry_tax, b.product_discount, b.coupon_discount, b.item_mrp_value,
(case when b.device_channel is null and b.os_info is null then 'both_null' else 'not_null' end) null_flag
from fact_core_item as b
where b.order_created_date = 20180723
and (is_shipped = 1 or is_realised = 1)
and b.platform_id = 1
and store_id = 1
and b.exchange_release_id is null

union all

select a.order_created_date, b.device_channel, b.os_info, a.idcustomer, a.shipping_charges, a.gift_charges, a.item_revenue_inc_cashback, a.order_group_id, a.quantity,
a.tax, a.cogs, a.royalty_commission, a.stn_input_vat_reversal, a.entry_tax, a.product_discount, a.coupon_discount, a.item_mrp_value,
(case when b.device_channel is null and b.os_info is null then 'both_null' else 'not_null' end) null_flag

from

(select exchange_release_id, order_created_date, idcustomer, shipping_charges, gift_charges, item_revenue_inc_cashback, order_group_id, quantity, tax, cogs, royalty_commission,
stn_input_vat_reversal, entry_tax, product_discount, coupon_discount, item_mrp_value
from fact_core_item as b
where order_created_date = 20180723
and (is_shipped = 1 or is_realised = 1)
and b.platform_id = 1
and store_id = 1
and exchange_release_id is not null) as a

inner join fact_core_item as b

on a.exchange_release_id = b.order_id) as b

inner join dim_customer_idea as e

on b.idcustomer = e.id

left join fact_user_lifetime as f

on e.customer_login = f.uidx

inner join dim_location as c

on b.idlocation = c.id

left join customer_insights.gaurav_month_cohort as a

on b.idcustomer = a.idcustomer
and a.year = '2018' and a.month = '07'

where null_flag = 'not_null'

group by 1, 2, 3, 4, 5, 6;