DAY="$(date "+%d")"
YEAR="$(date "+%Y")"
MONTH="$(date "+%m")"
DATE="$(date "+%Y%m%d")"

source /home/apollo/.config
export PGPASSWORD=$REDSHIFT_PASSWORD

if [[ $DAY == "01" ]];
then
                /usr/bin/psql -h $CLICKSTREAM_CLUSTER -U $USER -d $DATABASE -p 5439 -t -c  "insert into customer_insights.gaurav_month_cohort

                select $YEAR, '$MONTH', idcustomer, order_created_date, 
                (case when purchase_sequence = 1 then '1'
                          when purchase_sequence between 2 and 5 then '2-5'
                          when purchase_sequence between 6 and 10 then '6-10' else '10+' end) purchase_sequence, uidx, city_tier, gender, os_info, order_group_id,
                (case when month_bucket = 1 then 'M-1'
                          when month_bucket = 2 then 'M-2'
                          when month_bucket = 3 then 'M-3'
                          when month_bucket in (4, 5, 6) then 'M-4-6'
                          when month_bucket in (7, 8, 9, 10, 11, 12) then 'M-7-12' else 'M12+' end) month_bucket

                from

                (select idcustomer, order_created_date, purchase_sequence, row_number() over(partition by idcustomer order by order_created_date desc) rank_number,
                        datediff(months, date(order_created_date), date($DATE)) month_bucket, order_group_id, c.city_tier, f.gender, f.uidx,
                        (case when device_channel = 'mobile-app' and os_info = 'Android' then 'Android'
                              when device_channel = 'mobile-app' and os_info = 'iOS' then 'iOS' else 'Website' end) as os_info

                from customer_purchase_sequence_detailed as b

                inner join dim_customer_idea as e

                on b.idcustomer = e.id

                left join fact_user_lifetime as f

                on e.customer_login = f.uidx

                inner join dim_location as c

                on b.idlocation = c.id

                where b.order_created_date < $DATE
                and ((b.device_channel = 'mobile-app' and b.os_info in ('Android', 'iOS')) or b.device_channel in ('mobile-web', 'web'))) as a

                where rank_number = 1;"
fi

/usr/bin/psql -h $CLICKSTREAM_CLUSTER -U $USER -d $DATABASE -p 5439 -w -f /myntra/customer_insights/clickstream_jobs/sql_files/monthly_cohort_performance.sql

if [ $? -gt 0 ]
then
        echo "Either there is a Redshift DB connectivity issue or there might be an error in the script "
        exit 1;
else
        echo "Ran Successfully"
fi