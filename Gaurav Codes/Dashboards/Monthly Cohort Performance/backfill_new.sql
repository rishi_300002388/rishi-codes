delete from customer_insights.gaurav_monthly_cohort_purchase where date>=20181001;
insert into customer_insights.gaurav_monthly_cohort_purchase

select b.order_created_date date, (case when a.month_bucket is null then 'M-0' else a.month_bucket end) month_bucket, 
(case when a.purchase_sequence is null then '0' else a.purchase_sequence end) purchase_sequence, c.city_tier, 
(case when f.gender is null then 'unk' else f.gender end) gender, 
(case when b.device_channel = 'mobile-app' and b.os_info = 'Android' then 'Android'
    when b.device_channel = 'mobile-app' and b.os_info = 'iOS' then 'iOS' else 'Website' end) as os_info,
count(distinct b.idcustomer) purchase_repeat_users,
sum(nvl(b.shipping_charges, 0) + nvl(b.gift_charges, 0) + nvl(b.item_revenue_inc_cashback,0)) revenue,
count(distinct b.order_group_id) orders, sum(b.quantity) quantity,
sum(nvl(b.item_revenue_inc_cashback,0)+nvl(b.shipping_charges,0)+nvl(b.gift_charges,0) - nvl(b.tax,0) - nvl(b.cogs,0)-nvl(b.royalty_commission,0)-nvl(b.stn_input_vat_reversal,0)- nvl(b.entry_tax,0)) as num_gm,
sum(nvl(b.item_revenue_inc_cashback,0)+nvl(b.shipping_charges,0)+nvl(b.gift_charges,0) - nvl(b.tax,0)) as den_gm, 
sum(b.product_discount) product_discount, sum(b.coupon_discount) coupon_discount, sum(b.item_mrp_value) mrp

from 

(select b.order_created_date, b.device_channel, b.os_info, b.idcustomer, b.shipping_charges, b.gift_charges, b.item_revenue_inc_cashback, b.order_group_id, b.quantity,
b.tax, b.cogs, b.royalty_commission, b.stn_input_vat_reversal, b.entry_tax, b.product_discount, b.coupon_discount, b.item_mrp_value,
(case when b.device_channel is null and b.os_info is null then 'both_null' else 'not_null' end) null_flag, idlocation
from fact_core_item as b
where b.order_created_date >= 20181001 and to_char(sysdate - interval '1 day','YYYYMMDD')::bigint
and (is_shipped = 1 or is_realised = 1)
and b.platform_id = 1
and store_id = 1
and b.exchange_release_id is null

union all

select a.order_created_date, b.device_channel, b.os_info, a.idcustomer, a.shipping_charges, a.gift_charges, a.item_revenue_inc_cashback, a.order_group_id, a.quantity,
a.tax, a.cogs, a.royalty_commission, a.stn_input_vat_reversal, a.entry_tax, a.product_discount, a.coupon_discount, a.item_mrp_value,
(case when b.device_channel is null and b.os_info is null then 'both_null' else 'not_null' end) null_flag, a.idlocation

from

(select exchange_release_id, order_created_date, idcustomer, shipping_charges, gift_charges, item_revenue_inc_cashback, order_group_id, quantity, tax, cogs, royalty_commission,
stn_input_vat_reversal, entry_tax, product_discount, coupon_discount, item_mrp_value, idlocation
from fact_core_item as b
where b.order_created_date >= 20181001 and to_char(sysdate - interval '1 day','YYYYMMDD')::bigint
and (is_shipped = 1 or is_realised = 1)
and b.platform_id = 1
and store_id = 1
and exchange_release_id is not null) as a

inner join fact_core_item as b

on a.exchange_release_id = b.order_id) as b

inner join dim_date as d

on b.order_created_date = d.full_date

inner join dim_customer_idea as e

on b.idcustomer = e.id

left join fact_user_lifetime as f

on e.customer_login = f.uidx

inner join dim_location as c

on b.idlocation = c.id

left join customer_insights.gaurav_month_cohort as a

on b.idcustomer = a.idcustomer and d.year = a.year and d.month = a.month::BIGINT

where null_flag = 'not_null'

group by 1, 2, 3, 4, 5, 6;

-------
delete from customer_insights.gaurav_monthly_cohort_browse where date>=20181001;
insert into customer_insights.gaurav_monthly_cohort_browse

select load_date date, (case when c.month_bucket is null then 'M-0' else c.month_bucket end) month_bucket,
(case when c.purchase_sequence is null then '0' else c.purchase_sequence end) purchase_sequence, b.city_tier, 
(case when f.gender is null then 'unk' else f.gender end) gender, a.os_info,
sum(a.sessions) sessions,
count(distinct a.uidx) browse_repeat_users

from

(select load_date, uidx, (case when os in ('Android','android') then 'Android'
							   when os in ('iOS','iPhone OS') then 'iOS' else 'unknown' end) as os_info, sessions, city
	from daily_aggregates as a
	where load_date between 20181001 and to_char(sysdate - interval '1 day','YYYYMMDD')::bigint
	and uidx is not null
	and os in ('android','Android','iOS','iPhone OS')

union all

select load_date, uidx, 'Website' as os_info, sessions, city
	from daily_aggregates_web
	where load_date between 20181001 and to_char(sysdate - interval '1 day','YYYYMMDD')::bigint
	and uidx is not null
	and os in ('Desktop', 'Mweb')) as a

inner join dim_date as d

on a.load_date = d.full_date

left join fact_user_lifetime as f

on a.uidx = f.uidx

inner join dev.reapollo_new_6oct_city_mapping_cs as b

on lower(a.city) = lower(b.city)

left join customer_insights.gaurav_month_cohort as c

on a.uidx = c.uidx and d.year = c.year and d.month = c.month::BIGINT

group by 1, 2, 3, 4, 5, 6;

------
delete from customer_insights.gaurav_monthly_cohort_dashboard where date>=20181001;
insert into customer_insights.gaurav_monthly_cohort_dashboard

select a.date, d.week_of_the_year week, upper(d.month_string) as month, d.year, 1 platform_id, a.month_bucket, a.purchase_sequence, a.city_tier, a.gender, a.os_info, 
a.sessions, c.cohort_base, a.browse_repeat_users, b.purchase_repeat_users, b.revenue, b.orders, b.quantity, b.num_gm, b.den_gm, b.product_discount, 
b.coupon_discount, b.mrp

from customer_insights.gaurav_monthly_cohort_browse as a

inner join dim_date as d

on a.date = d.full_date
and a.date >= 20181101

left join customer_insights.gaurav_monthly_cohort_purchase as b

on a.date = b.date and a.month_bucket = b.month_bucket and a.purchase_sequence = b.purchase_sequence and a.city_tier = b.city_tier and a.gender = b.gender 
and a.os_info = b.os_info and b.date >= 20181001

left join 

(select year, month, month_bucket, purchase_sequence, city_tier, gender, os_info, count(distinct uidx) cohort_base

from customer_insights.gaurav_month_cohort as c

group by 1, 2, 3, 4, 5, 6, 7) as c

on a.month_bucket = c.month_bucket and a.purchase_sequence = c.purchase_sequence and a.city_tier = c.city_tier and a.gender = c.gender 
and a.os_info = c.os_info and d.year = c.year and d.month = c.month::BIGINT;


