delete from customer_insights.gaurav_month_cohort where month = '07';
insert into customer_insights.gaurav_month_cohort

select '2018', '07', idcustomer, order_created_date, 
(case when purchase_sequence = 1 then '1'
      when purchase_sequence between 2 and 5 then '2-5'
      when purchase_sequence between 6 and 10 then '6-10' else '10+' end) purchase_sequence, uidx, city_tier, gender, os_info, order_group_id,
(case when month_bucket = 1 then 'M-1'
      when month_bucket = 2 then 'M-2'
      when month_bucket = 3 then 'M-3'
      when month_bucket in (4, 5, 6) then 'M-4-6'
      when month_bucket in (7, 8, 9, 10, 11, 12) then 'M-7-12' else 'M12+' end) month_bucket

from

(select idcustomer, order_created_date, purchase_sequence, row_number() over(partition by idcustomer order by order_created_date desc) rank_number,
        datediff(months, date(order_created_date), date('20180701')) month_bucket, order_group_id, c.city_tier, 
        (case when f.gender is null then 'unk' else f.gender end) gender, f.uidx,
        (case when device_channel = 'mobile-app' and os_info = 'Android' then 'Android'
              when device_channel = 'mobile-app' and os_info = 'iOS' then 'iOS' else 'Website' end) as os_info

from customer_purchase_sequence_detailed as b

inner join dim_customer_idea as e

on b.idcustomer = e.id

left join fact_user_lifetime as f

on e.customer_login = f.uidx

inner join dim_location as c

on b.idlocation = c.id

where b.order_created_date < 20180701
and ((b.device_channel = 'mobile-app' and b.os_info in ('Android', 'iOS')) or b.device_channel in ('mobile-web', 'web'))) as a

where rank_number = 1;

-----
delete from customer_insights.gaurav_monthly_cohort_purchase where date=20180723;
insert into customer_insights.gaurav_monthly_cohort_purchase

select b.order_created_date date, (case when a.month_bucket is null then 'M-0' else a.month_bucket end) month_bucket, 
(case when a.purchase_sequence is null then '0' else a.purchase_sequence end) purchase_sequence, c.city_tier, 
(case when f.gender is null then 'unk' else f.gender end) gender,
(case when b.device_channel = 'mobile-app' and b.os_info = 'Android' then 'Android'
    when b.device_channel = 'mobile-app' and b.os_info = 'iOS' then 'iOS' else 'Website' end) as os_info,
count(distinct b.idcustomer) purchase_repeat_users,
sum(nvl(b.shipping_charges, 0) + nvl(b.gift_charges, 0) + nvl(b.item_revenue_inc_cashback,0)) revenue,
count(distinct b.order_group_id) orders, sum(b.quantity) quantity,
sum(nvl(b.item_revenue_inc_cashback,0)+nvl(b.shipping_charges,0)+nvl(b.gift_charges,0) - nvl(b.tax,0) - nvl(b.cogs,0)-nvl(b.royalty_commission,0)-nvl(b.stn_input_vat_reversal,0)- nvl(b.entry_tax,0)) as num_gm,
sum(nvl(b.item_revenue_inc_cashback,0)+nvl(b.shipping_charges,0)+nvl(b.gift_charges,0) - nvl(b.tax,0)) as den_gm, 
sum(b.product_discount) product_discount, sum(b.coupon_discount) coupon_discount, sum(b.item_mrp_value) mrp

from fact_core_item as b

inner join dim_customer_idea as e

on b.idcustomer = e.id

left join fact_user_lifetime as f

on e.customer_login = f.uidx

inner join dim_location as c

on b.idlocation = c.id

left join customer_insights.gaurav_month_cohort as a

on b.idcustomer = a.idcustomer
and a.year = '2018' and a.month = '07'

where b.order_created_date = 20180723
and (is_shipped = 1 or is_realised = 1)
and b.platform_id = 1
and store_id = 1
and ((b.device_channel = 'mobile-app' and b.os_info in ('Android', 'iOS')) or b.device_channel in ('mobile-web', 'web'))

group by 1, 2, 3, 4, 5, 6;

-------
delete from customer_insights.gaurav_monthly_cohort_browse where date=20180723;
insert into customer_insights.gaurav_monthly_cohort_browse

select load_date date, (case when c.month_bucket is null then 'M-0' else c.month_bucket end) month_bucket,
(case when c.purchase_sequence is null then '0' else c.purchase_sequence end) purchase_sequence, b.city_tier, 
(case when f.gender is null then 'unk' else f.gender end) gender,  a.os_info,
sum(a.sessions) sessions,
count(distinct a.uidx) browse_repeat_users

from

(select load_date, uidx, (case when os in ('Android','android') then 'Android'
                 when os in ('iOS','iPhone OS') then 'iOS' else 'unknown' end) as os_info, sessions, city
  from daily_aggregates as a
  where load_date = 20180723
  and uidx is not null
  and os in ('android','Android','iOS','iPhone OS')

union all

select load_date, uidx, 'Website' as os_info, sessions, city
  from daily_aggregates_web
  where load_date = 20180723
  and uidx is not null
  and os in ('Desktop', 'Mweb')) as a

left join fact_user_lifetime as f

on a.uidx = f.uidx

inner join dev.reapollo_new_6oct_city_mapping_cs as b

on lower(a.city) = lower(b.city)

left join customer_insights.gaurav_month_cohort as c

on a.uidx = c.uidx
and c.year = '2018' and c.month = '07'

group by 1, 2, 3, 4, 5, 6;

------
delete from customer_insights.gaurav_monthly_cohort_dashboard where date = 20180723;
insert into customer_insights.gaurav_monthly_cohort_dashboard

select a.date, d.week_of_the_year week, upper(d.month_string) as month, d.year, 1 platform_id, a.month_bucket, a.purchase_sequence, a.city_tier, a.gender, a.os_info, 
a.sessions, c.cohort_base, a.browse_repeat_users, b.purchase_repeat_users, b.revenue, b.orders, b.quantity, b.num_gm, b.den_gm, b.product_discount, 
b.coupon_discount, b.mrp

from customer_insights.gaurav_monthly_cohort_browse as a

inner join dim_date as d

on a.date = d.full_date
and a.date = 20180723

left join customer_insights.gaurav_monthly_cohort_purchase as b

on a.date = b.date and a.month_bucket = b.month_bucket and a.purchase_sequence = b.purchase_sequence and a.city_tier = b.city_tier and a.gender = b.gender 
and a.os_info = b.os_info and b.date = 20180723

left join 

(select month_bucket, purchase_sequence, city_tier, gender, os_info, count(distinct uidx) cohort_base

from customer_insights.gaurav_month_cohort as c

where c.year = '2018' and c.month = '07'

group by 1, 2, 3, 4, 5) as c

on a.month_bucket = c.month_bucket and a.purchase_sequence = c.purchase_sequence and a.city_tier = c.city_tier and a.gender = c.gender 
and a.os_info = c.os_info;