source /home/apollo/.config
export PGPASSWORD=$REDSHIFT_PASSWORD

/usr/bin/psql -h $CLICKSTREAM_CLUSTER -U $USER -d $DATABASE -p 5439 -w -f /myntra/customer_insights/clickstream_jobs/sql_files/BFD_18_shout_earn_performance_dashboard.sql

if [ $? -gt 0 ]
then
        echo "Either there is a Redshift DB connectivity issue or there might be an error in the script "
        exit 1;
else
        echo "Ran Successfully"
fi