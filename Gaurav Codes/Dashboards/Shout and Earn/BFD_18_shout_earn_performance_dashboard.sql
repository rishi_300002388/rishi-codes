BEGIN;

----

drop table referrer_details_table_1;
create table customer_insights.referrer_details_table_1 as

select referrer_user_id

from user_rewards_audit AS a

WHERE TRANSACTION_TIMESTAMP >= '2018-12-19T00:00:00'
and TRANSACTION_TIMESTAMP < '2018-12-26T00:00:00'
and status = 'SUCCESS'

group by 1;

----
drop table referee_details_table_1;
create table customer_insights.referee_details_table_1 as

select friend_user_id

from user_rewards_audit AS a

WHERE TRANSACTION_TIMESTAMP >= '2018-12-19T00:00:00'
and TRANSACTION_TIMESTAMP < '2018-12-26T00:00:00'
and status = 'SUCCESS'

group by 1;

------
drop table customer_insights.user_referrer_friends_flag_bfd_2018;
create table customer_insights.user_referrer_friends_flag_bfd_2018 as 

select distinct b.user_id, b.flag, b.referrer_user_id, a.referrer_reward

from

(select user_id, flag, referrer_user_id

from

(select user_id, TRANSACTION_TIMESTAMP, referrer_user_id, flag, row_number() over(partition by user_id order by TRANSACTION_TIMESTAMP) rw

from

(select *

from

(select referrer_user_id user_id, TRANSACTION_TIMESTAMP, row_number() over(partition by referrer_user_id order by TRANSACTION_TIMESTAMP) rw, 'referrer' flag, 
	'null' referrer_user_id
from user_rewards_audit
where status = 'SUCCESS'
and TRANSACTION_TIMESTAMP >= '2018-12-19T00:00:00'
and TRANSACTION_TIMESTAMP < '2018-12-26T00:00:00') as a

where rw = 1

union 

select *

from

(select friend_user_id user_id, TRANSACTION_TIMESTAMP, row_number() over(partition by friend_user_id order by TRANSACTION_TIMESTAMP) rw, 'friend' flag, referrer_user_id
from user_rewards_audit
where status = 'SUCCESS'
and TRANSACTION_TIMESTAMP >= '2018-12-19T00:00:00'
and TRANSACTION_TIMESTAMP < '2018-12-26T00:00:00') as a

where rw = 1) as a) as a

where rw = 1

group by 1, 2, 3) as b

inner join

(select user_id, sum(referrer_reward) referrer_reward

from

(select referrer_user_id user_id, sum(referrer_reward) referrer_reward
from user_rewards_audit
where status = 'SUCCESS'
and TRANSACTION_TIMESTAMP >= '2018-12-19T00:00:00'
and TRANSACTION_TIMESTAMP < '2018-12-26T00:00:00'
group by 1

union 

select friend_user_id user_id, sum(referrer_reward) referrer_reward
from user_rewards_audit
where status = 'SUCCESS'
and TRANSACTION_TIMESTAMP >= '2018-12-19T00:00:00'
and TRANSACTION_TIMESTAMP < '2018-12-26T00:00:00'
group by 1) as a

group by 1) as a

on b.user_id = a.user_id;

-----Referrer
DROP TABLE customer_insights.referrer_details_table;
CREATE TABLE customer_insights.referrer_details_table distkey(referrer_user_id) AS

SELECT DISTINCT referrer_user_id,
       gender,
       CASE
         WHEN registered_age BETWEEN 16 AND 25 THEN '16-25'
         WHEN registered_age BETWEEN 26 AND 35 THEN '26-35'
         WHEN registered_age BETWEEN 36 AND 45 THEN '36-45'
         WHEN registered_age BETWEEN 46 AND 55 THEN '46-55'
         WHEN registered_age BETWEEN 56 AND 70 THEN '56+'
         ELSE 'null'
       END AS age_bucket,
       no_of_successful_shares,
       CASE
         WHEN no_of_successful_shares <= 3 THEN 'a.<=3 shares'
         WHEN no_of_successful_shares BETWEEN 4 AND 6 THEN 'b.4-6 shares'
         WHEN no_of_successful_shares BETWEEN 7 AND 10 THEN 'c.7-10 shares'
         WHEN no_of_successful_shares > 10 THEN 'd.10+ shares'
       END AS successful_share_bucket,
       no_of_shares_attempted_and_clicked,
       referrer_reward,
       orders,
       quantity,
       loyalty_points_used,
        nvl(CASE WHEN referrer_reward >=loyalty_points_used THEN loyalty_points_used::FLOAT WHEN referrer_reward <loyalty_points_used 
        THEN referrer_reward::FLOAT END,0) AS referrer_loyalty_pts_used,
       revenue,
       NVL(revenue,0)/NULLIF(NVL(orders,0),0) AS referrer_ASP,
       loyalty_revenue,
       RGM,
       GM_denominator
FROM (SELECT A.referrer_user_id,
             gender,
             CASE
               WHEN datediff (YEAR,TO_DATE(LPAD(RIGHT (dob,8),8,'00000000'),'YYYYMMDD'),CURRENT_DATE) BETWEEN 16 AND 70 THEN datediff (YEAR,TO_DATE(LPAD(RIGHT (dob,8),8,'00000000'),'YYYYMMDD'),CURRENT_DATE)
               ELSE NULL
             END AS registered_age,
             COUNT(DISTINCT A.friend_user_id) AS no_of_shares_attempted_and_clicked,
             COUNT(DISTINCT CASE WHEN status = 'SUCCESS' THEN A.friend_user_id END) AS no_of_successful_shares,
             SUM(CASE WHEN status = 'SUCCESS' THEN A.referrer_reward END) AS referrer_reward
      
      FROM user_rewards_audit AS A

      inner join user_referrer_friends_flag_bfd_2018 as b

      on a.referrer_user_id = b.user_id

      LEFT JOIN dim_customer_idea AS dci ON a.referrer_user_id = dci.customer_login

      WHERE TRANSACTION_TIMESTAMP >= '2018-12-19T00:00:00'
      and TRANSACTION_TIMESTAMP < '2018-12-26T00:00:00'
      and   b.flag = 'referrer'

      GROUP BY 1, 2, 3) R

  LEFT JOIN (SELECT uidx,
                    SUM(orders) AS orders,
                    SUM(quantity) AS quantity,
                    SUM(loyalty_points_used) AS loyalty_points_used,
                    SUM(revenue) AS revenue,
                    SUM(loyalty_revenue) AS loyalty_revenue,
                    SUM(RGM) AS RGM,
                    SUM(GM_denominator) AS GM_denominator
             FROM ((SELECT customer_login AS uidx,
                           COUNT(DISTINCT order_group_id) AS orders,
                           SUM(item_quantity) AS quantity,
                           SUM(loyalty_used) AS loyalty_points_used,
                           SUM(nvl (item_revenue,0) + nvl (cashback_redeemed,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) AS revenue,
                           SUM(CASE WHEN loyalty_used >0 THEN (nvl (item_revenue,0) + nvl (cashback_redeemed,0) + nvl (shipping_charge,0)
                            + nvl (gift_charge,0))END) AS loyalty_revenue,
                           (SUM(item_revenue + cashback_redeemed - nvl (tax_amount,0) - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) - SUM(CASE WHEN brand = 'Puma' AND seller_id = 28 THEN item_revenue + cashback_redeemed - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0) - nvl (sales_commission,0) - nvl (logistics_charges,0) - nvl (tax_amount,0) ELSE item_purchase_price_inc_tax - vendor_funding + royalty_commission + nvl (entry_tax,0) END)) AS RGM,
                           SUM(item_revenue + cashback_redeemed - nvl (tax_amount,0) - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) AS GM_denominator
                    FROM fact_order_live
                    WHERE (is_realised = 1 OR is_shipped = 1)
                    AND (customer_login not ilike '%myntra360%' and customer_login not like '%benchmarking%' and customer_login not ilike '%test%')
                    and (order_created_date >= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint)
                    and ((order_created_date <= 20181225 and order_created_date >= 20181220) or (order_created_date = 20181219 and order_created_time >= 2000))
                    GROUP BY 1)
                    UNION
                    (SELECT customer_login AS uidx,
                           COUNT(DISTINCT order_group_id) AS orders,
                           SUM(quantity) AS quantity,
                           SUM(Loyalty_pts_used) AS loyalty_points_used,
                           SUM((NVL (item_revenue_inc_cashback,0) + NVL (shipping_charges,0) + NVL (gift_charges,0))) AS revenue,
                           SUM(CASE WHEN Loyalty_pts_used >0 THEN (NVL (item_revenue_inc_cashback,0) + NVL (shipping_charges,0) + NVL (gift_charges,0)) END) AS loyalty_revenue,
                           SUM(((nvl (item_revenue_inc_cashback,0) - nvl (tax,0) + nvl (shipping_charges,0) + nvl (gift_charges,0)) -(nvl (cogs,0) + nvl (royalty_commission,0) + nvl (stn_input_vat_reversal,0) + nvl (entry_tax,0)))) AS RGM,
                           SUM((nvl (item_revenue_inc_cashback,0) - nvl (tax,0) + NVL (shipping_charges,0) + NVL (gift_charges,0))) AS GM_denominator
                    FROM fact_core_item AS fci
                      JOIN dim_customer_idea AS dci ON fci.idcustomer = dci.id
                    WHERE (is_realised = 1 OR is_shipped = 1)
                    AND order_created_date <= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '2 day')),'YYYYMMDD')::bigint 
                    and ((order_created_date <= 20181225 and order_created_date >= 20181220) or (order_created_date = 20181219 and order_created_time >= 2000))
                    AND   store_id = 1
                    GROUP BY 1))
             GROUP BY 1) fol ON R.referrer_user_id = fol.uidx;

-----------Referee
DROP TABLE if exists customer_insights.referee_details_table;
CREATE TABLE customer_insights.referee_details_table distkey(referrer_user_id) AS

SELECT DISTINCT friend_user_id,
       referrer_user_id,
       friend_reward,
       gender,
       CASE
         WHEN registered_age BETWEEN 16 AND 25 THEN '16-25'
         WHEN registered_age BETWEEN 26 AND 35 THEN '26-35'
         WHEN registered_age BETWEEN 36 AND 45 THEN '36-45'
         WHEN registered_age BETWEEN 46 AND 55 THEN '46-55'
         WHEN registered_age BETWEEN 56 AND 70 THEN '56+'
         ELSE 'null'
       END AS age_bucket,
       orders,
       quantity,
       loyalty_points_used,
          nvl(CASE WHEN friend_reward >=loyalty_points_used THEN loyalty_points_used::FLOAT 
        WHEN friend_reward <loyalty_points_used THEN friend_reward::FLOAT END,0) AS referee_loyalty_pts_used,
       revenue,
       loyalty_revenue,
       NVL(revenue,0)/NULLIF(NVL(orders,0),0) AS referee_ASP,
       RGM,
       GM_denominator

FROM (SELECT DISTINCT A.friend_user_id,
             A.referrer_user_id,
             gender,
             CASE
               WHEN datediff (YEAR,TO_DATE(LPAD(RIGHT (dob,8),8,'00000000'),'YYYYMMDD'),CURRENT_DATE) BETWEEN 16 AND 70 THEN datediff (YEAR,TO_DATE(LPAD(RIGHT (dob,8),8,'00000000'),'YYYYMMDD'),CURRENT_DATE)
               ELSE NULL
             END AS registered_age,
             A.friend_reward
      FROM user_rewards_audit AS A

      inner join user_referrer_friends_flag_bfd_2018 as b

      on A.friend_user_id = b.user_id

      LEFT JOIN dim_customer_idea AS dci ON a.friend_user_id = dci.customer_login

      WHERE status = 'SUCCESS'
      AND   TRANSACTION_TIMESTAMP >= '2018-12-19T00:00:00'
      and   TRANSACTION_TIMESTAMP < '2018-12-26T00:00:00'
      and   b.flag = 'friend') R

LEFT JOIN (SELECT uidx,
                    SUM(orders) AS orders,
                    SUM(quantity) AS quantity,
                    SUM(loyalty_points_used) AS loyalty_points_used,
                    SUM(revenue) AS revenue,
                    SUM(loyalty_revenue) AS loyalty_revenue,
                    SUM(RGM) AS RGM,
                    SUM(GM_denominator) AS GM_denominator
             FROM ((SELECT customer_login AS uidx,
                           COUNT(DISTINCT order_group_id) AS orders,
                           SUM(item_quantity) AS quantity,
                           SUM(loyalty_used) AS loyalty_points_used,
                           SUM(nvl (item_revenue,0) + nvl (cashback_redeemed,0)) AS revenue,
                           SUM(CASE WHEN loyalty_used >0 THEN (nvl (item_revenue,0) + nvl (cashback_redeemed,0) + nvl (shipping_charge,0)
                            + nvl (gift_charge,0))END) AS loyalty_revenue,
                           (SUM(item_revenue + cashback_redeemed - nvl (tax_amount,0) - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) - SUM(CASE WHEN brand = 'Puma' AND seller_id = 28 THEN item_revenue + cashback_redeemed - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0) - nvl (sales_commission,0) - nvl (logistics_charges,0) - nvl (tax_amount,0) ELSE item_purchase_price_inc_tax - vendor_funding + royalty_commission + nvl (entry_tax,0) END)) AS RGM,
                           SUM(item_revenue + cashback_redeemed - nvl (tax_amount,0) - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) AS GM_denominator
                    FROM fact_order_live
                    WHERE (is_realised = 1 OR is_shipped = 1)
                    AND (customer_login not ilike '%myntra360%' and customer_login not like '%benchmarking%' and customer_login not ilike '%test%')
                    and (order_created_date >= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint)
                    and ((order_created_date <= 20181225 and order_created_date >= 20181220) or (order_created_date = 20181219 and order_created_time >= 2000))
                    GROUP BY 1)
                    UNION
                    (SELECT customer_login AS uidx,
                           COUNT(DISTINCT order_group_id) AS orders,
                           SUM(quantity) AS quantity,
                           SUM(Loyalty_pts_used) AS loyalty_points_used,
                           SUM((NVL (item_revenue_inc_cashback,0) + NVL (shipping_charges,0) + NVL (gift_charges,0))) AS revenue,
                           SUM(CASE WHEN Loyalty_pts_used >0 THEN (NVL (item_revenue_inc_cashback,0) + NVL (shipping_charges,0) + NVL (gift_charges,0)) END) AS loyalty_revenue,
                           SUM(((nvl (item_revenue_inc_cashback,0) - nvl (tax,0) + nvl (shipping_charges,0) + nvl (gift_charges,0)) -(nvl (cogs,0) + nvl (royalty_commission,0) + nvl (stn_input_vat_reversal,0) + nvl (entry_tax,0)))) AS RGM,
                           SUM((nvl (item_revenue_inc_cashback,0) - nvl (tax,0) + NVL (shipping_charges,0) + NVL (gift_charges,0))) AS GM_denominator
                    FROM fact_core_item AS fci
                      JOIN dim_customer_idea AS dci ON fci.idcustomer = dci.id
                    WHERE (is_realised = 1 OR is_shipped = 1)
                    AND order_created_date <= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '2 day')),'YYYYMMDD')::bigint 
                    and ((order_created_date <= 20181225 and order_created_date >= 20181220) or (order_created_date = 20181219 and order_created_time >= 2000))
                    AND   store_id = 1
                    GROUP BY 1))
             GROUP BY 1) fol ON R.friend_user_id = fol.uidx;

-----Friends Visit Recency
drop table customer_insights.user_flag_visit_recency_bfd_2018;
create table customer_insights.user_flag_visit_recency_bfd_2018 as 

select (case when b.last_visited_date between 20181201 and 20181218 then 'december' 
       when b.last_visited_date between 20181101 and 20181130 then 'november'
			 when b.last_visited_date between 20180901 and 20181031 then 'sept_oct'
			 when b.last_visited_date between 20180701 and 20180831 then 'july_aug'
			 when b.last_visited_date between 20180501 and 20180630 then 'may_june'
			 when b.last_visited_date < 20180501 then 'before_may'
			 when b.last_visited_date is null then 'new_users' else 'null' end) visit_month, a.user_id, a.referrer_user_id, a.flag, a.referrer_reward

from user_referrer_friends_flag_bfd_2018 as a

left join user_visit_recency_bfd_2018 as b

on a.user_id = b.uidx

where a.flag = 'friend'

group by 1, 2, 3, 4, 5;

-----Friends performance
drop table customer_insights.friends_performance_bfd_2018;
create table customer_insights.friends_performance_bfd_2018 as 

select a.visit_month, count(distinct a.user_id) referee_users,
count(distinct case when purchased is not null then a.user_id end) referee_purchased,
sum(revenue) referee_revenue,
sum(orders) referee_orders,
sum(case when referrer_reward > nvl(loyalty_pts_used,0) then loyalty_pts_used else referrer_reward end) referee_loyalty_pts_used,
sum(num_gm) referee_num_gm,
sum(den_gm) referee_den_gm

from

(select a.user_id, a.referrer_reward::float, a.visit_month,
(case when c.order_group_id is not null then a.user_id end) purchased,
SUM(revenue) revenue,
count(distinct c.order_group_id) orders,
sum(nvl(c.loyalty_pts_used,0)) loyalty_pts_used,
sum(num_gm) as num_gm,
sum(den_gm) as den_gm

from user_flag_visit_recency_bfd_2018 as a

left join dim_customer_idea as b

on a.user_id = b.customer_login

left join 

(select idcustomer, order_group_id::bigint, 
(NVL(item_revenue_inc_cashback,0) + NVL(shipping_charges,0) + NVL(gift_charges,0)) AS revenue,
loyalty_pts_used, 
((nvl(item_revenue_inc_cashback,0) - nvl(tax,0) + nvl(shipping_charges,0) + nvl(gift_charges,0)) - (nvl(cogs,0) + nvl(royalty_commission,0) + nvl(stn_input_vat_reversal,0) + nvl(entry_tax,0))) as num_gm,
(nvl(item_revenue_inc_cashback,0) - nvl(tax,0) + NVL(shipping_charges,0) + NVL(gift_charges,0)) as den_gm

from fact_core_item as c

where order_created_date <= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '2 day')),'YYYYMMDD')::bigint 
and ((order_created_date <= 20181225 and order_created_date >= 20181220) or (order_created_date = 20181219 and order_created_time >= 2000))
and (is_realised = 1 OR is_shipped = 1)
and store_id = 1

union

select b.id idcustomer, order_group_id, 
nvl(item_revenue,0) + nvl(cashback_redeemed,0) + nvl(shipping_charge,0) + nvl(gift_charge,0) AS revenue,
loyalty_used loyalty_pts_used, 
(item_revenue + cashback_redeemed - nvl (tax_amount,0) - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) - (CASE WHEN brand = 'Puma' AND seller_id = 28 THEN item_revenue + cashback_redeemed - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0) - nvl (sales_commission,0) - nvl (logistics_charges,0) - nvl (tax_amount,0) ELSE item_purchase_price_inc_tax - vendor_funding + royalty_commission + nvl (entry_tax,0) END) as num_gm,
(item_revenue + cashback_redeemed - nvl (tax_amount,0) - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) as den_gm

from fact_order_live as a

inner join dim_customer_idea as b

on a.customer_login = b.customer_login

where (a.is_realised = 1 OR a.is_shipped = 1)
and (a.customer_login not ilike '%myntra360%' and a.customer_login not like '%benchmarking%' and a.customer_login not ilike '%test%')
and (order_created_date >= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint)
and ((order_created_date <= 20181225 and order_created_date >= 20181220) or (order_created_date = 20181219 and order_created_time >= 2000))) as c

on b.id = c.idcustomer

group by 1, 2, 3, 4) as a

group by 1;

-----Referrers Performance
drop table customer_insights.referrers_performance_bfd_2018;
create table customer_insights.referrers_performance_bfd_2018 as 

select count(distinct a.user_id) users, count(distinct case when purchased is not null then a.user_id end) referrer_purchased,
sum(revenue) referrer_revenue,
sum(orders) referrer_orders,
sum(case when nvl(loyalty_pts_used,0) > referrer_reward then referrer_reward else loyalty_pts_used end) referrer_loyalty_pts_used,
sum(num_gm) referrer_num_gm,
sum(den_gm) referrer_den_gm,
count(distinct case when b.user_id is not null then a.user_id end) users_with_5_plus_successful_referrals,
count(distinct case when e.user_id is not null then a.user_id end) users_reactivated_dormant_users,
count(distinct case when f.user_id is not null then a.user_id end) users_activated_new_users

from

(select a.user_id, a.referrer_reward::float,
(case when c.order_group_id is not null then a.user_id end) purchased,
SUM(c.revenue) revenue,
count(distinct c.order_group_id) orders,
sum(nvl(c.loyalty_pts_used,0)) loyalty_pts_used,
sum(c.num_gm) as num_gm,
sum(c.den_gm) as den_gm

from user_referrer_friends_flag_bfd_2018 as a

left join dim_customer_idea as b

on a.user_id = b.customer_login

left join 

(select idcustomer, order_group_id::bigint, 
(NVL(item_revenue_inc_cashback,0) + NVL(shipping_charges,0) + NVL(gift_charges,0)) AS revenue,
loyalty_pts_used, 
((nvl(item_revenue_inc_cashback,0) - nvl(tax,0) + nvl(shipping_charges,0) + nvl(gift_charges,0)) - (nvl(cogs,0) + nvl(royalty_commission,0) + nvl(stn_input_vat_reversal,0) + nvl(entry_tax,0))) as num_gm,
(nvl(item_revenue_inc_cashback,0) - nvl(tax,0) + NVL(shipping_charges,0) + NVL(gift_charges,0)) as den_gm

from fact_core_item as c

where order_created_date <= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '2 day')),'YYYYMMDD')::bigint 
and ((order_created_date <= 20181225 and order_created_date >= 20181220) or (order_created_date = 20181219 and order_created_time >= 2000))
and (is_realised = 1 OR is_shipped = 1)
and store_id = 1

union

select b.id idcustomer, order_group_id, 
nvl(item_revenue,0) + nvl(cashback_redeemed,0) + nvl(shipping_charge,0) + nvl(gift_charge,0) AS revenue,
loyalty_used loyalty_pts_used, 
(item_revenue + cashback_redeemed - nvl (tax_amount,0) - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) - (CASE WHEN brand = 'Puma' AND seller_id = 28 THEN item_revenue + cashback_redeemed - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0) - nvl (sales_commission,0) - nvl (logistics_charges,0) - nvl (tax_amount,0) ELSE item_purchase_price_inc_tax - vendor_funding + royalty_commission + nvl (entry_tax,0) END) as num_gm,
(item_revenue + cashback_redeemed - nvl (tax_amount,0) - nvl (loyalty_used,0) - nvl (mynts_used,0) + nvl (shipping_charge,0) + nvl (gift_charge,0)) as den_gm

from fact_order_live as a

inner join dim_customer_idea as b

on a.customer_login = b.customer_login

where (a.is_realised = 1 OR a.is_shipped = 1)
and (a.customer_login not ilike '%myntra360%' and a.customer_login not like '%benchmarking%' and a.customer_login not ilike '%test%')
and (order_created_date >= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint)
and ((order_created_date <= 20181225 and order_created_date >= 20181220) or (order_created_date = 20181219 and order_created_time >= 2000))) as c

on b.id = c.idcustomer

where a.flag = 'referrer'
group by 1, 2, 3) as a

left join

(select referrer_user_id user_id, count(1) referrers
from user_rewards_audit
where status = 'SUCCESS'
and TRANSACTION_TIMESTAMP >= '2018-12-19T00:00:00'
and TRANSACTION_TIMESTAMP < '2018-12-26T00:00:00'

group by 1
having referrers > 5) as b

on a.user_id = b.user_id

left join 

(select referrer_user_id user_id, count(1) rw
from user_flag_visit_recency_bfd_2018
where visit_month in ('sept_oct', 'july_aug', 'may_june', 'before_may')
group by 1) as e

on a.user_id = e.user_id

left join 

(select referrer_user_id user_id, count(1) rw
from user_flag_visit_recency_bfd_2018
where visit_month in ('new_users')
group by 1) as f

on a.user_id = f.user_id;

-----Efficiency metrics
drop table customer_insights.shout_earn_efficiency_bfd_2018;
create table customer_insights.shout_earn_efficiency_bfd_2018 as 

select sum(referee_loyalty_pts_used) total_referee_loyalty_pts_used,
	   sum(case when visit_month in ('new_users', 'sept_oct', 'july_aug', 'may_june', 'before_may') then referee_orders end) new_dormant_users_transaction,
	   sum(case when visit_month in ('new_users', 'sept_oct', 'july_aug', 'may_june', 'before_may') then referee_revenue end) new_dormant_users_rev

from friends_performance_bfd_2018;

----Platform Conversion
drop table customer_insights.bfd_2018_platform_conv;
create table customer_insights.bfd_2018_platform_conv as 

select 100.0*b.platform_customers/nullif(a.platform_users,0) platform_conversion

from

(select count(distinct a.uidx) platform_users

from sessionhour_view as a

left join user_referrer_friends_flag_bfd_2018 as b

on a.uidx = b.user_id

where b.user_id is null
and ((load_date::bigint between 20181220 and 20181225) or (load_date::bigint = 20181220 and hour::bigint >= 20))) as a

cross join

(select count(distinct c.customer_login) platform_customers

from

(select b.customer_login

from fact_core_item as c

inner join dim_customer_idea as b

on c.idcustomer = b.id

where order_created_date <= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '2 day')),'YYYYMMDD')::bigint 
and ((order_created_date <= 20181225 and order_created_date >= 20181220) or (order_created_date = 20181219 and order_created_time >= 2000))
and (is_realised = 1 OR is_shipped = 1)
and store_id = 1

union

select customer_login

from fact_order_live as a

where (a.is_realised = 1 OR a.is_shipped = 1)
and (a.customer_login not ilike '%myntra360%' and a.customer_login not like '%benchmarking%' and customer_login not ilike '%test%')
and (order_created_date >= to_char(date(convert_timezone('UTC-5:30', getdate() - interval '1 day')),'YYYYMMDD')::bigint)
and ((order_created_date <= 20181225 and order_created_date >= 20181220) or (order_created_date = 20181219 and order_created_time >= 2000))) as c

left join user_referrer_friends_flag_bfd_2018 as b

on c.customer_login = b.user_id

where b.user_id is null) as b;

-----
END;