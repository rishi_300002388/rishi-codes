import base64
import io
import dash
from dash.dependencies import Input, Output, State
import dash_table_experiments as dt
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import plotly.graph_objs as go
import sqlalchemy as sq
import plotly.figure_factory as ff
from datetime import datetime, timedelta
import operator
import os
import json
import numpy as np
from sklearn import model_selection
from sklearn.linear_model import LinearRegression
import pickle
import math

app = dash.Dash()

app.scripts.config.serve_locally = True
app.config['suppress_callback_exceptions'] = True

loaded_model = pickle.load(open('crm_model.pkl', 'rb'))

dff_ohe = pd.read_csv("crm_rawdata_with_OHE.csv")
cols = list(dff_ohe.columns.values)[2:]

df = pd.read_csv("crm_rawdata.csv")
dff = pd.read_csv("nov_planned.csv")
df['date']=pd.to_datetime(df['date'], format='%d/%m/%y')

#engine = sq.create_engine("postgresql+psycopg2://analysis_user:20180703@asdTgd!@dw-prod.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")
clickstream_engine=sq.create_engine("postgresql+psycopg2://customer_insights_ddl:20180703@ajqoyP@dw-clickstream.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com:5439/myntra_dw")
query = "select * from customer_insights.crm_rawdata_1 where order_created_date <= to_char(sysdate - interval '4 day','YYYYMMDD')::bigint and lp_share is not null"
actuals_df=pd.read_sql_query(query,clickstream_engine)
actuals_df.rename(columns={'order_created_date':'date',
                          'avg_tat':'avg_TAT',
                          'lp_share':'top_brand_lp_share'}, 
                 inplace=True)
actuals_df['date']=pd.to_datetime(actuals_df['date'], format='%Y%m%d')
actuals_df['acc_gm_delta'] = actuals_df['acq_gm_perc'] - actuals_df['gm_perc']

ind_list=['isp_wo_delivery_log','gm_perc','top_brand_lp_share','acc_gm_delta','avg_TAT','cancellation_perc', 'shipping_charge_flag_1_low', 'shipping_charge_flag_1_med']
#cat_ind_list = ['week_number_2','week_number_3','week_number_4','week_number_5','day_monday','day_saturday','day_sunday','day_thursday','day_tuesday','day_wednesday','shipping_charge_flag_1_low','shipping_charge_flag_1_med','final_flag_1','final_flag_with_start_end_fest','final_flag_with_start_end_hrd','final_flag_with_start_end_na','final_flag_with_start_end_new season','final_flag_with_start_end_post sale','final_flag_with_start_end_presale','final_flag_with_start_end_sale','final_flag_with_start_end_sale_end','final_flag_with_start_end_sale_start','pointsproperty_1','superhour_flag_1','rushhour_flag_1']

app.layout = html.Div([

    html.H5("Upload Files"),
    dcc.Upload(
        id='upload-data',
        children=html.Div([
            'Drag and Drop or ',
            html.A('Select Files')
        ]),
        style={
            'width': '100%',
            'height': '60px',
            'lineHeight': '60px',
            'borderWidth': '1px',
            'borderStyle': 'dashed',
            'borderRadius': '5px',
            'textAlign': 'center',
            'margin': '10px'
        },
        multiple=False),

    html.H5("Planned Inputs"),
    html.Div(dt.DataTable(rows=[{}], id='table')),
    html.Br(),

    html.H5("Conversion Predictions"),
    dcc.Graph(id='actuals_vs_preictions'),

    html.Hr(),
    html.H5("RCA Framework"),
    html.Div([
        dcc.DatePickerSingle(
            id='my-date-picker-single',
            min_date_allowed=pd.to_datetime(datetime(2017, 1, 1)),
            max_date_allowed=pd.to_datetime(datetime.now() - timedelta(4)),
            date= pd.to_datetime(datetime.now() - timedelta(4)),
            #initial_visible_month=dt(2018, 7, 5),
            clearable=True,
            placeholder='Date',
            #end_date=dt(2018, 10, 30)
        )
    ]),

    #html.Br(),

    html.Div([
        dcc.Graph(
            id='waterfall-chart'
        )
    ], style={'width': '100%', 'display': 'inline-block', 'padding': '0 20'}),

    html.Br(),
    html.H5("Planned vs Actuals Data"),
    html.Div([
        dcc.Graph(
            id='data-table',
        )
    ], style={'width': '100%', 'display': 'inline-block', 'padding': '0 20'}),
    html.Hr()

])

@app.callback(
     dash.dependencies.Output('data-table', 'figure'),
     [Input('my-date-picker-single', 'date'),
               Input('table', 'rows')])
def generate_table_pr(date, tablerows):
    dff = pd.DataFrame(tablerows)
    dff['date'] = pd.to_datetime(dff['date'], format='%Y%m%d')
    planned = dff[(dff.date == date)]
    planned['Flag'] = 'Planned'
    ind_list_1=['Flag', 'isp_wo_delivery_log','gm_perc','top_brand_lp_share','acc_gm_delta','avg_TAT','cancellation_perc', 'shipping_charge_flag_1']
    planned = planned[ind_list_1]
    planned = planned.round(2)

    actuals = actuals_df[(actuals_df.date == date)]
    actuals['Flag'] = 'Actuals'
    actuals = actuals[ind_list_1]
    actuals = actuals.round(2)

    dataframe=planned.append(actuals)
    #print dataframe
    return ff.create_table(dataframe.round(2))

# file upload function
def parse_contents(contents, filename):
    content_type, content_string = contents.split(',')
    decoded = base64.b64decode(content_string)
    try:
        if 'csv' in filename:
            # Assume that the user uploaded a CSV file
            df = pd.read_csv(
                io.StringIO(decoded.decode('utf-8')))
        elif 'xls' in filename:
            # Assume that the user uploaded an excel file
            df = pd.read_excel(io.BytesIO(decoded))

    except Exception as e:
        print(e)
        return None

    return df

# callback table creation
@app.callback(Output('table', 'rows'),
              [Input('upload-data', 'contents'),
               Input('upload-data', 'filename')])
def update_output(contents, filename):
    if contents is not None:
        df = parse_contents(contents, filename)
        if df is not None:
            return df.to_dict('records')
        else:
            return [{}]
    else:
        return [{}]

#Predictions
@app.callback(Output('actuals_vs_preictions', 'figure'),
              [Input('table', 'rows')])
def update_figure(tablerows):
    dff = pd.DataFrame(tablerows)
    dff['date'] = pd.to_datetime(dff['date'], format='%Y%m%d')
    sd = min(dff['date'])
    ed = max(dff['date'])
    merge = pd.concat([df, dff], ignore_index=True)
    merge['week_number'] = merge['week_number'].astype(object)
    merge['pointsproperty'] = merge['pointsproperty'].astype(object)
    merge['rushhour_flag'] = merge['rushhour_flag'].astype(object)
    merge['final_flag'] = merge['final_flag'].astype(object)
    merge['superhour_flag'] = merge['superhour_flag'].astype(object)
    merge['final_flag_with_start_end'] = merge['final_flag_with_start_end'].astype(object)
    merge['shipping_charge_flag_1'] = merge['shipping_charge_flag_1'].astype(object)
    a = merge.loc[:, merge.columns != 'date']
    a = pd.get_dummies(a,drop_first=True)
    a['date'] = merge['date']
    filtered_dff = a[(a.date >= sd) & (a.date <= ed)]
    #print "dff"
    #print filtered_dff.columns, merge.columns, df.columns
    filtered_dff['pred'] = loaded_model.predict(filtered_dff[cols])

    actuals_1 = actuals_df[(actuals_df.date >= sd) & (actuals_df.date <= ed)]
    actuals = pd.merge(actuals_1.drop(['acq_gm_perc','conversion'], axis = 1), dff[['date', 'final_flag_with_start_end', 'final_flag', 'pointsproperty', 'superhour_flag', 'rushhour_flag']], how='inner', on = 'date')
    merge = pd.concat([df, actuals])
    merge['week_number'] = merge['week_number'].astype(object)
    merge['pointsproperty'] = merge['pointsproperty'].astype(object)
    merge['rushhour_flag'] = merge['rushhour_flag'].astype(object)
    merge['final_flag'] = merge['final_flag'].astype(object)
    merge['superhour_flag'] = merge['superhour_flag'].astype(object)
    merge['final_flag_with_start_end'] = merge['final_flag_with_start_end'].astype(object)
    merge['shipping_charge_flag_1'] = merge['shipping_charge_flag_1'].astype(object)
    a = merge.loc[:, merge.columns != 'date']
    a = pd.get_dummies(a,drop_first=True)
    a['date'] = merge['date']
    actuals = a[(a.date >= sd) & (a.date <= ed)]
    actuals = pd.merge(actuals, actuals_1[['date', 'conversion']], how = 'inner', on = 'date')
    actuals = actuals.sort_values(by = 'date')
    actuals['pred'] = loaded_model.predict(actuals[cols])
    a = actuals['pred'].max()
    b = actuals['conversion'].max()
    c = filtered_dff['pred'].max()
    d = math.ceil(max(a,b,c)*2)/2
    #print "Printing Variables"
    #print a, b, c
    #print math.ceil(max(a,b,c)*2)/2
    
    traces = []
    traces.append(
        go.Scatter(
        x=actuals['date'],
        y=actuals['conversion'],
        mode = 'lines+markers',
        name = 'Conversion'
        )  
    )
    traces.append(
        go.Scatter(
        x=filtered_dff['date'],
        y=filtered_dff['pred'],
        mode = 'lines+markers',
        name = 'Prediction with planned'
        )
    )
    traces.append(
        go.Scatter(
        x=actuals['date'],
        y=actuals['pred'],
        mode = 'lines+markers',
        name = 'Prediction with actuals'
        )
    )

    return {
        'data': traces,
        'layout': go.Layout(
                #title='Conversion',
                xaxis={'title': 'Date'},
                yaxis={'title': 'Conversion', 'range' : [0.75,d]},
                #margin={'l': 40, 'b': 40, 't': 10, 'r': 10},
                #legend={'x': 0, 'y': 1},
                hovermode='closest'
        )
    }

#waterfall for particular date
@app.callback(Output('waterfall-chart', 'figure'),
              [Input('my-date-picker-single', 'date'),
               Input('table', 'rows')])
def update_figure(date, tablerows):
    dff = pd.DataFrame(tablerows)
    dff['date'] = pd.to_datetime(dff['date'], format='%Y%m%d')
    sd = min(dff['date'])
    ed = max(dff['date'])
    merge = pd.concat([df, dff], ignore_index=True)
    merge['week_number'] = merge['week_number'].astype(object)
    merge['pointsproperty'] = merge['pointsproperty'].astype(object)
    merge['rushhour_flag'] = merge['rushhour_flag'].astype(object)
    merge['final_flag'] = merge['final_flag'].astype(object)
    merge['superhour_flag'] = merge['superhour_flag'].astype(object)
    merge['final_flag_with_start_end'] = merge['final_flag_with_start_end'].astype(object)
    merge['shipping_charge_flag_1'] = merge['shipping_charge_flag_1'].astype(object)
    a = merge.loc[:, merge.columns != 'date']
    a = pd.get_dummies(a,drop_first=True)
    a['date'] = merge['date']
    filtered_dff = a[(a.date >= sd) & (a.date <= ed)]
    planned = filtered_dff[(filtered_dff.date == date)]
    planned['pred'] = loaded_model.predict(planned[cols])
    #planned = planned.reset_index()

    actuals = actuals_df[(actuals_df.date >= sd) & (actuals_df.date <= ed)]
    actuals = pd.merge(actuals.loc[:, actuals.columns != 'acq_gm_perc'], dff[['date', 'final_flag_with_start_end', 'final_flag', 'pointsproperty', 'superhour_flag', 'rushhour_flag']], how='inner', on = 'date')
    merge = pd.concat([df, actuals])
    merge['week_number'] = merge['week_number'].astype(object)
    merge['pointsproperty'] = merge['pointsproperty'].astype(object)
    merge['rushhour_flag'] = merge['rushhour_flag'].astype(object)
    merge['final_flag'] = merge['final_flag'].astype(object)
    merge['superhour_flag'] = merge['superhour_flag'].astype(object)
    merge['final_flag_with_start_end'] = merge['final_flag_with_start_end'].astype(object)
    merge['shipping_charge_flag_1'] = merge['shipping_charge_flag_1'].astype(object)
    a = merge.loc[:, merge.columns != 'date']
    a = pd.get_dummies(a,drop_first=True)
    a['date'] = merge['date']
    actuals = a[(a.date >= sd) & (a.date <= ed)]
    actuals = actuals[(actuals.date == date)]
    actuals['pred'] = loaded_model.predict(actuals[cols])
    #actuals = actuals.reset_index()
    
    plot_data = pd.DataFrame()
    plot_data['Prediction with planned'] = planned['pred']
    
    for i in ind_list:
        inp=planned
        inp = pd.merge(inp.loc[:, inp.columns != i], actuals[['date', i]], how='inner', on = 'date')
        plot_data[i]= loaded_model.predict(inp[cols]) - planned['pred']

    plot_data['Prediction with Actuals'] = actuals.iloc[0]['pred']
    delta=list(plot_data.values[0])
    
    plot_values_bot=[]

    for i in range(0,len(plot_data.columns)):
        if i==0 or i==len(plot_data.columns)-1:
            plot_values_bot.append(0)
        else:
            plot_values_bot.append(sum(delta[:i]))

    x_data = list(plot_data.columns)
    y_data = plot_values_bot
    text = list(str(round(i,2)) for i in delta)

    #Base
    trace0 = go.Bar(
        x=x_data,
        y=plot_values_bot,
        marker=dict(
            color='rgba(1,1,1, 0.0)',
        )
    )

    #Actual Values
    trace1 = go.Bar(
        x=x_data,
        y=delta,
        text = text,
        textposition = 'auto',
        marker=dict(
            color='rgba(55, 128, 191, 0.7)',
            line=dict(
                color='rgba(55, 128, 191, 1.0)',
                width=2,
            )
        )
    )
    data = [trace0,trace1]
    layout = go.Layout(
        title='Change attribution',
        xaxis=dict(),
        barmode='stack',
        #height = 500,
        paper_bgcolor='rgba(245, 246, 249, 1)',
        plot_bgcolor='rgba(245, 246, 249, 1)',
        showlegend=False,
        margin=go.layout.Margin(
            l=50,
            r=50,
            b=120,
            t=100,
            pad=4
        ),
    )

#    annotations = []
#
#    for i in range(0, len(plot_data.columns)):
#        annotations.append(dict(x=x_data[i], y=y_data[i], text=text[i],
#                                      font=dict(family='Arial', size=12,
#                                      color='rgba(0, 0, 0, 250)'),
#                                      showarrow=False,))
#        layout['annotations'] = annotations

    return go.Figure(data=data, layout=layout)

app.css.append_css({
    "external_url": "https://codepen.io/chriddyp/pen/bWLwgP.css"
})

if __name__ == '__main__':
    app.run_server(debug=True)