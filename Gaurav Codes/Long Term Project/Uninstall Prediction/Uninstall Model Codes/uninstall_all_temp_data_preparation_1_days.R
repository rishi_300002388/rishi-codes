con <- dbConnect(PostgreSQL(), host="dw-clickstream.cvrnhetyq5tx.ap-southeast-1.redshift.amazonaws.com", port="5439", dbname="myntra_dw", user="customer_insights_ddl", password="20180703@ajqoyP")
query <- "select * from gaurav_all_temp_dataset_1"

browsing <- dbGetQuery(con,query)
#str(browsing)
browsing$install_source = as.factor(browsing$install_source)
browsing$logged_in_flag  = as.factor(browsing$logged_in_flag)
browsing$five_day_uninstall = as.factor(browsing$five_day_uninstall)
browsing$blocked_flag  = as.factor(browsing$blocked_flag)
#browsing$last_activity  = as.factor(browsing$last_activity)
browsing$days_since_active = as.factor(browsing$days_since_active)
#browsing$active_days = as.factor(browsing$active_days)
#browsing$notif_days = as.factor(browsing$notif_days)
#browsing$days_since_active = as.numeric(browsing$days_since_active)
#browsing$active_days = as.numeric(browsing$active_days)
#browsing$notif_days = as.numeric(browsing$notif_days)
str(browsing)
#quantiles = colQuantiles(data.matrix(browsing), rows = NULL, cols = NULL, probs = seq(from = 0.98, to = 1, by = 0.002), na.rm = T)
#write.csv(quantiles, '5_day_activity_quantiles.csv', quote=FALSE)
data_both = browsing
#data_both$five_day_uninstall = ifelse(is.na(data_both$uninstall_date) | 
#                                        (as.Date(as.character(data_both$uninstall_date), format = '%Y%m%d') - as.Date(as.character(data_both$install_date), format = '%Y%m%d')-1) > 5, 0, 1)
#data_both = subset(browsing, uninstalled_earlier <= 3 & sessions <= 25 & sessions > 0 & 
#                     search <= 35 & lp_views <= 250 & pdp_views <= 350 & atc <= 35 & wishlist <= 40 &
#                     screenloads <= 900 & received <= 120 & collapsed <= 65 & dismissed <= 35)


#a = as.data.frame(as.integer(data_both$uninstall_date)-as.integer(data_both$install_date))
#a[is.na(a)] = 11 
#b = ifelse(a >= 6 & a <= 10, 1, 0)
#data_both$five_day_uninstall = b
table(data_both$five_day_uninstall)
data_both$five_day_uninstall = as.factor(data_both$five_day_uninstall)
#remove('a', 'b')
#data_both$five_day_uninstall = as.factor(data_both$five_day_uninstall)

str(data_both)
set.seed(123)
split = sample.split(data_both$five_day_uninstall, SplitRatio = 0.75)
train_all= subset(data_both, split == TRUE)
test_all= subset(data_both, split == FALSE)
#train_all= subset(data_both, install_date < 20180715)
#test_all= subset(data_both, install_date >= 20180715)
str(data_both)
i = c(4, c(6:33), c(35:40))
both = data_both[,i]
#train = train_all[,i]
#test = test_all[,i]
str(both)
#Split into train and test
#set.seed(123)
#split = sample.split(both$five_day_uninstall, SplitRatio = 0.75)
#train= subset(both, split == TRUE)
#test= subset(both, split == FALSE)

#New Variables
both_new_var = both
both_new_var$session_days = both_new_var$sessions/both_new_var$active_days
both_new_var$sessions_age = both_new_var$sessions/6
both_new_var$lp_views_session = both_new_var$lp_views/both_new_var$sessions
both_new_var$lp_views_days = both_new_var$lp_views/both_new_var$active_days
both_new_var$lp_views_age = both_new_var$lp_views/6
both_new_var$pdp_views_session = both_new_var$pdp_views/both_new_var$sessions
both_new_var$pdp_views_days = both_new_var$pdp_views/both_new_var$active_days
both_new_var$pdp_views_age = both_new_var$pdp_views/6
both_new_var$screenloads_session = both_new_var$screenloads/both_new_var$sessions
both_new_var$screenloads_days = both_new_var$screenloads/both_new_var$active_days
both_new_var$screenloads_age = both_new_var$screenloads/6
both_new_var$usage = both_new_var$active_days/6

#set.seed(123)
split = sample.split(both_new_var$five_day_uninstall, SplitRatio = 0.75)
train= subset(both_new_var, split == TRUE)
test= subset(both_new_var, split == FALSE)
#str(both_new_var)
#train= subset(both_new_var, install_date < 20180715)
#test= subset(both_new_var, install_date >= 20180715)
#train = train[,2:52]
#test = test[,2:52]

#Feature Scaling - not needed
str(train)
i = c(4, 6, 7, 8, 9)
j = c(5, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 21, 22, 23, 24, 25, 26)
categorical_per_both = data_both[,i]
continous_both = data_both[,j]
both = cbind(categorical_per_both, continous_both)
max = apply(continous_both , 2 , max)
min = apply(continous_both, 2 , min)
continous_scaled_both = as.data.frame(scale(continous_both, center = min, scale = max - min))
scaled_both = cbind(categorical_per_both, continous_scaled_both)

#Split into train and test
set.seed(123)
split = sample.split(scaled_both$five_day_uninstall, SplitRatio = 0.75)
train_scaled= subset(scaled_both, split == TRUE)
test_scaled= subset(scaled_both, split == FALSE)

source("precision_recall.R")
