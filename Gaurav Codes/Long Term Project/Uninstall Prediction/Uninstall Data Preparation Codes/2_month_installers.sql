insert into clickstream.whitelist values
('customer_insights','gaurav_july_month_installers','Gaurav','2022-01-01'),
('customer_insights','gaurav_july_month_installers_browse','Gaurav','2022-01-01'),
('customer_insights','gaurav_july_month_installers_decay','Gaurav','2022-01-01'),
('customer_insights','gaurav_july_month_installers_dataset','Gaurav','2022-01-01');
-----
drop table gaurav_july_month_installers;
create table customer_insights.gaurav_july_month_installers as

select a.*, (case when c.device_id is null then 'didnot_uninstall' else '1_17_uninstall' end) uninstall_flag

from

(select a.install_date, b.device_id, a.install_source

from

(select device_id, install_date, install_source, row_number() over(partition by device_id order by flag) rank

from

(select device_id, TO_CHAR(created_datetime,'YYYYMMDD')::BIGINT AS install_date,
	(case when campaign_source is null then 'Organic'
		  when campaign_source in ('Xiaomi') then 'PB'
		  when campaign_source not in ('Referral', 'Mobile Site') then 'Paid' else campaign_source end) install_source,
	(case when campaign_source in ('Xiaomi') then 1 else 2 end) flag

from app_install_tune

where TO_CHAR(created_datetime,'YYYYMMDD')::BIGINT >= 20180701
and TO_CHAR(created_datetime,'YYYYMMDD')::BIGINT <= 20180731
AND   attribution = 'Install'
AND   keyspace IN ('GAID')

union 

select device_id, TO_CHAR(created_datetime,'YYYYMMDD')::BIGINT AS install_date,
	(case when campaign_source is null then 'Organic'
		  when campaign_source in ('Xiaomi') then 'PB'
		  when campaign_source not in ('Referral', 'Mobile Site') then 'Paid' else campaign_source end) install_source,
	(case when campaign_source in ('Xiaomi') then 1 else 2 end) flag

from app_install_appsflyer

where TO_CHAR(created_datetime,'YYYYMMDD')::BIGINT >= 20180701
and TO_CHAR(created_datetime,'YYYYMMDD')::BIGINT <= 20180731
AND   keyspace IN ('GAID')) as a) as a

inner join 

(SELECT device_id, gaid advertising_id
FROM daily_aggregates
where load_date between 20180701 and 20180831
GROUP BY 1, 2) as b

on a.device_id = b.advertising_id
and rank = 1

group by 1, 2, 3) as a

left join

(select device_id
from user_install_uninstall_events
where event = 'Uninstalled'
and load_date between 20180701 and 20180831
group by 1) as b

on a.device_id = b.device_id

left join

(select device_id
from user_install_uninstall_events
where event = 'Uninstalled'
and load_date between 20180901 and 20180917
group by 1) as c

on a.device_id = c.device_id

where b.device_id is null

group by 1, 2, 3, 4;

--------
drop table gaurav_july_month_installers_browse;
create table customer_insights.gaurav_july_month_installers_browse as

select a.*, b.load_date, (date(b.load_date) - date(a.install_date)) day_flag, 
max(uidx) uidx,
max(logged_in_flag) logged_in_flag,
sum(sessions) sessions,
sum(searches_fired) search,
sum(lp_views) lp_views,
sum(all_pdp_views) pdp_views,
sum(nvl(added_to_cart, 0) + nvl(add_to_collection, 0)) atc,
sum(cart_view) cart_view,
sum(add_to_list) wishlist,
sum(screenloads) screenloads

from gaurav_july_month_installers as a

inner join 

(select device_id, load_date, (case when uidx is null then 0 else 1 end) logged_in_flag, sessions, searches_fired, lp_views, all_pdp_views, added_to_cart, add_to_collection, 
	cart_view, add_to_list, screenloads, uidx

from daily_aggregates as b
where b.load_date between 20180701 and 20180831) as b

on a.device_id = b.device_id

where (date(b.load_date) - date(a.install_date)) >= 0

group by 1, 2, 3, 4, 5;

-----
drop table gaurav_july_month_installers_decay;
create table customer_insights.gaurav_july_month_installers_decay as

select day_flag, a.sessions_user/b.sessions_user decay_value

from

(select day_flag, sum(sessions_user) over(order by day_flag rows unbounded preceding) sessions_user

from 

(select day_flag, 1.0*sessions/users sessions_user 

from

(select date(b.load_date) - date(a.install_date) day_flag, sum(sessions) sessions

from gaurav_july_month_installers as a

inner join 

(select device_id, load_date, sum(sessions) sessions from daily_aggregates where load_date between 20180701 and 20180831 group by 1, 2) as b

on a.device_id = b.device_id

where date(b.load_date) - date(a.install_date) between 0 and 21

group by 1) as a

cross join

(select count(1) users from gaurav_july_month_installers) as b) as a) as a 

cross join 

(select sessions_user

from

(select day_flag, sum(sessions_user) over(order by day_flag rows unbounded preceding) sessions_user

from 

(select day_flag, 1.0*sessions/users sessions_user 

from

(select date(b.load_date) - date(a.install_date) day_flag, sum(sessions) sessions

from gaurav_july_month_installers as a

inner join 

(select device_id, load_date, sum(sessions) sessions from daily_aggregates where load_date between 20180701 and 20180831 group by 1, 2) as b

on a.device_id = b.device_id

where date(b.load_date) - date(a.install_date) between 0 and 21

group by 1) as a

cross join

(select count(1) users from gaurav_july_month_installers) as b) as a)

where day_flag = 21) as b;

------Dataset
drop table gaurav_july_month_installers_dataset;
create table customer_insights.gaurav_july_month_installers_dataset as

select a.*, (case when c.gender is null then 'unk' else c.gender end) gender, (case when b.city_tier is null then 4 else b.city_tier::bigint end) city_tier,
(case when d.uidx is null then 'not_purchased' else 'purchased' end) purchase_flag

from

(select device_id, install_date, install_source, uninstall_flag, 
(date(20180831) - date(install_date)) platform_age,	
(date(20180831) - date(max(load_date))) days_since_active,
max(uidx) uidx,
max(logged_in_flag) logged_in_flag,
sum(case when day_flag between 0 and 6 then 1 else 0 end) w1_active_days,
sum(case when day_flag between 0 and 6 then sessions else 0 end) w1_sessions,
sum(case when day_flag between 0 and 6 then search else 0 end) w1_search,
sum(case when day_flag between 0 and 6 then lp_views else 0 end) w1_lp_views,
sum(case when day_flag between 0 and 6 then pdp_views else 0 end) w1_pdp_views,
sum(case when day_flag between 0 and 6 then atc else 0 end) w1_atc,
sum(case when day_flag between 0 and 6 then cart_view else 0 end) w1_cart_view,
sum(case when day_flag between 0 and 6 then wishlist else 0 end) w1_wishlist,
sum(case when day_flag between 0 and 6 then screenloads else 0 end) w1_screenloads,
sum(case when day_flag between 7 and 20 then 1 else 0 end) w23_active_days,
sum(case when day_flag between 7 and 20 then sessions else 0 end) w23_sessions,
sum(case when day_flag between 7 and 20 then search else 0 end) w23_search,
sum(case when day_flag between 7 and 20 then lp_views else 0 end) w23_lp_views,
sum(case when day_flag between 7 and 20 then pdp_views else 0 end) w23_pdp_views,
sum(case when day_flag between 7 and 20 then atc else 0 end) w23_atc,
sum(case when day_flag between 7 and 20 then cart_view else 0 end) w23_cart_view,
sum(case when day_flag between 7 and 20 then wishlist else 0 end) w23_wishlist,
sum(case when day_flag between 7 and 20 then screenloads else 0 end) w23_screenloads,
sum(case when day_flag >= 21 and date(20180831) - date(load_date) not in (0, 1, 2, 3, 4, 5, 6, 7) then 1 else 0 end) w34_active_days,
sum(case when day_flag >= 21 and date(20180831) - date(load_date) not in (0, 1, 2, 3, 4, 5, 6, 7) then sessions else 0 end) w34_sessions,
sum(case when day_flag >= 21 and date(20180831) - date(load_date) not in (0, 1, 2, 3, 4, 5, 6, 7) then search else 0 end) w34_search,
sum(case when day_flag >= 21 and date(20180831) - date(load_date) not in (0, 1, 2, 3, 4, 5, 6, 7) then lp_views else 0 end) w34_lp_views,
sum(case when day_flag >= 21 and date(20180831) - date(load_date) not in (0, 1, 2, 3, 4, 5, 6, 7) then pdp_views else 0 end) w34_pdp_views,
sum(case when day_flag >= 21 and date(20180831) - date(load_date) not in (0, 1, 2, 3, 4, 5, 6, 7) then atc else 0 end) w34_atc,
sum(case when day_flag >= 21 and date(20180831) - date(load_date) not in (0, 1, 2, 3, 4, 5, 6, 7) then cart_view else 0 end) w34_cart_view,
sum(case when day_flag >= 21 and date(20180831) - date(load_date) not in (0, 1, 2, 3, 4, 5, 6, 7) then wishlist else 0 end) w34_wishlist,
sum(case when day_flag >= 21 and date(20180831) - date(load_date) not in (0, 1, 2, 3, 4, 5, 6, 7) then screenloads else 0 end) w34_screenloads,
sum(case when date(20180831) - date(load_date) in (0, 1, 2, 3, 4, 5, 6, 7) then 1 else 0 end) recent_active_days,
sum(case when date(20180831) - date(load_date) in (0, 1, 2, 3, 4, 5, 6, 7) then sessions else 0 end) recent_sessions,
sum(case when date(20180831) - date(load_date) in (0, 1, 2, 3, 4, 5, 6, 7) then search else 0 end) recent_search,
sum(case when date(20180831) - date(load_date) in (0, 1, 2, 3, 4, 5, 6, 7) then lp_views else 0 end) recent_lp_views,
sum(case when date(20180831) - date(load_date) in (0, 1, 2, 3, 4, 5, 6, 7) then pdp_views else 0 end) recent_pdp_views,
sum(case when date(20180831) - date(load_date) in (0, 1, 2, 3, 4, 5, 6, 7) then atc else 0 end) recent_atc,
sum(case when date(20180831) - date(load_date) in (0, 1, 2, 3, 4, 5, 6, 7) then cart_view else 0 end) recent_cart_view,
sum(case when date(20180831) - date(load_date) in (0, 1, 2, 3, 4, 5, 6, 7) then wishlist else 0 end) recent_wishlist,
sum(case when date(20180831) - date(load_date) in (0, 1, 2, 3, 4, 5, 6, 7) then screenloads else 0 end) recent_screenloads

from gaurav_july_month_installers_browse as a

group by 1, 2, 3, 4, 5) as a

left join

(select device_id, uidx, gender, city, row_number() over(partition by device_id order by uidx) rank from fact_user_lifetime) as c

on a.device_id = c.device_id
and c.rank = 1

left join city_tier_mapping_browsing as b

on lower(c.city) = lower(b.city)

left join 

(select customer_login as uidx, min(order_created_date) order_created_date

from customer_purchase_sequence_detailed as a 

inner join dim_customer_idea as b

on a.idcustomer = b.id

where order_created_date between 20180701 and 20180831

group by 1)  as d

on c.uidx = d.uidx
and date(d.order_created_date) between date(a.install_date) and date(20180831)

group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47;

-----July month final dataset
drop table gaurav_july_month_installers_dataset_1;
create table customer_insights.gaurav_july_month_installers_dataset_1 as

select a.*, 
(case when f.device_name is null then 0.4776 else uninstall_rate end) uninstall_rate,
(case when f.device_name is null then 0.2142 else uninstall_rate_7 end) uninstall_rate_7,
(case when f.device_name is null then 0.1646 else uninstall_rate_14 end) uninstall_rate_14,
(case when f.device_name is null then 0.1334 else uninstall_rate_21 end) uninstall_rate_21,
(case when f.device_name is null then 0.1050 else uninstall_rate_30 end) uninstall_rate_30,
(case when f.device_name is null then 0.0469 else uninstall_rate_60 end) uninstall_rate_60,
1.0*notif_sessions/sessions notif_sessions_per

from

(select a.device_id, a.install_date, a.uninstall_flag, a.gender, a.city_tier, a.install_source,
a.platform_age, a.days_since_active, a.logged_in_flag,
a.w1_active_days, a.w1_sessions, a.w1_search, a.w1_lp_views, a.w1_pdp_views, a.w1_screenloads, 
a.w23_active_days, a.w23_sessions, a.w23_lp_views, a.w23_pdp_views, a.w23_screenloads, 
a.w34_active_days, a.w34_sessions, a.w34_lp_views, a.w34_pdp_views, a.w34_screenloads,
(date(20180901) - (date(install_date) + 21)) w34_notif_days, 
(case when a.recent_active_days = 0 then 0 else 1 end) recent_active_flag,
(case when w1_active_days > 0 and w23_active_days > 0 and w34_active_days > 0 then 1 else 0 end) all_weeks_active_flag,
(case when w1_active_days > 0 and w23_active_days > 0 and w34_active_days = 0 then 1 else 0 end) w123_active_flag,
(case when w1_active_days > 0 and w23_active_days = 0 and w34_active_days > 0 then 1 else 0 end) w134_active_flag,
(case when w1_active_days > 0 and w23_active_days = 0 and w34_active_days = 0 then 1 else 0 end) w1_active_flag,
(1.0*w1_active_days/7 - 1.0*w23_active_days/14)/(1.0*w1_active_days/7) w1_2_active_days_delta,
(1.0*w1_sessions/7 - 1.0*w23_sessions/14)/(1.0*w1_sessions/7) w1_2_sessions_delta,
(1.0*w1_active_days/7 - 1.0*w34_active_days/(platform_age - 21))/(1.0*w1_active_days/7) w1_3_active_days_delta,
(1.0*w1_sessions/7 - 1.0*w23_sessions/(platform_age - 21))/(1.0*w1_sessions/7) w1_3_sessions_delta,
sum(case when date(load_date) - date(install_date) between 0 and 6 then notif_open else 0 end) w1_notif_open,
sum(case when date(load_date) - date(install_date) between 0 and 6 then notif_receive else 0 end) w1_notif_receive,
sum(case when date(load_date) - date(install_date) between 0 and 6 then notif_dismiss else 0 end) w1_notif_dismiss,
sum(case when date(load_date) - date(install_date) between 0 and 6 then notif_collapse else 0 end) w1_notif_collapse,
sum(case when date(load_date) - date(install_date) between 7 and 20 then notif_open else 0 end) w23_notif_open,
sum(case when date(load_date) - date(install_date) between 7 and 20 then notif_receive else 0 end) w23_notif_receive,
sum(case when date(load_date) - date(install_date) between 7 and 20 then notif_dismiss else 0 end) w23_notif_dismiss,
sum(case when date(load_date) - date(install_date) between 7 and 20 then notif_collapse else 0 end) w23_notif_collapse,
sum(case when date(load_date) - date(install_date) >= 21 then notif_open else 0 end) w34_notif_open,
sum(case when date(load_date) - date(install_date) >= 21 then notif_receive else 0 end) w34_notif_receive,
sum(case when date(load_date) - date(install_date) >= 21 then notif_dismiss else 0 end) w34_notif_dismiss,
sum(case when date(load_date) - date(install_date) >= 21 then notif_collapse else 0 end) w34_notif_collapse

from gaurav_july_month_installers_dataset as a

left join 

(select device_id, load_date, sum(case when received = 0 and (collapsed > 0 or dismissed >0) then 1 else received end) notif_receive, sum(collapsed) notif_collapse, 
	sum(dismissed) notif_dismiss, sum(opened) notif_open
from fact_user_notification
where load_date::BIGINT between 20180701 and 20180831
group by 1, 2) as e

on a.device_id = e.device_id

where w1_sessions != 0

group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35) as a

left join gaurav_device_name_mapping_1 as b

on a.device_id = b.device_id

left join 

(select f.device_name, g.uninstall_rate, g.uninstall_rate_7, g.uninstall_rate_14, g.uninstall_rate_21, g.uninstall_rate_30, g.uninstall_rate_60

from customer_insights.gaurav_uninstall_all_device_uninstall_2 as f

inner join customer_insights.gaurav_uninstall_all_device_bucket_uninstall_new_2 as g

on f.device_bucket_1 = g.device_bucket

where f.device_name is not null) as f

on lower(b.device_name) = lower(f.device_name)

left join 

(select device_id, count(distinct case when lower(utm_medium) like '%notif%' then session_id end) notif_sessions,
count(distinct case when utm_campaign is not null and utm_medium is not null and utm_source is not null and lower(utm_medium) not like '%notif%' then session_id end) deeplink_sessions,
count(distinct session_id) sessions

from sessiondaily_view

where load_date::bigint between 20180701 and 20180831
and session_id is not null

group by 1) as c

on a.device_id = c.device_id;

----logged in users dataset
drop table gaurav_july_month_installers_logged_in_dataset;
create table customer_insights.gaurav_july_month_installers_logged_in_dataset as

select a.*, 
(case when f.device_name is null then 0.4776 else uninstall_rate end) uninstall_rate,
(case when f.device_name is null then 0.2142 else uninstall_rate_7 end) uninstall_rate_7,
(case when f.device_name is null then 0.1646 else uninstall_rate_14 end) uninstall_rate_14,
(case when f.device_name is null then 0.1334 else uninstall_rate_21 end) uninstall_rate_21,
(case when f.device_name is null then 0.1050 else uninstall_rate_30 end) uninstall_rate_30,
(case when f.device_name is null then 0.0469 else uninstall_rate_60 end) uninstall_rate_60,
1.0*notif_sessions/sessions notif_sessions_per

from

(select a.device_id, a.install_date, a.uninstall_flag, a.gender, a.city_tier, a.install_source, a.purchase_flag, b.channel,
(case when d.eng is null then 'Not Browsed' else d.eng end) ms_eng,
(case when d.di is null then 'No Product viewed and purchased' else d.di end) ms_perf,
a.platform_age, a.days_since_active, 
a.w1_active_days, a.w1_sessions, a.w1_search, a.w1_lp_views, a.w1_pdp_views, a.w1_screenloads, 
a.w23_active_days, a.w23_sessions, a.w23_lp_views, a.w23_pdp_views, a.w23_screenloads, 
a.w34_active_days, a.w34_sessions, a.w34_lp_views, a.w34_pdp_views, a.w34_screenloads,
(date(20180901) - (date(install_date) + 21)) w34_notif_days, 
(case when f.purchase_sequence is null then 0 else f.purchase_sequence end) purchase_sequence,
(case when f.order_revenue is null then 0 else f.order_revenue end) order_revenue,
(case when f.order_qty is null then 0 else f.order_qty end) order_qty,
(case when f.orders is null then 0 else f.orders end) orders,
(case when 1.0*online_orders/orders >= 0.5 then 'online' else 'cod' end) payment_method,
(case when f.purchase_sequence is null then 0 else 1.0*order_revenue/orders end) asp,
(case when f.purchase_sequence is null then 0 else 1.0*order_revenue/order_qty end) isp,
(case when a.recent_active_days = 0 then 0 else 1 end) recent_active_flag,
(case when w1_active_days > 0 and w23_active_days > 0 and w34_active_days > 0 then 1 else 0 end) all_weeks_active_flag,
(case when w1_active_days > 0 and w23_active_days > 0 and w34_active_days = 0 then 1 else 0 end) w123_active_flag,
(case when w1_active_days > 0 and w23_active_days = 0 and w34_active_days > 0 then 1 else 0 end) w134_active_flag,
(case when w1_active_days > 0 and w23_active_days = 0 and w34_active_days = 0 then 1 else 0 end) w1_active_flag,
(1.0*w1_active_days/7 - 1.0*w23_active_days/14)/(1.0*w1_active_days/7) w1_2_active_days_delta,
(1.0*w1_sessions/7 - 1.0*w23_sessions/14)/(1.0*w1_sessions/7) w1_2_sessions_delta,
(1.0*w1_active_days/7 - 1.0*w34_active_days/(platform_age - 21))/(1.0*w1_active_days/7) w1_3_active_days_delta,
(1.0*w1_sessions/7 - 1.0*w23_sessions/(platform_age - 21))/(1.0*w1_sessions/7) w1_3_sessions_delta,
sum(case when date(load_date) - date(install_date) between 0 and 6 then notif_open else 0 end) w1_notif_open,
sum(case when date(load_date) - date(install_date) between 0 and 6 then notif_receive else 0 end) w1_notif_receive,
sum(case when date(load_date) - date(install_date) between 0 and 6 then notif_dismiss else 0 end) w1_notif_dismiss,
sum(case when date(load_date) - date(install_date) between 0 and 6 then notif_collapse else 0 end) w1_notif_collapse,
sum(case when date(load_date) - date(install_date) between 7 and 20 then notif_open else 0 end) w23_notif_open,
sum(case when date(load_date) - date(install_date) between 7 and 20 then notif_receive else 0 end) w23_notif_receive,
sum(case when date(load_date) - date(install_date) between 7 and 20 then notif_dismiss else 0 end) w23_notif_dismiss,
sum(case when date(load_date) - date(install_date) between 7 and 20 then notif_collapse else 0 end) w23_notif_collapse,
sum(case when date(load_date) - date(install_date) >= 21 then notif_open else 0 end) w34_notif_open,
sum(case when date(load_date) - date(install_date) >= 21 then notif_receive else 0 end) w34_notif_receive,
sum(case when date(load_date) - date(install_date) >= 21 then notif_dismiss else 0 end) w34_notif_dismiss,
sum(case when date(load_date) - date(install_date) >= 21 then notif_collapse else 0 end) w34_notif_collapse

from gaurav_july_month_installers_dataset as a

left join dim_customer_idea as b

on a.uidx = b.customer_login

left join

(select id, eng, di from microsegmentation_all_weekly where enddate = 20180825 group by 1, 2, 3) as d

on b.id = d.id

left join 

(select idcustomer as id, max(purchase_sequence) purchase_sequence, sum(order_revenue) order_revenue, sum(order_qty) order_qty,
	count(distinct order_group_id) orders, count(distinct case when payment_method = 'on' then order_group_id end) online_orders

from customer_purchase_sequence_detailed as a 

where order_created_date >= 20180701
and order_created_date <= 20180831

group by 1)  as f

on b.id = f.id

left join 

(select device_id, load_date, sum(case when received = 0 and (collapsed > 0 or dismissed >0) then 1 else received end) notif_receive, sum(collapsed) notif_collapse, 
	sum(dismissed) notif_dismiss, sum(opened) notif_open
from fact_user_notification
where load_date::BIGINT between 20180701 and 20180831
group by 1, 2) as e

on a.device_id = e.device_id

where w1_sessions != 0
and a.logged_in_flag = 1

group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39) as a

left join gaurav_device_name_mapping_1 as b

on a.device_id = b.device_id

left join 

(select f.device_name, g.uninstall_rate, g.uninstall_rate_7, g.uninstall_rate_14, g.uninstall_rate_21, g.uninstall_rate_30, g.uninstall_rate_60

from customer_insights.gaurav_uninstall_all_device_uninstall_2 as f

inner join customer_insights.gaurav_uninstall_all_device_bucket_uninstall_new_2 as g

on f.device_bucket_1 = g.device_bucket

where f.device_name is not null) as f

on lower(b.device_name) = lower(f.device_name)

left join 

(select device_id, count(distinct case when lower(utm_medium) like '%notif%' then session_id end) notif_sessions,
count(distinct case when utm_campaign is not null and utm_medium is not null and utm_source is not null and lower(utm_medium) not like '%notif%' then session_id end) deeplink_sessions,
count(distinct session_id) sessions

from sessiondaily_view

where load_date::bigint between 20180701 and 20180831
and session_id is not null

group by 1) as c

on a.device_id = c.device_id;

------non logged in users dataset
drop table gaurav_july_month_installers_non_logged_in_dataset;
create table customer_insights.gaurav_july_month_installers_non_logged_in_dataset as

select a.*, 
(case when f.device_name is null then 0.4776 else uninstall_rate end) uninstall_rate,
(case when f.device_name is null then 0.2142 else uninstall_rate_7 end) uninstall_rate_7,
(case when f.device_name is null then 0.1646 else uninstall_rate_14 end) uninstall_rate_14,
(case when f.device_name is null then 0.1334 else uninstall_rate_21 end) uninstall_rate_21,
(case when f.device_name is null then 0.1050 else uninstall_rate_30 end) uninstall_rate_30,
(case when f.device_name is null then 0.0469 else uninstall_rate_60 end) uninstall_rate_60,
1.0*notif_sessions/sessions notif_sessions_per

from

(select a.device_id, a.install_date, a.uninstall_flag, a.gender, a.city_tier, a.install_source,
a.platform_age, a.days_since_active, a.logged_in_flag,
a.w1_active_days, a.w1_sessions, a.w1_search, a.w1_lp_views, a.w1_pdp_views, a.w1_screenloads, 
a.w23_active_days, a.w23_sessions, a.w23_lp_views, a.w23_pdp_views, a.w23_screenloads, 
a.w34_active_days, a.w34_sessions, a.w34_lp_views, a.w34_pdp_views, a.w34_screenloads,
(date(20180901) - (date(install_date) + 21)) w34_notif_days, 
(case when a.recent_active_days = 0 then 0 else 1 end) recent_active_flag,
(case when w1_active_days > 0 and w23_active_days > 0 and w34_active_days > 0 then 1 else 0 end) all_weeks_active_flag,
(case when w1_active_days > 0 and w23_active_days > 0 and w34_active_days = 0 then 1 else 0 end) w123_active_flag,
(case when w1_active_days > 0 and w23_active_days = 0 and w34_active_days > 0 then 1 else 0 end) w134_active_flag,
(case when w1_active_days > 0 and w23_active_days = 0 and w34_active_days = 0 then 1 else 0 end) w1_active_flag,
(1.0*w1_active_days/7 - 1.0*w23_active_days/14)/(1.0*w1_active_days/7) w1_2_active_days_delta,
(1.0*w1_sessions/7 - 1.0*w23_sessions/14)/(1.0*w1_sessions/7) w1_2_sessions_delta,
(1.0*w1_active_days/7 - 1.0*w34_active_days/(platform_age - 21))/(1.0*w1_active_days/7) w1_3_active_days_delta,
(1.0*w1_sessions/7 - 1.0*w23_sessions/(platform_age - 21))/(1.0*w1_sessions/7) w1_3_sessions_delta,
sum(case when date(load_date) - date(install_date) between 0 and 6 then notif_open else 0 end) w1_notif_open,
sum(case when date(load_date) - date(install_date) between 0 and 6 then notif_receive else 0 end) w1_notif_receive,
sum(case when date(load_date) - date(install_date) between 0 and 6 then notif_dismiss else 0 end) w1_notif_dismiss,
sum(case when date(load_date) - date(install_date) between 0 and 6 then notif_collapse else 0 end) w1_notif_collapse,
sum(case when date(load_date) - date(install_date) between 7 and 20 then notif_open else 0 end) w23_notif_open,
sum(case when date(load_date) - date(install_date) between 7 and 20 then notif_receive else 0 end) w23_notif_receive,
sum(case when date(load_date) - date(install_date) between 7 and 20 then notif_dismiss else 0 end) w23_notif_dismiss,
sum(case when date(load_date) - date(install_date) between 7 and 20 then notif_collapse else 0 end) w23_notif_collapse,
sum(case when date(load_date) - date(install_date) >= 21 then notif_open else 0 end) w34_notif_open,
sum(case when date(load_date) - date(install_date) >= 21 then notif_receive else 0 end) w34_notif_receive,
sum(case when date(load_date) - date(install_date) >= 21 then notif_dismiss else 0 end) w34_notif_dismiss,
sum(case when date(load_date) - date(install_date) >= 21 then notif_collapse else 0 end) w34_notif_collapse

from gaurav_july_month_installers_dataset as a

left join 

(select device_id, load_date, sum(case when received = 0 and (collapsed > 0 or dismissed >0) then 1 else received end) notif_receive, sum(collapsed) notif_collapse, 
	sum(dismissed) notif_dismiss, sum(opened) notif_open
from fact_user_notification
where load_date::BIGINT between 20180701 and 20180831
group by 1, 2) as e

on a.device_id = e.device_id

where w1_sessions != 0
and a.logged_in_flag = 0

group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36) as a

left join gaurav_device_name_mapping_1 as b

on a.device_id = b.device_id

left join 

(select f.device_name, g.uninstall_rate, g.uninstall_rate_7, g.uninstall_rate_14, g.uninstall_rate_21, g.uninstall_rate_30, g.uninstall_rate_60

from customer_insights.gaurav_uninstall_all_device_uninstall_2 as f

inner join customer_insights.gaurav_uninstall_all_device_bucket_uninstall_new_2 as g

on f.device_bucket_1 = g.device_bucket

where f.device_name is not null) as f

on lower(b.device_name) = lower(f.device_name)

left join 

(select device_id, count(distinct case when lower(utm_medium) like '%notif%' then session_id end) notif_sessions,
count(distinct case when utm_campaign is not null and utm_medium is not null and utm_source is not null and lower(utm_medium) not like '%notif%' then session_id end) deeplink_sessions,
count(distinct session_id) sessions

from sessiondaily_view

where load_date::bigint between 20180701 and 20180831
and session_id is not null

group by 1) as c

on a.device_id = c.device_id;
-------
drop table gaurav_july_month_installers_non_logged_in_dataset_1;
create table customer_insights.gaurav_july_month_installers_non_logged_in_dataset_1 as

select a.device_id,install_date,uninstall_flag,city_tier,install_source,purchase_flag,platform_age,days_since_active,w1_active_days,w1_sessions,w1_search,w1_lp_views,
w1_pdp_views,w1_screenloads,w23_active_days,w23_sessions,w23_lp_views,w23_pdp_views,w23_screenloads,w34_active_days,w34_sessions,w34_lp_views,w34_pdp_views,
w34_screenloads,w34_notif_days,recent_active_flag,all_weeks_active_flag,w123_active_flag,w134_active_flag,w1_active_flag,w1_2_active_days_delta,w1_2_sessions_delta,
w1_3_active_days_delta,w1_3_sessions_delta,w1_notif_open,w1_notif_receive,w1_notif_dismiss,w1_notif_collapse,w23_notif_open,w23_notif_receive,w23_notif_dismiss,
w23_notif_collapse,w34_notif_open,w34_notif_receive,w34_notif_dismiss,w34_notif_collapse,uninstall_rate,uninstall_rate_7,uninstall_rate_14,uninstall_rate_21,
uninstall_rate_30,uninstall_rate_60,notif_sessions_per, (case when b.gender is null then 'unk' else b.gender end) gender

from gaurav_july_month_installers_non_logged_in_dataset as a

left join inferred_gender as b

on a.device_id = b.device_id;


