insert into clickstream.whitelist values
('customer_insights','gaurav_all_temp_4','Gaurav','2022-01-01'),
('customer_insights','gaurav_all_temp_qot_4','Gaurav','2022-01-01'),
('customer_insights','gaurav_all_temp_decay_4','Gaurav','2022-01-01'),
('customer_insights','gaurav_all_temp_dataset_4','Gaurav','2022-01-01');
-----engagement activity
drop table if exists customer_insights.gaurav_all_temp_4;
create table customer_insights.gaurav_all_temp_4 as

select a.device_id, install_date, uninstall_date, install_source, uninstalled_earlier, flag, device_name, (b.load_date - a.install_date) day_flag,
max(logged_in_flag) logged_in_flag,
sum(sessions) sessions,
sum(searches_fired) search,
sum(lp_views) lp_views,
sum(all_pdp_views) pdp_views,
sum(nvl(added_to_cart, 0) + nvl(add_to_collection, 0)) atc,
sum(cart_view) cart_view,
sum(add_to_list) wishlist,
sum(screenloads) screenloads

from

(select a.device_id, install_date, uninstall_date, (case when uninstall_date::BIGINT is null then 'didnot_uninstall' else 'uninstall' end) flag, install_source, 
	uninstalled_earlier, b.device_name

from gaurav_all_devices_install_uninstall_date_source as a

inner join gaurav_device_name_mapping_1 as b

on a.device_id = b.device_id

where install_date::BIGINT between 20180701 and 20180720
and ((uninstall_date::BIGINT - install_date::BIGINT) >= 5 or uninstall_date::BIGINT is null)) as a

inner join 

(select device_id, load_date, (case when uidx is null then 0 else 1 end) logged_in_flag, sessions, searches_fired, lp_views, all_pdp_views, added_to_cart, add_to_collection, 
	cart_view, add_to_list, screenloads

from daily_aggregates as b
where b.load_date >= 20180629) as b

on a.device_id = b.device_id

where b.load_date - a.install_date between 0 and 4

group by 1, 2, 3, 4, 5, 6, 7, 8;

-----QOT
drop table if exists customer_insights.gaurav_all_temp_qot_4;
create table customer_insights.gaurav_all_temp_qot_4 as

select c.device_id, c.install_date, (a.load_date - c.install_date) day_flag,
count(distinct case when session_flag = 'Disengaged' then a.session_id end) as disengaged_sessions,
count(distinct case when session_flag = 'Casual Browse' then a.session_id end) as casual_browse_sessions,
count(distinct case when session_flag = 'Targeted/Intense Browse' then a.session_id end) as targetedintense_browse_sessions,
count(distinct case when session_flag = 'Cart View' then a.session_id end) as cart_view_sessions,
count(distinct case when session_flag = 'Bounce' then a.session_id end) as bounce_sessions,
count(distinct case when session_flag = 'Order Review' then a.session_id end) as order_review_sessions,
count(distinct case when session_flag = 'Shortlist' then a.session_id end) as shortlist_sessions,
count(distinct a.session_id) sessions

from 

(select device_id, install_date, uninstall_date
from customer_insights.gaurav_all_devices_install_uninstall_date_source

where install_date::BIGINT between 20180701 and 20180720
and ((uninstall_date::BIGINT - install_date::BIGINT) >= 5 or uninstall_date::BIGINT is null)) as c

inner join sessiondaily_view as a

on c.device_id = a.device_id
and a.load_date >= 20180629

inner join 

(select session_id, session_flag
from qot_daily_session_classification_base as b
where b.load_date >= 20180629 and b.load_date <= 20180701

union

select session_id, session_flag
from qot2_daily_session_classification_base as b
where b.load_date >= 20180702) as b

on a.session_id = b.session_id

where a.load_date - c.install_date::BIGINT between 0 and 4

group by 1, 2, 3;

------Decay value
drop table if exists customer_insights.gaurav_all_temp_decay_4;
create table customer_insights.gaurav_all_temp_decay_4 as

select day_flag, a.logged_in_flag, 1.0*num_decay/den_decay decay

from

(select day_flag, logged_in_flag, count(*) den_decay from gaurav_all_temp_4 group by 1, 2) as a

inner join (select logged_in_flag, count(*) num_decay from gaurav_all_temp_4 where day_flag = 4 group by 1) as b

on a.logged_in_flag = b.logged_in_flag;

-----
drop table if exists customer_insights.gaurav_all_temp_dataset_4;
create table customer_insights.gaurav_all_temp_dataset_4 as

select a.*, 
(case when received = 0 then 0 else 1.0*received/notif_days end) received_days, 
(case when received = 0 then 0 else 1.0*(collapsed)/(notif_days) end) collapsed_days,
(case when received = 0 then 0 else 1.0*(dismissed)/(notif_days) end) dismissed_days

from

(select a.device_id, a.install_date, a.uninstall_date, a.install_source, a.uninstalled_earlier, a.logged_in_flag, a.uninstall_rate, 
a.manufacturer_uninstall_rate, a.five_day_uninstall,  a.active_days, a.days_since_active, a.sessions, a.search, a.lp_views, a.pdp_views, a.atc, a.wishlist, a.screenloads, 
a.disengaged_sessions, a.casual_browse_sessions, a.targetedintense_browse_sessions, a.cart_view_sessions, a.bounce_sessions, a.order_review_sessions, a.shortlist_sessions,
1.0*a.disengaged_sessions/a.qot_sessions disengaged_sessions_per, 1.0*a.casual_browse_sessions/a.qot_sessions casual_browse_sessions_per, 
1.0*a.targetedintense_browse_sessions/a.qot_sessions targetedintense_browse_sessions_per, 1.0*a.cart_view_sessions/a.qot_sessions cart_view_sessions_per, 
1.0*a.order_review_sessions/a.qot_sessions order_review_sessions_per, 1.0*a.shortlist_sessions/a.qot_sessions shortlist_sessions_per, 
1.0*a.bounce_sessions/a.qot_sessions bounce_sessions_per, count(distinct b.load_date) notif_days, max(blocked) blocked_flag, 
sum(case when received = 0 and (collapsed > 0 or dismissed >0) then 1 else received end) received, sum(collapsed) collapsed, sum(dismissed) dismissed

from

(select a.device_id, a.install_date, a.uninstall_date, a.install_source, a.uninstalled_earlier, a.logged_in_flag,
(case when f.uninstall_rate is null then 0.45 else f.uninstall_rate end) uninstall_rate, 
(case when d.uninstall_rate is null then 0.44 else d.uninstall_rate end) manufacturer_uninstall_rate,
(case when a.uninstall_date::BIGINT - a.install_date::BIGINT between 5 and 9 then 1 else 0 end) five_day_uninstall, count(distinct a.day_flag) active_days, 
5 - max(a.day_flag) days_since_active, sum(a.sessions*decay) sessions, sum(search*decay) search, sum(lp_views*decay) lp_views, sum(pdp_views*decay) pdp_views, 
sum(atc*decay) atc, sum(wishlist*decay) wishlist, sum(decay*screenloads) screenloads, sum(disengaged_sessions*decay) disengaged_sessions, 
sum(decay*casual_browse_sessions) casual_browse_sessions, sum(decay*targetedintense_browse_sessions) targetedintense_browse_sessions, 
sum(decay*cart_view_sessions) cart_view_sessions, sum(decay*bounce_sessions) bounce_sessions, sum(decay*order_review_sessions) order_review_sessions, 
sum(decay*shortlist_sessions) shortlist_sessions, sum(decay*e.sessions) qot_sessions

from customer_insights.gaurav_all_temp_4 as a

inner join gaurav_all_temp_qot_4 as e

on a.device_id = e.device_id and a.install_date = e.install_date and a.day_flag = e.day_flag

inner join gaurav_all_temp_decay_4 as b

on a.day_flag = b.day_flag and a.logged_in_flag = b.logged_in_flag

left join 

(select f.device_name, g.uninstall_rate

from customer_insights.gaurav_uninstall_all_device_uninstall_1 as f

inner join customer_insights.gaurav_uninstall_all_device_bucket_uninstall_new_1 as g

on f.device_bucket_1 = g.device_bucket) as f

on a.device_name = f.device_name

inner join 

(select device_id, device_manufacturer from sessiondaily_view where load_date::BIGINT >= 20180629 group by 1, 2) as c

on a.device_id = c.device_id

left join 

(select d.device_manufacturer, e.uninstall_rate

from gaurav_uninstall_all_device_manufacturer_uninstall as d

inner join gaurav_uninstall_all_device_manufacturer_bucket_uninstall_new as e

on d.device_manufacturer_bucket_1 = e.device_manufacturer_bucket) as d

on c.device_manufacturer = d.device_manufacturer

group by 1, 2, 3, 4, 5, 6, 7, 8, 9) as a

left join fact_user_notification as b

on a.device_id = b.device_id
and load_date::BIGINT >= 20180629

where b.load_date - a.install_date between 0 and 4

group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32) as a;